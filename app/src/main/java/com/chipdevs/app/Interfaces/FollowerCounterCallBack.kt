package com.chipdevs.app.Interfaces


interface FollowerCounterCallBack
{

    fun getFollowers(
        count: Int,
    )
    fun getFollowing(
        count: Int,
    )
}