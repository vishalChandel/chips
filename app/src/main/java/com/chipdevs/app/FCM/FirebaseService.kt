package com.chipdevs.app.FCM

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.chipdevs.app.R
import com.chipdevs.app.models.CustomNotificationModel
import com.chipdevs.app.ui.activities.BaseActivity
import com.chipdevs.app.ui.activities.SplashActivity
import com.chipdevs.app.utils.AppPrefrences
import com.chipdevs.app.utils.IS_LOGIN
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import kotlin.random.Random


private const val CHANNEL_ID = "my_channel"
var notificationListing: CustomNotificationModel? = null
class FirebaseService : FirebaseMessagingService() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Log.e("NotifData", message.data.toString())
        if(AppPrefrences().readBoolean(this, IS_LOGIN, true)) {
            //then send push notifications

            if (message.data.isNotEmpty()) {
                try {
                    notificationListing =
                        Gson().fromJson(message.data["data"], CustomNotificationModel::class.java)
                    Log.e("ListNotif", notificationListing.toString())
                    sendNotification(message)
                } catch (e: Exception) {
                    Log.e("FCM", "onMessageReceived: $e")
                }

            }
        }
        else
        {
            Log.e("Status","UserLoggedOut")
        }
    }


    fun sendNotification(message: RemoteMessage)
    {
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("Data", notificationListing)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK )
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, FLAG_ONE_SHOT)

        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(message.data["title"])
            .setContentText(message.data["body"])
            .setSmallIcon(R.drawable.ic_push_notification)
            .setSound(defaultSoundUri)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .build()
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random.nextInt()


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName = "channelName"
            val channel = NotificationChannel(CHANNEL_ID, channelName, IMPORTANCE_HIGH).apply {
                enableLights(true)
                lightColor = Color.GREEN
            }
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(notificationID, notification)
    }


}