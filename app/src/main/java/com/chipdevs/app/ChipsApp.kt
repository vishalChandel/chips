package com.chipdevs.app.utils

import android.app.Application
import io.branch.referral.Branch
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

class ChipsApp : Application() {
    /**
     * Getting the Current Class Name
     */
    val TAG: String = "ChipsApp"

    /**
     * Initialize the Applications Instance
     */
    private var mInstance: ChipsApp? = null

    /**
     * Synchronized the Application Class Instance
     */
    @Synchronized
    fun getInstance():ChipsApp?{
        return mInstance
    }
    /*
     * Socket
     * */
    var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()
        // Branch logging for debugging
        Branch.enableDebugMode()

        // Branch object initialization
        Branch.getAutoInstance(this)
        // Required initialization logic here!
        mInstance = this
        try {
            mSocket = IO.socket(SOCKET_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    fun getSocket(): Socket? {
        return mSocket
    }
}