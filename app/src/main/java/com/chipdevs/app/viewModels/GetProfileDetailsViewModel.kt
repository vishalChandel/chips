package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.GetProfileDetailsModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GetProfileDetailsViewModel : ViewModel() {
    private var mGetProfileDetailsModel: MutableLiveData<GetProfileDetailsModel>? = null

    fun getProfileData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<GetProfileDetailsModel>? {
        mGetProfileDetailsModel = MutableLiveData<GetProfileDetailsModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getProfileDetailsRequest(mParams).enqueue(object : Callback<GetProfileDetailsModel?> {
            override fun onResponse(
                call: Call<GetProfileDetailsModel?>?,
                response: Response<GetProfileDetailsModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mGetProfileDetailsModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<GetProfileDetailsModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mGetProfileDetailsModel
    }
}