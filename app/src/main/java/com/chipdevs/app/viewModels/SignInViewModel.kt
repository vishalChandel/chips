package com.chipdevs.app.viewModels

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.SignInModel
import com.chipdevs.app.retrofit.ApiClient.apiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignInViewModel : ViewModel() {
    private var mSignInModel: MutableLiveData<SignInModel>? = null

    fun getSignInData(activity: Activity?, mParams: Map<String?, String?>?): LiveData<SignInModel>? {
        mSignInModel = MutableLiveData<SignInModel>()
        val apiInterface: ApiInterface = apiClient!!.create(ApiInterface::class.java)
        apiInterface.signInRequest(mParams).enqueue(object : Callback<SignInModel?> {
            override fun onResponse(call: Call<SignInModel?>?, response: Response<SignInModel?>) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    Log.e("SignINData", response?.body()!!.userDetails?.userID.toString())
                    mSignInModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }
            override fun onFailure(call: Call<SignInModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSignInModel
    }
}







