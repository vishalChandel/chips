package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.RemoveFromWishListModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoveFromWishListViewModel : ViewModel() {
    private var mRemoveFromWishListModel: MutableLiveData<RemoveFromWishListModel>? = null

    fun removeFromWishListData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<RemoveFromWishListModel>? {
        mRemoveFromWishListModel = MutableLiveData<RemoveFromWishListModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.removeFromWishListRequest(mParams).enqueue(object : Callback<RemoveFromWishListModel?> {
            override fun onResponse(
                call: Call<RemoveFromWishListModel?>?,
                response: Response<RemoveFromWishListModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mRemoveFromWishListModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<RemoveFromWishListModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mRemoveFromWishListModel
    }
}