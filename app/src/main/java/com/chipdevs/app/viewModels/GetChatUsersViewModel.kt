package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.GetChatUsersModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetChatUsersViewModel : ViewModel() {
    private var mGetChatUsersModel: MutableLiveData<GetChatUsersModel>? = null

    fun getGetChatUsersData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<GetChatUsersModel>? {
        mGetChatUsersModel = MutableLiveData<GetChatUsersModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getChatUsersRequest(mParams)
            .enqueue(object : Callback<GetChatUsersModel?> {
                override fun onResponse(
                    call: Call<GetChatUsersModel?>?,
                    response: Response<GetChatUsersModel?>
                ) {
                    BaseActivity().dismissProgressDialog()
                    if (response.body() != null) {
                        mGetChatUsersModel!!.setValue(response.body())
                    } else {
                        BaseActivity().dismissProgressDialog()
                    }
                }

                override fun onFailure(call: Call<GetChatUsersModel?>?, t: Throwable?) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mGetChatUsersModel
    }
}
