package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.MoveToBagModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoveToBagViewModel : ViewModel() {
    private var mMoveToBagModel: MutableLiveData<MoveToBagModel>? = null

    fun moveToBagData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<MoveToBagModel>? {
        mMoveToBagModel = MutableLiveData<MoveToBagModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.moveToBagRequest(mParams).enqueue(object : Callback<MoveToBagModel?> {
            override fun onResponse(
                call: Call<MoveToBagModel?>?,
                response: Response<MoveToBagModel?>
            ) {
               BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mMoveToBagModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<MoveToBagModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mMoveToBagModel
    }
}