package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.ChangePwdModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangePwdViewModel : ViewModel() {
    private var mChangePwdModel: MutableLiveData<ChangePwdModel>? = null

    fun sendChangePwdData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<ChangePwdModel>? {
        mChangePwdModel = MutableLiveData<ChangePwdModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.changePwdRequest(mParams).enqueue(object : Callback<ChangePwdModel?> {
            override fun onResponse(
                call: Call<ChangePwdModel?>?,
                response: Response<ChangePwdModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mChangePwdModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<ChangePwdModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mChangePwdModel
    }
}
