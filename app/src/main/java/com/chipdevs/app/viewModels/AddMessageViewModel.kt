package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.AddMessageModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddMessageViewModel : ViewModel() {
    private var mAddMessageModel: MutableLiveData<AddMessageModel>? = null

    fun sendAddMessageData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<AddMessageModel>? {
        mAddMessageModel = MutableLiveData<AddMessageModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.addMsgRequest(mParams).enqueue(object : Callback<AddMessageModel?> {
            override fun onResponse(
                call: Call<AddMessageModel?>?,
                response: Response<AddMessageModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mAddMessageModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<AddMessageModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAddMessageModel
    }
}
