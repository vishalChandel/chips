package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.UpdateMessageSeenModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateMsgSeenViewModel : ViewModel() {
    private var mUpdateMessageSeenModel: MutableLiveData<UpdateMessageSeenModel>? = null

    fun sendUpdateMessageSeenData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<UpdateMessageSeenModel>? {
        mUpdateMessageSeenModel = MutableLiveData<UpdateMessageSeenModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.updateMessageSeenRequest(mParams).enqueue(object : Callback<UpdateMessageSeenModel?> {
            override fun onResponse(
                call: Call<UpdateMessageSeenModel?>?,
                response: Response<UpdateMessageSeenModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mUpdateMessageSeenModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<UpdateMessageSeenModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mUpdateMessageSeenModel
    }
}
