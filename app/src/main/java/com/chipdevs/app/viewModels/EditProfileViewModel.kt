package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.EditProfileModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class EditProfileViewModel : ViewModel() {
    private var mEditProfileModel: MutableLiveData<EditProfileModel>? = null

    fun sendEditProfileData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<EditProfileModel>? {
        mEditProfileModel = MutableLiveData<EditProfileModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.editProfileRequest(mParams).enqueue(object : Callback<EditProfileModel?> {
            override fun onResponse(
                call: Call<EditProfileModel?>?,
                response: Response<EditProfileModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mEditProfileModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<EditProfileModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mEditProfileModel
    }
}
