package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.ForgotPwdModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPwdViewModel : ViewModel() {
    private var mForgotPwdModel: MutableLiveData<ForgotPwdModel>? = null

    fun sendForgotPwdData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<ForgotPwdModel>? {
        mForgotPwdModel = MutableLiveData<ForgotPwdModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.forgotPwdRequest(mParams).enqueue(object : Callback<ForgotPwdModel?> {
            override fun onResponse(
                call: Call<ForgotPwdModel?>?,
                response: Response<ForgotPwdModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mForgotPwdModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<ForgotPwdModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mForgotPwdModel
    }
}
