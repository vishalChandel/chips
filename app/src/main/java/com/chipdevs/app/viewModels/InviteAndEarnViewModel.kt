package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.InviteAndEarnModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InviteAndEarnViewModel : ViewModel() {
    private var mInviteAndEarnModel: MutableLiveData<InviteAndEarnModel>? = null

    fun sendInviteAndEarnData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<InviteAndEarnModel>? {
        mInviteAndEarnModel = MutableLiveData<InviteAndEarnModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.inviteAndEarnRequest(mParams).enqueue(object : Callback<InviteAndEarnModel?> {
            override fun onResponse(
                call: Call<InviteAndEarnModel?>?,
                response: Response<InviteAndEarnModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mInviteAndEarnModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<InviteAndEarnModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mInviteAndEarnModel
    }
}
