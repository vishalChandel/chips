package com.chipdevs.app.viewModels

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.SignUpModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpViewModel : ViewModel() {
    private var mSignUpModel: MutableLiveData<SignUpModel>? = null

    fun postSignUpData(activity: Activity?, mParams: Map<String?, String?>?): LiveData<SignUpModel>? {
        mSignUpModel = MutableLiveData<SignUpModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.signUpRequest(mParams).enqueue(object : Callback<SignUpModel?> {
            override fun onResponse(call: Call<SignUpModel?>?, response: Response<SignUpModel?>) {
              Log.e("Response",response.message().toString())
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mSignUpModel!!.value = response.body()
                    BaseActivity().dismissProgressDialog()
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }
            override fun onFailure(call: Call<SignUpModel?>?, t: Throwable?) {
                Log.e("Response","S")
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSignUpModel
    }
}
