package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.WishListModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetWishListViewModel : ViewModel() {
    private var mWishListModel: MutableLiveData<WishListModel>? = null

    fun getWishListData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<WishListModel>? {
        mWishListModel = MutableLiveData<WishListModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getWishListRequest(mParams)
            .enqueue(object : Callback<WishListModel?> {
                override fun onResponse(
                    call: Call<WishListModel?>?,
                    response: Response<WishListModel?>
                ) {
                    BaseActivity().dismissProgressDialog()
                    if (response.body() != null) {
                        mWishListModel!!.setValue(response.body())
                    } else {
                        BaseActivity().dismissProgressDialog()
                    }
                }

                override fun onFailure(call: Call<WishListModel?>?, t: Throwable?) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mWishListModel
    }
}
