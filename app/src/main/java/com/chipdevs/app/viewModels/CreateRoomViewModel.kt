package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import com.chips.app.models.CreateRoomModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CreateRoomViewModel : ViewModel() {
    private var mCreateRoomModel: MutableLiveData<CreateRoomModel>? = null

    fun sendCreateRoomData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<CreateRoomModel>? {
        mCreateRoomModel = MutableLiveData<CreateRoomModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.createRoomRequest(mParams).enqueue(object : Callback<CreateRoomModel?> {
            override fun onResponse(
                call: Call<CreateRoomModel?>?,
                response: Response<CreateRoomModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mCreateRoomModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<CreateRoomModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mCreateRoomModel
    }
}
