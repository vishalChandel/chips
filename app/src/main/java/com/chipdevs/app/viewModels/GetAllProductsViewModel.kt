package com.chipdevs.app.viewModels

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.GetAllProductsModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetAllProductsViewModel : ViewModel() {
    private var mGetAllProductsModel: MutableLiveData<GetAllProductsModel>? = null

    fun getAllProductsData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<GetAllProductsModel>? {
        mGetAllProductsModel = MutableLiveData<GetAllProductsModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getAllProductsRequest(mParams)
            .enqueue(object : Callback<GetAllProductsModel?> {
                override fun onResponse(
                    call: Call<GetAllProductsModel?>?,
                    response: Response<GetAllProductsModel?>
                ) {
                    BaseActivity().dismissProgressDialog()
                    if (response.body() != null) {
                        Log.e("AllProducts",response.body().toString())
                        mGetAllProductsModel!!.setValue(response.body())
                    } else {
                        BaseActivity().dismissProgressDialog()
                    }
                }

                override fun onFailure(call: Call<GetAllProductsModel?>?, t: Throwable?) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mGetAllProductsModel
    }
}
