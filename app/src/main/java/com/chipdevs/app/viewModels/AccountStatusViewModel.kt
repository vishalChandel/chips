package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.AccountStatusModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AccountStatusViewModel : ViewModel() {
    private var mAccountStatusModel: MutableLiveData<AccountStatusModel>? = null

    fun sendAccountStatusData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<AccountStatusModel>? {
        mAccountStatusModel = MutableLiveData<AccountStatusModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.accountStatusRequest(mParams).enqueue(object : Callback<AccountStatusModel?> {
            override fun onResponse(
                call: Call<AccountStatusModel?>?,
                response: Response<AccountStatusModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mAccountStatusModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<AccountStatusModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAccountStatusModel
    }
}
