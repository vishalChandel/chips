package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.ContactUsModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactUsViewModel : ViewModel() {
    private var mContactUsModel: MutableLiveData<ContactUsModel>? = null

    fun sendContactUsData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<ContactUsModel>? {
        mContactUsModel = MutableLiveData<ContactUsModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.sendContactUsRequest(mParams).enqueue(object : Callback<ContactUsModel?> {
            override fun onResponse(
                call: Call<ContactUsModel?>?,
                response: Response<ContactUsModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mContactUsModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<ContactUsModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mContactUsModel
    }
}
