package com.chipdevs.app.viewModels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chipdevs.app.models.GetChatListingModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GetChatListingViewModel : ViewModel() {
    private var mGetChatListingModel: MutableLiveData<GetChatListingModel>? = null

    fun sendGetChatListingData(
        activity: Activity?,
        mParams: Map<String?, String?>?
    ): LiveData<GetChatListingModel>? {
        mGetChatListingModel = MutableLiveData<GetChatListingModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getChatListingRequest(mParams).enqueue(object : Callback<GetChatListingModel?> {
            override fun onResponse(
                call: Call<GetChatListingModel?>?,
                response: Response<GetChatListingModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mGetChatListingModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<GetChatListingModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mGetChatListingModel
    }
}
