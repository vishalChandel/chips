package com.chipdevs.app.utils


/*
* Base Server Url
* */
const val BASE_URL = "http://54.205.52.3/v2/"

const val SOCKET_URL = "https://jaohar-uk.herokuapp.com"


/*
* Links Constants
* */
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

/*
* Link Urls
* */
const val ABOUT_WEB_LINK = BASE_URL + "webServices/AboutUs.html"
const val PP_WEB_LINK = BASE_URL + "webServices/terms&services.html"
const val TERMS_WEB_LINK = BASE_URL + "webServices/terms&services.html"

/*
* Fragment Tags
* */
const val HOME_TAG = "home_tag"
const val CHIPS_TAG = "chips_tag"
const val SHARE_TAG = "share_tag"
const val PROFILE_TAG = "profile_tag"

/*
* Shared Preferences Keys
* */
const val IS_LOGIN = "is_login"
const val USERID = "userID"
const val USERNAME = "userName"
const val LASTNAME = "lastName"
const val NAME = "name"
const val FIRSTNAME = "firstName"
const val EMAIL = "email"
var PROFILEPIC = "image"
const val GENDER = "gender"
const val DOB = "dob"
const val GRADE = "grade"
var STATUS = "status"
const val ROOM_ID = "room_id"
const val PAGE_NO = "page_no"
const val DEVICE_TOKEN = "device_token"


/*
* Data Transfer
* */

/*
* View Holder
* */
const val LEFT_VIEW_HOLDER = 0
const val RIGHT_VIEW_HOLDER = 1
const val LAST_MSG = "last_msg"