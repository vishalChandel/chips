package com.chipdevs.app.utils

import android.app.Application

class BackStackManager: Application() {
    companion object {
        @JvmField
        var fragmentPosition: String = ""
    }
}