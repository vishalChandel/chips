package com.chipdevs.app.ui.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.AddFollowUnfollowModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import kotlinx.android.synthetic.main.activity_others_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class OthersProfileActivity : BaseActivity() {
    @BindView(R.id.tvFollowerName)
    lateinit var tvFollowerName: TextView
    @BindView(R.id.imgBackRL)
    lateinit var imgBackRL: RelativeLayout
    @BindView(R.id.followUserTv)
    lateinit var followUserTv: TextView
    var mOtherUserId = ""
    var mImageUrl = ""
    var mFollowStatus=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_others_profile)
        ButterKnife.bind(this)
        setDataOnWidgets()


        followUserTv.setOnClickListener {
            followUnFollowRequest()
        }
        imgBackRL.setOnClickListener {
            onBackPressed()
        }



        @OnClick(
            R.id.imgBackRL,
            R.id.followUserTv
        )
        fun onClick(view: View) {
            when (view.id) {
                R.id.imgBackRL -> onBackPressed()
                R.id.followUserTv -> followUserRequest()
            }
        }


    }

    private fun setDataOnWidgets() {
        val mUserName = intent.getStringExtra("mUserName")
        if (mUserName!!.isNotEmpty()) {
            tvFollowerName.text = mUserName
        }
        mOtherUserId = intent.getStringExtra("mOtherUserId").toString()
        mImageUrl = intent.getStringExtra("mImageUrl").toString()
        mFollowStatus = intent.getStringExtra("mFollowStatus").toString()

        Glide.with(this!!).load(mImageUrl)
            .error(R.drawable.ic_profile)
            .placeholder(R.drawable.ic_profile)
            .into(followerProfileIv)

        if(mFollowStatus=="1")
        {
            followUserTv.text = getString(R.string.follow_request_sent)
            followUserTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_description))
            followUserTv.setTextColor(resources.getColor(R.color.colorBlack))
        }
        else {
            followUserTv.text = getString(R.string.follow)
            followUserTv.setTextColor(resources.getColor(R.color.colorWhite))
            followUserTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_follow))
        }
    }


    fun followUnFollowRequest()
    {

        if( followUserTv.text==getString(R.string.follow))
        {
            followUserRequest()
        }
        else if( followUserTv.text==getString(R.string.following))
        {
            followUserRequest()
        }
        else
        {
            followUserRequest()
        }
    }

    fun followUserRequest() {
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followUserID"] = mOtherUserId
        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.followRequest(mMap).enqueue(object : Callback<AddFollowUnfollowModel> {
            override fun onResponse(
                call: Call<AddFollowUnfollowModel>,
                response: Response<AddFollowUnfollowModel>
            ) {
                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1" && responseModel?.notificationDetails != null) {
                    Log.e(TAG, responseModel.notificationDetails.toString())
                    updateWidgets()
                    isRequestSent ="yes"
                }
                else
                {
                    isRequestSent="no"
                    updateToDefaultWidgets()
                }
            }

            override fun onFailure(call: Call<AddFollowUnfollowModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }

    private fun updateToDefaultWidgets() {
        followUserTv.text = getString(R.string.follow)
        followUserTv.setTextColor(resources.getColor(R.color.colorWhite))
        followUserTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_follow))
    }

    private fun updateWidgets() {

            followUserTv.text = getString(R.string.follow_request_sent)
            followUserTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_description))
            followUserTv.setTextColor(resources.getColor(R.color.colorBlack))
    }

    companion object
    {
        var isRequestSent=""
    }
}