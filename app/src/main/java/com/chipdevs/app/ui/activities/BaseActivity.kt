package com.chipdevs.app.ui.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.chipdevs.app.R
import com.chipdevs.app.utils.*
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.regex.Pattern

open class BaseActivity : AppCompatActivity() {

    //  Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    //  Initialize Activity
    var mActivity: Activity = this@BaseActivity

    //  Initialize Objects
    var progressDialog: Dialog? = null

    //  Finishes Activity
    override fun finish() {
        super.finish()
        overridePendingTransitionSlideDownExit()
    }

    //  Start Activity
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransitionSlideUPEnter()
    }

    //  Overrides the pending Activity transition by performing the "Bottom Up" animation.
    fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    //  Overrides the pending Activity transition by performing the "Bottom Down" animation.
    fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }

    //  Clear EditText Focus
    fun setEditTextFocused(mActivity: Activity, mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    //  Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    //  Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    //  Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    //  Email Address Validation
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    //  To Check Internet Connections
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    //  Get UserId
    fun getLoggedInUserID(): String {
        return AppPrefrences().readString(mActivity, USERID, "")!!
    }

    //    Get UserName
    fun getUserName(): String {
        return AppPrefrences().readString(mActivity, USERNAME, "")!!
    }


    //  Get Email
    fun getEmail(): String {
        return AppPrefrences().readString(mActivity, EMAIL, "")!!
    }

    //  Get Account Status
    fun getStatus(): String {
        return AppPrefrences().readString(mActivity, STATUS, "")!!
    }

    //  Check whether user is logged in or not
    fun isLogin(): Boolean {
        return AppPrefrences().readBoolean(mActivity, IS_LOGIN, false)
    }

    //  Open SignIn Activity Alert Dialog
    fun showFinishOpenPreviousActivityAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val i = Intent(mActivity, SignInActivity::class.java)
            startActivity(i)
            finish()
        }
        alertDialog.show()
    }

    //  Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    //    Switch between fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

}