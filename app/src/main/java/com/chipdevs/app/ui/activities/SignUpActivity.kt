package com.chipdevs.app.ui.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.models.SignUpModel
import com.chipdevs.app.utils.*
import com.chipdevs.app.viewModels.SignUpViewModel

import kotlinx.android.synthetic.main.activity_sign_up.*
import java.util.*

import com.chipdevs.app.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging


class
SignUpActivity : BaseActivity() {
    @BindView(R.id.btnSignUpTV)
    lateinit var btnSignUpTV: TextView

    @BindView(R.id.txtSignInTV)
    lateinit var txtSignInTV: TextView

    @BindView(R.id.editGradeET)
    lateinit var editGradeET: EditText

    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editUsernameET)
    lateinit var editUsernameET: EditText

    @BindView(R.id.editEmailAddressET)
    lateinit var editEmailAddressET: EditText

    @BindView(R.id.editConfirmEmailET)
    lateinit var editConfirmEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.editConfirmPwdET)
    lateinit var editConfirmPwdET: EditText

    @BindView(R.id.txtMaleTV)
    lateinit var txtMaleTV: TextView

    @BindView(R.id.txtFemaleTV)
    lateinit var txtFemaleTV: TextView

    @BindView(R.id.txtShoeSizeTV1)
    lateinit var txtShoeSizeTV1: TextView

    @BindView(R.id.txtShoeSizeTV2)
    lateinit var txtShoeSizeTV2: TextView

    @BindView(R.id.txtShoeSizeTV3)
    lateinit var txtShoeSizeTV3: TextView

    @BindView(R.id.txtShoeSizeTV4)
    lateinit var txtShoeSizeTV4: TextView

    @BindView(R.id.txtShoeSizeTV5)
    lateinit var txtShoeSizeTV5: TextView

    @BindView(R.id.txtOtherTV)
    lateinit var txtOtherTV: TextView

    @BindView(R.id.termsConditionCB)
    lateinit var termsConditionCB: CheckBox

    /*
     * Initialize Objects...
     */
    var mDeviceToken: String = ""

    private var mSignUpViewModel: SignUpViewModel? = null

    var mSendDBDate: String = ""

    var mGender: String = ""

    var mShoeSize: String = ""

    var mAge: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(this)
        mSignUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
       getDeviceToken()
        setEditTextFocused(mActivity, editGradeET)
    }

    @OnClick(
        R.id.btnSignUpTV,
        R.id.txtSignInTV,
        R.id.termsConditionsTV,
        R.id.imgMaleIV,
        R.id.imgFemaleIV,
        R.id.imgOtherIV,
        R.id.shoeSizeLL1,
        R.id.shoeSizeLL2,
        R.id.shoeSizeLL3,
        R.id.shoeSizeLL4,
        R.id.shoeSizeLL5,
        R.id.txtDOBTV,
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.btnSignUpTV -> performSignUpClick()
            R.id.txtSignInTV -> performSignInClick()
            R.id.termsConditionsTV -> performTermsClick()
            R.id.imgMaleIV -> performMaleClick()
            R.id.imgFemaleIV -> performFemaleClick()
            R.id.imgOtherIV -> performOtherClick()
            R.id.shoeSizeLL1 -> performShoeSizeClick1()
            R.id.shoeSizeLL2 -> performShoeSizeClick2()
            R.id.shoeSizeLL3 -> performShoeSizeClick3()
            R.id.shoeSizeLL4 -> performShoeSizeClick4()
            R.id.shoeSizeLL5 -> performShoeSizeClick5()
            R.id.txtDOBTV -> showDatePickar(txtDOBTV)
        }
    }

    private fun performSignUpClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSignUpRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = editFirstNameET.text.toString()
        mMap["lastName"] = editLastNameET.text.toString()
        mMap["userName"] = editUsernameET.text.toString()
        mMap["email"] = editEmailAddressET.text.toString()
        mMap["pass"] = editPasswordET.text.toString()
        mMap["age"] = mAge.toString()
        mMap["grade"] = editGradeET.text.toString()
        mMap["gender"] = mGender
        mMap["shoeSize"] = mShoeSize
        mMap["dob"] = mSendDBDate
        mMap["referCode"]=editReferCoderET.text.toString()
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = "2"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSignUpRequest() {
        showProgressDialog(mActivity)
        mSignUpViewModel?.postSignUpData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<SignUpModel?> { mSignUpModel ->
                if (mSignUpModel.status == 1) {
                    dismissProgressDialog()
                    showFinishOpenPreviousActivityAlertDialog(mActivity, getString(R.string.you_have_signed_up_successfully))
                    editFirstNameET.setText("")
                    editLastNameET.setText("")
                    editUsernameET.setText("")
                    editEmailAddressET.setText("")
                    editConfirmEmailET.setText("")
                    editPasswordET.setText("")
                    editConfirmPwdET.setText("")
                    txtDOBTV.setText("")
                    editGradeET.setText("")
                }
                else if(mSignUpModel.status==0){
                    showAlertDialog(mActivity,mSignUpModel.message )
                    dismissProgressDialog()
                }
                else {
                    showAlertDialog(mActivity,mSignUpModel.message )
                    dismissProgressDialog()
                }
            })
    }

    private fun performSignInClick() {
        val i = Intent(mActivity, SignInActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun performTermsClick() {
        val i = Intent(mActivity, WebviewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }


    private fun getDeviceToken() {

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                mDeviceToken = task.result!!
                Log.e(TAG, "**Push Token**$mDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $mDeviceToken")

                AppPrefrences().writeString(this, DEVICE_TOKEN,mDeviceToken)
            })

    }




    private fun performMaleClick() {
        mGender = "Male"
        genderSelection(0)
    }

    private fun performFemaleClick() {
        mGender = "Female"
        genderSelection(1)
    }

    private fun performOtherClick() {
        mGender = "Other"
        genderSelection(2)
    }

    private fun performShoeSizeClick1() {
        mShoeSize = "7.5"
        shoeSizeSelection(0)
    }

    private fun performShoeSizeClick2() {
        mShoeSize = "8"
        shoeSizeSelection(1)
    }

    private fun performShoeSizeClick3() {
        mShoeSize = "8.5"
        shoeSizeSelection(2)
    }

    private fun performShoeSizeClick4() {
        mShoeSize = "9"
        shoeSizeSelection(3)
    }

    private fun performShoeSizeClick5() {
        mShoeSize = "9.5"
        shoeSizeSelection(4)
    }


    private fun genderSelection(mPos: Int) {
        imgMaleIV.setImageResource(R.drawable.ic_male)
        imgFemaleIV.setImageResource(R.drawable.ic_female)
        imgOtherIV.setImageResource(R.drawable.ic_other)
        txtMaleTV.setTextColor(resources.getColor(R.color.colorBlue))
        txtFemaleTV.setTextColor(resources.getColor(R.color.colorBlue))
        txtOtherTV.setTextColor(resources.getColor(R.color.colorBlue))
        if (mPos == 0) {
            imgMaleIV.setImageResource(R.drawable.ic_male_selected)
            txtMaleTV.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 1) {
            imgFemaleIV.setImageResource(R.drawable.ic_female_selected)
            txtFemaleTV.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 2) {
            imgOtherIV.setImageResource(R.drawable.ic_other_selected)
            txtOtherTV.setTextColor(resources.getColor(R.color.colorYellow))
        }
    }

    private fun shoeSizeSelection(mPos: Int) {
        txtShoeSizeTV1.setBackgroundResource(R.drawable.bg_shoe_size)
        txtShoeSizeTV2.setBackgroundResource(R.drawable.bg_shoe_size)
        txtShoeSizeTV3.setBackgroundResource(R.drawable.bg_shoe_size)
        txtShoeSizeTV4.setBackgroundResource(R.drawable.bg_shoe_size)
        txtShoeSizeTV5.setBackgroundResource(R.drawable.bg_shoe_size)
        txtShoeSizeTV1.setTextColor(resources.getColor(R.color.colorBlue))
        txtShoeSizeTV2.setTextColor(resources.getColor(R.color.colorBlue))
        txtShoeSizeTV3.setTextColor(resources.getColor(R.color.colorBlue))
        txtShoeSizeTV4.setTextColor(resources.getColor(R.color.colorBlue))
        txtShoeSizeTV5.setTextColor(resources.getColor(R.color.colorBlue))

        if (mPos == 0) {
            txtShoeSizeTV1.setBackgroundResource(R.drawable.bg_shoe_size_selected)
            txtShoeSizeTV1.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 1) {
            txtShoeSizeTV2.setBackgroundResource(R.drawable.bg_shoe_size_selected)
            txtShoeSizeTV2.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 2) {
            txtShoeSizeTV3.setBackgroundResource(R.drawable.bg_shoe_size_selected)
            txtShoeSizeTV3.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 3) {
            txtShoeSizeTV4.setBackgroundResource(R.drawable.bg_shoe_size_selected)
            txtShoeSizeTV4.setTextColor(resources.getColor(R.color.colorYellow))
        } else if (mPos == 4) {
            txtShoeSizeTV5.setBackgroundResource(R.drawable.bg_shoe_size_selected)
            txtShoeSizeTV5.setTextColor(resources.getColor(R.color.colorYellow))
        }
    }

    // DatePicker
    open fun showDatePickar(mTextView: EditText) {
        val c = Calendar.getInstance()
        val mYear = c[Calendar.YEAR]
        val mMonth = c[Calendar.MONTH]
        val mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                var monthValue = ""
                monthValue = if (monthOfYear + 1 >= 10) {
                    (monthOfYear + 1).toString()
                } else {
                    "0" + (monthOfYear + 1).toString()
                }
                var dateValue = ""
                dateValue = if (dayOfMonth >= 10) {
                    dayOfMonth.toString()
                } else {
                    "0$dayOfMonth"
                }
                //2021-06-23
                var mActualDate: String = monthValue + "-" + dateValue + "-" + year
                mSendDBDate = "" + year + "-" + monthValue + "-" + dateValue
                mAge = (mYear - year)
                mTextView.setText(mActualDate)

            }, mYear, mMonth, mDay
        )
        val currentDate = System.currentTimeMillis()
        datePickerDialog.datePicker.maxDate = currentDate
        datePickerDialog.show()

    }


    /*
  * Set up validations for Log In fields
  * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_firstname))
            flag = false
        } else if (editLastNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_lastname))
            flag = false
        } else if (editUsernameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_username))
            flag = false
        } else if (editEmailAddressET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(editEmailAddressET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editConfirmEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_re_enter_email_address))
            flag = false
        } else if (!editEmailAddressET!!.text.toString().trim()
                .equals(editConfirmEmailET!!.text.toString().trim())
        ) {
            showAlertDialog(mActivity, getString(R.string.the_new_re_typed_email))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        } else if (editPasswordET.text.toString().length < 4) {
            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
            flag = false
        } else if (editConfirmPwdET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_re_enter_password))
            flag = false
        } else if (!editPasswordET!!.text.toString().trim()
                .equals(editConfirmPwdET!!.text.toString().trim())
        ) {
            showAlertDialog(mActivity, getString(R.string.the_new_re_typed_pwd))
            flag = false
        }
//        else if (editGradeET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_grade))
//            flag = false}

//        } else if (mSendDBDate.trim { it <= ' ' } == "") {
////            showAlertDialog(mActivity, getString(R.string.please_select_dob))
////            flag = false
         else if (mGender.trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_gender))
            flag = false
        }
       else if (mShoeSize.trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_shoe_size))
            flag = false
        }
//        else if (mAge <= 18) {
//            showAlertDialog(mActivity, getString(R.string.your_age_should_be_equal_or_greater))
//            flag = false
//        }
        else if (termsConditionCB.isChecked == false) {
            showAlertDialog(mActivity, getString(R.string.please_accept))
            flag = false
        }
        return flag
    }

}