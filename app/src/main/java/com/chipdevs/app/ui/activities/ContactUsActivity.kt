package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.models.ContactUsModel
import com.chipdevs.app.viewModels.ContactUsViewModel
import java.util.HashMap

class ContactUsActivity : BaseActivity() {
    @BindView(R.id.editUsernameET)
    lateinit var editUsernameET: EditText

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPhoneNumberET)
    lateinit var editPhoneNumberET: EditText

    @BindView(R.id.editDescriptionET)
    lateinit var editDescriptionET: EditText

    private var mContactUsViewModel: ContactUsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        ButterKnife.bind(this)
        setEditTextFocused(mActivity, editPhoneNumberET)
        mContactUsViewModel = ViewModelProviders.of(this).get(ContactUsViewModel::class.java)
        setDataOnWidgets()
    }

    private fun setDataOnWidgets() {
        editUsernameET.setText(getUserName())
        editEmailET.setText(getEmail())
    }

    @OnClick(
        R.id.imgBackRL,
        R.id.btnSubmitTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> onBackPressed()
            R.id.btnSubmitTV -> performSubmitClick()
        }
    }

    fun performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeChangePwdRequest()
            else
                showToast(mActivity, getString(R.string.internal_server_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["username"] = getUserName()
        mMap["email"] = editEmailET.text.toString()
        mMap["phone_no"] = editPhoneNumberET.text.toString()
        mMap["message"] = editDescriptionET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeChangePwdRequest() {
        showProgressDialog(mActivity)
        mContactUsViewModel?.sendContactUsData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<ContactUsModel?> { mContactUsModel ->
                if (mContactUsModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mContactUsModel.message)
                    val i = Intent(mActivity, SettingsActivity::class.java)
                    startActivity(i)
                    finish()
                    editUsernameET.setText("")
                    editEmailET.setText("")
                    editPhoneNumberET.setText("")
                    editDescriptionET.setText("")
                } else {
                    showAlertDialog(mActivity, mContactUsModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    /*
      * Set up validations for Change Password fields
      * */
    fun isValidate(): Boolean {
        var flag = true
        if (editUsernameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_username))
            flag = false
        } else if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPhoneNumberET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number))
            flag = false
        } else if (editDescriptionET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_description))
            flag = false
        }
        return flag
    }
}