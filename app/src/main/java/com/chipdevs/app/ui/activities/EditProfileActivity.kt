package com.chipdevs.app.ui.activities

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.EditProfileModel
import com.chipdevs.app.models.GetProfileDetailsModel
import com.chipdevs.app.viewModels.EditProfileViewModel
import com.chipdevs.app.viewModels.GetProfileDetailsViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

import android.graphics.BitmapFactory
import android.net.Uri
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.nekoloop.base64image.Base64Image
import com.nekoloop.base64image.RequestEncode
import java.io.*


class EditProfileActivity : BaseActivity() {

    @BindView(R.id.profileIV)
    lateinit var profileIV: ImageView

    @BindView(R.id.txtNameTV)
    lateinit var txtNameTV: TextView

    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editUserNameET)
    lateinit var editUserNameET: EditText

    @BindView(R.id.editDOBET)
    lateinit var editDOBET: TextView

    @BindView(R.id.editGenderET)
    lateinit var editGenderET: TextView

    @BindView(R.id.editGradeET)
    lateinit var editGradeET: EditText

 @BindView(R.id.editUserCashTagET)
    lateinit var editCashTagET: EditText

    private var mGetProfileDetailsViewModel: GetProfileDetailsViewModel? = null
    private var mEditProfileViewModel: EditProfileViewModel? = null

    /*
    * Initialize Menifest Permissions:
    * & Camera Gallery Request @params
    * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA

    var mSendDBDate: String? = ""
    var mGender: String? = ""
    var mAge: Int = 20
    var mBitmap: Bitmap? = null
    var mBase64Image: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        mEditProfileViewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)
        mGetProfileDetailsViewModel =
            ViewModelProviders.of(this).get(GetProfileDetailsViewModel::class.java)
        executeGetProfileDetailsRequest()
    }


    @OnClick(
        R.id.txtCancelTV,
        R.id.txtDoneTV,
        R.id.editDOBET,
        R.id.editGenderET,
        R.id.imgUpdateProfileIV


    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtDoneTV -> performDoneClick()
            R.id.txtCancelTV -> performCancelClick()
            R.id.editDOBET -> showDatePicker(editDOBET)
            R.id.editGenderET -> showBottomSheetDialogue(editGenderET)
            R.id.imgUpdateProfileIV -> performImagePickClick()
        }
    }

    private fun performCancelClick() {
        val i = Intent(mActivity, SettingsActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    private fun performImagePickClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(50)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                120,
                120
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            try {
                //Image Uri will not be null for RESULT_OK
                val uri: Uri = data?.data!!
                // Use Uri object instead of File to avoid storage permissions
                Glide
                    .with(this)
                    .load(uri)
                    .centerCrop()
                    .placeholder(R.drawable.ic_profile)
                    .into(profileIV)

                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                val out = ByteArrayOutputStream()
                selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                mBitmap = selectedImage

                Base64Image.with(this)
                    .encode(mBitmap)
                    .into(object : RequestEncode.Encode {
                        override fun onFailure() {
                            Log.e(TAG, "onFailure: ")
                            profileIV.setImageResource(0)
                        }

                        override fun onSuccess(p0: String?) {
                            Log.e(TAG, "onSuccess: ")
                            mBase64Image = p0!!
                        }
                    })

            } catch (e: IOException) {
                Log.e(TAG, "****Error****" + e.printStackTrace())
                e.printStackTrace()
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    fun setDataOnWidgets(mGetProfileModel: GetProfileDetailsModel) {
//      Set Data on Widgets
        txtNameTV.text =
            mGetProfileModel.userDetails?.firstName + " " + mGetProfileModel.userDetails?.lastName
        editFirstNameET.setText(mGetProfileModel.userDetails?.firstName)
        editLastNameET.setText(mGetProfileModel.userDetails?.lastName)
        editUserNameET.setText(mGetProfileModel.userDetails?.userName)
        editCashTagET.setText(mGetProfileModel.userDetails?.cashTag)
        editDOBET.setText(mGetProfileModel.userDetails?.dob)
        editGenderET.setText(mGetProfileModel.userDetails?.gender)
        editGradeET.setText(mGetProfileModel.userDetails?.grade)
        mGender = mGetProfileModel.userDetails?.gender
        mSendDBDate = mGetProfileModel.userDetails?.dob
        if(mGetProfileModel.userDetails?.image!=null && mGetProfileModel.userDetails?.image!==""){

            Glide
                .with(this)
                .load(mGetProfileModel.userDetails?.image)
                .centerCrop()
                .placeholder(R.drawable.ic_profile)
                .into(profileIV)

            profileIV.buildDrawingCache()
            val mImageBitmap : Bitmap = profileIV.getDrawingCache()

            Base64Image.with(this)
                .encode(mImageBitmap)
                .into(object : RequestEncode.Encode {
                    override fun onFailure() {
                        Log.e(TAG, "onFailure: ")
                        profileIV.setImageResource(0)
                    }

                    override fun onSuccess(p0: String?) {
                        Log.e(TAG, "onSuccess: ")
                        mBase64Image = p0!!
                    }
                })


    }
    }

    private fun performDoneClick() {
        setEditTextFocused(mActivity,editGradeET)
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeEditProfileRequest()
            else
                showToast(mActivity, getString(R.string.internal_server_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userid"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailsRequest() {
        showProgressDialog(mActivity)
        mGetProfileDetailsViewModel?.getProfileData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<GetProfileDetailsModel?> { mGetProfileData ->
                if (mGetProfileData.status == "1") {
                    dismissProgressDialog()
                    setDataOnWidgets(mGetProfileData)
                } else {
                    showAlertDialog(mActivity, mGetProfileData.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    private fun mEditProfileParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["image"] = mBase64Image
        mMap["firstName"] = editFirstNameET.text.toString()
        mMap["lastName"] = editLastNameET.text.toString()
        mMap["userName"] = editUserNameET.text.toString()
        mMap["cashTag"] = editCashTagET.text.toString()
        mMap["age"] = mAge.toString()
        mMap["grade"] = editGradeET.text.toString()
        mMap["gender"] = mGender
        mMap["dob"] = mSendDBDate
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeEditProfileRequest() {
        showProgressDialog(mActivity)
        mEditProfileViewModel?.sendEditProfileData(mActivity, mEditProfileParams())?.observe(this,
            androidx.lifecycle.Observer<EditProfileModel?> { mEditProfileModel ->
                if (mEditProfileModel.status == "1") {
                    dismissProgressDialog()
                    showToast(mActivity, mEditProfileModel.message)
                    val i = Intent(mActivity, SettingsActivity::class.java)
                    startActivity(i)
                    finish()

                } else {
                    showAlertDialog(mActivity, mEditProfileModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    /*
    * Set up validations for EditProfile EditText fields
    * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_firstname))
            flag = false
        } else if (editLastNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_lastname))
            flag = false
        } else if (editUserNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_username))
            flag = false
        } else if (editDOBET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_dob))
            flag = false
        }
        else if (editCashTagET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.enter_casgtag))
            flag = false
        }
        else if (mAge <= 18) {
            showAlertDialog(mActivity, getString(R.string.your_age_should_be_equal_or_greater))
            flag = false
        } else if (editGradeET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_grade))
            flag = false
        } else if (editGenderET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_gender))
            flag = false
        }
        return flag
    }


    // DatePicker
    open fun showDatePicker(mTextView: TextView) {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editDOBET.getWindowToken(), 0)
        val c = Calendar.getInstance()
        val mYear = c[Calendar.YEAR]
        val mMonth = c[Calendar.MONTH]
        val mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                var monthValue = ""
                monthValue = if (monthOfYear + 1 >= 10) {
                    (monthOfYear + 1).toString()
                } else {
                    "0" + (monthOfYear + 1).toString()
                }
                var dateValue = ""
                dateValue = if (dayOfMonth >= 10) {
                    dayOfMonth.toString()
                } else {
                    "0$dayOfMonth"
                }
                //2021-06-23
                var mActualDate: String = "" + year + "-" + monthValue + "-" + dateValue
                mSendDBDate = "" + year + "-" + monthValue + "-" + dateValue
                mAge = (mYear - year)
                mTextView.setText(mActualDate)

            }, mYear, mMonth, mDay
        )
        val currentDate = System.currentTimeMillis()
        datePickerDialog.datePicker.maxDate = currentDate
        datePickerDialog.show()
    }

    //    Bottom Sheet Dialog
    private fun showBottomSheetDialogue(mTextView: TextView) {
        val mBottomSheetDialog = BottomSheetDialog(mActivity)
        // on below line we are inflating a layout file which we have created.
        val view = layoutInflater.inflate(R.layout.dialog_bottom_sheet, null)
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.setContentView(view)
        mBottomSheetDialog.show()

        val txtMaleTV: TextView = view.findViewById(R.id.txtMaleTV)
        txtMaleTV.setOnClickListener {
            mGender = "Male"
            editGenderET.text = "Male"
            mBottomSheetDialog.dismiss()
        }
        val txtFemaleTV: TextView = view.findViewById(R.id.txtFemaleTV)
        txtFemaleTV.setOnClickListener {
            mGender = "Female"
            editGenderET.text = "Female"
            mBottomSheetDialog.dismiss()
        }
        val txtOtherTV: TextView = view.findViewById(R.id.txtOtherTV)
        txtOtherTV.setOnClickListener {
            mGender = "Other"
            editGenderET.text = "Other"
            mBottomSheetDialog.dismiss()
        }
    }
}
