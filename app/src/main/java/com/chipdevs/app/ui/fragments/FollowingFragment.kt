package com.chipdevs.app.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.chipdevs.app.R
import com.chipdevs.app.adapters.FollowingAdapter
import com.chipdevs.app.models.AddFollowUnfollowModel
import com.chipdevs.app.models.AllPeoplesModel
import com.chipdevs.app.models.Userfollowlist
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.FriendProfileActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FollowingFragment(
    var mOtherUserId: String,
    var followingListingGot: MutableList<Userfollowlist>
) : BaseFragment(),
    FollowingAdapter.followUnFollowUser {
    @BindView(R.id.noFollowingTV)
    lateinit var noFollowingTV: TextView
    @BindView(R.id.followingRV)
    lateinit var followingRV: RecyclerView
    lateinit var mUnbinder: Unbinder
    lateinit var followingAdapter: FollowingAdapter
    var followingListing = mutableListOf<Userfollowlist>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_following, container, false)
        mUnbinder = ButterKnife.bind(this, view)

        updateWidgets(followingListingGot)
        return view
    }

    private fun initRecyclerView() {


        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        followingRV.layoutManager = layoutManager
        followingAdapter = FollowingAdapter(this, followingListingGot, activity,getLoggedInUserID())
        followingRV.adapter = followingAdapter
    }




    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }

    override fun onFollowClick(
        position: Int,
        otherUserId: String,
        isfollow: String,
        btnFollowTV: TextView,
        status: String,
        changeStatus: String
    ) {


        if(btnFollowTV.text==getString(R.string.requested))
        {

            followTheFollowing(position, otherUserId, isfollow, btnFollowTV, status,changeStatus)
        }
        else
        {
            followTheFollowing(position, otherUserId, isfollow, btnFollowTV, status, changeStatus)
        }

    }

    fun followTheFollowing(
        position: Int,
        otherUserId: String,
        isfollow: String,
        btnFollowTV: TextView,
        status: String,
        changeStatus: String
    )
    {


        showProgressDialog(activity)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followUserID"] = otherUserId
        Log.e(TAG,mMap.toString())
        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.followRequest(mMap).enqueue(object : Callback<AddFollowUnfollowModel> {
            override fun onResponse(
                call: Call<AddFollowUnfollowModel>,
                response: Response<AddFollowUnfollowModel>
            ) {
                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1" && responseModel?.notificationDetails != null) {
                    Log.e(TAG, responseModel.notificationDetails.toString())

                    if (status=="1") {
                        btnFollowTV.text = getString(R.string.requested)

                        btnFollowTV.setBackground(
                            ContextCompat.getDrawable(
                                activity!!,
                                R.drawable.bg_description
                            )
                        )
                        btnFollowTV.setTextColor(resources.getColor(R.color.colorTextBlack))
                        followingListingGot[position].changeStatus="0"
                        ( activity as FriendProfileActivity).followersTv.text=followingListingGot.size.toString()+" Followers"

                    } else if (status == "0") {
                        btnFollowTV.text = getString(R.string.following)
                        btnFollowTV.setBackground(
                            ContextCompat.getDrawable(
                                activity!!,
                                R.drawable.bg_btn
                            )
                        )
                        btnFollowTV.setTextColor(resources.getColor(R.color.colorWhite))
                    }

                } else {
                    btnFollowTV.text = getString(R.string.follow)
                    btnFollowTV.setBackground(
                        ContextCompat.getDrawable(
                            activity!!,
                            R.drawable.bg_btn
                        )
                    )
                    btnFollowTV.setTextColor(resources.getColor(R.color.colorWhite))
                    followingListingGot[position].changeStatus="0"

                }
            }

            override fun onFailure(call: Call<AddFollowUnfollowModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }

    fun updateWidgets(followingListingGot: MutableList<Userfollowlist>)
    {
        if (followingListingGot.isEmpty()) {
            noFollowingTV.visibility = View.VISIBLE
            followingRV.visibility = View.GONE
        } else {

            noFollowingTV.visibility = View.GONE
            followingRV.visibility = View.VISIBLE
            initRecyclerView()
        }
    }
}