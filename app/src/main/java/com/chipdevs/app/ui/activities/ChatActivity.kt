package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.chat.ChatMessagesAdapter
import com.chipdevs.app.models.*
import com.chipdevs.app.utils.*
import com.chipdevs.app.viewModels.AddMessageViewModel
import com.chipdevs.app.viewModels.GetChatListingViewModel
import com.chipdevs.app.viewModels.UpdateMsgSeenViewModel
import com.chipdevs.app.models.AllMessagesItem
import com.chipdevs.app.models.GetChatListingModel
import com.google.gson.Gson
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONException
import org.json.JSONObject

import java.util.HashMap

class ChatActivity : BaseActivity() {

    /* Widgets...*/
    @BindView(R.id.backRL)
    lateinit var backRL: RelativeLayout

    @BindView(R.id.mSwipeRefreshSR)
    lateinit var mSwipeRefreshSR: SwipeRefreshLayout

    @BindView(R.id.txtOtherUserNameTV)
    lateinit var txtOtherUserNameTV: TextView

    @BindView(R.id.chatDataRV)
    lateinit var chatDataRV: RecyclerView

    @BindView(R.id.editMsgET)
    lateinit var editMsgET: EditText

    @BindView(R.id.sendRL)
    lateinit var sendRL: RelativeLayout

    @BindView(R.id.imgSendIV)
    lateinit var imgSendIV: ImageView

    @BindView(R.id.mProgressPB)
    lateinit var mProgressPB: com.wang.avi.AVLoadingIndicatorView

    /* Initialize objects...*/
    lateinit var mChatMessagesAdapter: ChatMessagesAdapter
    private var mAddMessageViewModel: AddMessageViewModel? = null
    private var mGetChatListingViewModel: GetChatListingViewModel? = null
    private var mUpdateMsgSeenViewModel: UpdateMsgSeenViewModel? = null
    var mMsgArrayList: ArrayList<AllMessagesItem?>? = ArrayList()
    var mPageNo: Int = 1
    var mIsPagination = true
    var mRoomNo = ""
    var mReceiverId = ""
    var fromNotification = ""
    private var mSocket: Socket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        ButterKnife.bind(this)

        mGetChatListingViewModel = ViewModelProviders.of(this).get(GetChatListingViewModel::class.java)
        mAddMessageViewModel = ViewModelProviders.of(this).get(AddMessageViewModel::class.java)
        mUpdateMsgSeenViewModel =
            ViewModelProviders.of(this).get(UpdateMsgSeenViewModel::class.java)

        getIntentData()
        setTopSwipeRefresh()
        setEditFocus()
        setDataOnWidgets()
    }

    /*
       * Set Edit Message Focus
       * */
    private fun setEditFocus() {
        editMsgET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                chatDataRV.scrollToPosition(0)
            }

            override fun afterTextChanged(editable: Editable) {
                chatDataRV.scrollToPosition(0)
            }
        })
    }

    private fun getIntentData() {
        if (intent != null) {
            mRoomNo = intent.getStringExtra("mRoomId").toString()
            mReceiverId = intent.getStringExtra("otherUserID").toString()
            fromNotification = intent.getStringExtra("fromNotification").toString()
            txtOtherUserNameTV.setText(intent.getStringExtra("mUsername")).toString()
            setUpSocketData()
        }

    }

    private fun setTopSwipeRefresh() {
        mSwipeRefreshSR.setColorSchemeColors(
            resources.getColor(R.color.colorYellow),
            resources.getColor(R.color.colorBlack),
            resources.getColor(R.color.colorRed)
        )
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true) {
                ++mPageNo
                getAllMsgData()
            } else {
                mSwipeRefreshSR.isRefreshing = false
            }
        }
    }


    @OnClick(
        R.id.backRL,
        R.id.sendRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.backRL -> performBackClick()
            R.id.sendRL -> performSendClick()
        }
    }

    private fun setUpSocketData() {
        val app: ChipsApp = application as ChipsApp
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", mRoomNo)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()
        getAllMsgData()
        showToast(this,"Connected")
    }

    fun performBackClick() {
        if (fromNotification == "true") {
            updateMsgSeenData()
            val i = Intent(mActivity, HomeActivity::class.java)
            BackStackManager.fragmentPosition = "home_tag"
            startActivity(i)
            finish()
        } else {
            updateMsgSeenData()
            finish()
        }
    }

    private fun getAllMsgData() {
        if (isNetworkAvailable(mActivity))
            executeGetAllMsgRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mGetMsgParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["room_id"] = mRoomNo
        mMap["page_no"] = mPageNo.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMsgRequest() {
        mSwipeRefreshSR.isRefreshing = true
        mGetChatListingViewModel?.sendGetChatListingData(mActivity, mGetMsgParam())?.observe(this,
            androidx.lifecycle.Observer<GetChatListingModel?> { mGetChatListingModel ->
                if (mGetChatListingModel.status == 1) {
                    mSwipeRefreshSR.isRefreshing = false
                    if (mGetChatListingModel.allMessages!!.size < 10) {
                        mIsPagination = false;
                    }
                    for (i in 0..mGetChatListingModel.allMessages!!.size - 1) {
                        if (mGetChatListingModel.allMessages!!.get(i)!!.senderId.equals(getLoggedInUserID()))
                        {
                            mGetChatListingModel.allMessages!!.get(i)!!.viewType = RIGHT_VIEW_HOLDER
                        } else {
                            mGetChatListingModel.allMessages!!.get(i)!!.viewType = LEFT_VIEW_HOLDER
                        }
                    }
                    mMsgArrayList!!.addAll(mGetChatListingModel.allMessages!!)
                    mMsgArrayList!!.sortedWith(compareBy({ it!!.creationDate }))
                    mChatMessagesAdapter.notifyDataSetChanged()
                    if (mPageNo == 1) {
                        chatDataRV.scrollToPosition(0)
                    }
                    updateMsgSeenData()
                } else {
                    mSwipeRefreshSR.isRefreshing = false
                    mChatMessagesAdapter.notifyDataSetChanged()
                }
            }
        )
    }

    /*
   * Set up validations for Chat
   * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editMsgET.text.toString().trim { it <= ' ' } == "") {
            flag = false
        }
        return flag
    }

    private fun performSendClick() {
        if (isNetworkAvailable(mActivity)) {
            if (isValidate()) {
                executeAddMsgRequest();
            }
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }

    private fun mAddMsgParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["sender_id"] = getLoggedInUserID()
        mMap["message"] = editMsgET.text.toString().trim()
        mMap["room_id"] = mRoomNo
        mMap["receiver_id"] = mReceiverId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddMsgRequest() {
        mProgressPB.visibility = View.VISIBLE
        imgSendIV.visibility = View.GONE
        editMsgET.isEnabled = false
        mSwipeRefreshSR.isRefreshing = false
        mAddMessageViewModel?.sendAddMessageData(mActivity, mAddMsgParam())?.observe(this,
            androidx.lifecycle.Observer<AddMessageModel?> { mAddMessageModel ->
                if (mAddMessageModel.status == 1) {
                    mProgressPB.visibility = View.GONE
                    imgSendIV.visibility = View.VISIBLE
                    editMsgET.isEnabled = true
                    mAddMessageModel.data!!.viewType = RIGHT_VIEW_HOLDER
                    mMsgArrayList!!.add(0, mAddMessageModel.data)
                    mChatMessagesAdapter.notifyDataSetChanged()
                    scrollToBottom()
                    // perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(mAddMessageModel.data)
                    var obj: JSONObject? = null
                    try {
                        obj = JSONObject(mOjectString)
                        Log.d("My App", obj.toString())
                    } catch (t: Throwable) {
                        Log.e(
                            "My App",
                            "Could not parse malformed JSON: \"" + mOjectString.toString() + "\""
                        )
                    }
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    Log.e(TAG, "****mOjectString****" + mRoomNo)
                    Log.e(TAG, "*****MsgText****" + editMsgET.getText().toString())
                    mSocket!!.emit("newMessage", mRoomNo, obj)
                    editMsgET.setText("")
                } else {
                    mProgressPB.visibility = View.GONE
                    imgSendIV.visibility = View.VISIBLE
                    editMsgET.isEnabled = true
                    showToast(mActivity, mAddMessageModel.message)

                }
            }
        )
    }

    private fun updateMsgSeenData() {
        if (isNetworkAvailable(mActivity))
            executeUpdateMsgSeenRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mUpdateMsgSeenParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["room_id"] = mRoomNo
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeUpdateMsgSeenRequest() {
        mUpdateMsgSeenViewModel?.sendUpdateMessageSeenData(mActivity, mUpdateMsgSeenParam())
            ?.observe(this,
                androidx.lifecycle.Observer<UpdateMessageSeenModel?> { mUpdateMessageSeenModel ->
                    if (mUpdateMessageSeenModel.status == 1) {
                        Log.e(TAG, "*****Msg****" + mUpdateMessageSeenModel)
                        Log.e(TAG, "*****Msg****" + "Msg Seen")
                    } else {
                        showToast(mActivity, mUpdateMessageSeenModel.message)
                    }
                }
            )
    }

    private fun setDataOnWidgets() {
        mChatMessagesAdapter = ChatMessagesAdapter(mActivity, mMsgArrayList)
        chatDataRV.setNestedScrollingEnabled(false)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        chatDataRV.layoutManager = linearLayoutManager
        chatDataRV.setItemAnimator(DefaultItemAnimator())
        chatDataRV.setAdapter(mChatMessagesAdapter)

        if (editMsgET.hasFocus()) {
            scrollToBottom()
        }
    }


    private fun scrollToBottom() {
        if (mMsgArrayList != null) {
            chatDataRV.scrollToPosition(0)
        }
    }

    /*
     * Socket Chat Implementations:
     * */

    private val onConnect = Emitter.Listener {

        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                Log.e(TAG, "****MessageObject****" + args.get(1).toString())
                val mGson = Gson()

                val mChatMessageData: AllMessagesItem = mGson.fromJson(args.get(1).toString(), AllMessagesItem::class.java)
                Log.e(TAG, "**getLoggedInUserID**" + getLoggedInUserID() + "receiverId" + mChatMessageData.receiverId)
                if (mChatMessageData.senderId!!.equals(getLoggedInUserID())) {
                    mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                } else {
                    mChatMessageData.viewType = LEFT_VIEW_HOLDER
                }

                for (i in 1..mMsgArrayList!!.size) {
                    if (mMsgArrayList!!.get(i)!!.username != null && mMsgArrayList!!.get(i)!!.profilePic != null) {
                        mChatMessageData.username = mMsgArrayList!!.get(i)!!.username
                        mChatMessageData.profilePic = mMsgArrayList!!.get(i)!!.profilePic
                        break
                    }
                }

                mMsgArrayList!!.add(0, mChatMessageData)
                mChatMessagesAdapter.notifyDataSetChanged()
                scrollToBottom()
                showToast(this,"CAlled")
               // updateMsgSeenData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
                scrollToBottom()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }

    override fun onBackPressed() {
        performBackClick()
    }

}
