package com.chipdevs.app.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.chipdevs.app.R
import com.chipdevs.app.adapters.FriendsListAdapter
import com.chipdevs.app.models.FriendsItem
import com.chipdevs.app.models.GetProfileDetailsModel
import com.chipdevs.app.ui.activities.ChatUserActivity
import com.chipdevs.app.ui.activities.SettingsActivity
import com.chipdevs.app.viewModels.GetProfileDetailsViewModel
import java.util.*

class ProfileFragment : BaseFragment() {
    /*
    * Initialize...
    * */
    @BindView(R.id.friendsRV)
    lateinit var friendsRV: RecyclerView

    @BindView(R.id.txtChipsAvailableTV)
    lateinit var txtChipsAvailableTV: TextView

    @BindView(R.id.txtPlayerRankingTV)
    lateinit var txtPlayerRankingTV: TextView

    @BindView(R.id.txtFirstNameTV)
    lateinit var txtFirstNameTV: TextView

    @BindView(R.id.txtLastNameTV)
    lateinit var txtLastNameTV: TextView

    @BindView(R.id.txtUsernameTV)
    lateinit var txtUsernameTV: TextView

    @BindView(R.id.txtDobTV)
    lateinit var txtDobTV: TextView

    @BindView(R.id.txtGenderTV)
    lateinit var txtGenderTV: TextView

    @BindView(R.id.txtGradeTV)
    lateinit var txtGradeTV: TextView

    @BindView(R.id.txtCashTAgTV)
    lateinit var txtCashTagET: TextView

    lateinit var mUnbinder: Unbinder
    var friendsListAdapter: FriendsListAdapter? = null
    private var mGetProfileDetailsViewModel: GetProfileDetailsViewModel? = null
    var mArrayList: ArrayList<FriendsItem?>? = ArrayList<FriendsItem?>()
    var mGetProfileDetailsModel: GetProfileDetailsModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        mGetProfileDetailsViewModel =
            ViewModelProviders.of(this).get(GetProfileDetailsViewModel::class.java)

        return view
    }

    @OnClick(
        R.id.imgSettingsIV,
        R.id.imgChatIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgSettingsIV -> performSettingsClick()
            R.id.imgChatIV -> performChatClick()

        }
    }

    private fun getProfileDetails() {
        if (activity?.let { isNetworkAvailable(it) }!!) {
            executeGetProfileDetailsRequest()
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userid"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailsRequest() {
        showProgressDialog(activity)
        activity?.let {
            mGetProfileDetailsViewModel?.getProfileData(activity, mParams())?.observe(
                it,
                androidx.lifecycle.Observer<GetProfileDetailsModel?> { mGetProfileDetailsModel ->
                    if (mGetProfileDetailsModel.status!!.equals("1")) {
                        dismissProgressDialog()
                        mArrayList = mGetProfileDetailsModel.friends as ArrayList<FriendsItem?>?
                        mArrayList?.reverse()
                        setAdapter()
                        setDataOnWidgets(mGetProfileDetailsModel)
                    } else {
                        dismissProgressDialog()
                    }
                })
        }
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        friendsRV.setLayoutManager(layoutManager)
        friendsListAdapter = FriendsListAdapter(activity, mArrayList)
        friendsRV.setAdapter(friendsListAdapter)
    }

    //        Set Data on Widgets
    fun setDataOnWidgets(mGetProfileDetailsModel: GetProfileDetailsModel) {

        txtPlayerRankingTV.text = mGetProfileDetailsModel.playerRanking
        txtChipsAvailableTV.text = mGetProfileDetailsModel.chipsAvailable
        txtFirstNameTV.text = mGetProfileDetailsModel.userDetails?.firstName
        txtLastNameTV.text = mGetProfileDetailsModel.userDetails?.lastName
        txtUsernameTV.text = mGetProfileDetailsModel.userDetails?.userName
        if(mGetProfileDetailsModel.userDetails?.cashTag!!.isEmpty())
        {
            txtCashTagET.text = "N/A"

        }
        else

        {
            txtCashTagET.text = mGetProfileDetailsModel.userDetails?.cashTag

        }
        txtDobTV.text = mGetProfileDetailsModel.userDetails?.dob
        txtGenderTV.text = mGetProfileDetailsModel.userDetails?.gender
        txtGradeTV.text = mGetProfileDetailsModel.userDetails?.grade

    }


    private fun performChatClick() {
        val intent = Intent(activity, ChatUserActivity::class.java)
        startActivity(intent)
    }

    private fun performSettingsClick() {
        val intent = Intent(activity, SettingsActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        getProfileDetails()
    }
}