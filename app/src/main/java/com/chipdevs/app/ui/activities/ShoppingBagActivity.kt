package com.chipdevs.app.ui.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.ShoppingBagAdapter
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import kotlinx.android.synthetic.main.activity_shopping_bag.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ShoppingBagActivity : BaseActivity(), ShoppingBagAdapter.onRemoveItem {
    lateinit var shoppingBagAdapter: ShoppingBagAdapter
    var allProductListing = mutableListOf<CartDetails>()

    var quantityTV: TextView? = null

    @BindView(R.id.shoppingBagRV)
    lateinit var shoppingBagRV: RecyclerView

    @BindView(R.id.noItemsInCart)
    lateinit var noItemsInCart: TextView

    @BindView(R.id.placeOrderTv)
    lateinit var placeOrderTv: TextView

    @BindView(R.id.placeOrderParentLL)
    lateinit var placeOrderParentLL: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_bag)
        ButterKnife.bind(this)
        getAllBagDetails()

    }

    @OnClick(
        R.id.imgBackRL,
        R.id.imgWishListIV,
        R.id.placeOrderTv
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> performBackClick()
            R.id.imgWishListIV -> performWishListClick()
            R.id.placeOrderTv -> performPlaceOrderClick()
        }
    }


    private fun performBackClick() {


        val i = Intent(mActivity, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)

    }

    private fun performWishListClick() {
        val i = Intent(mActivity, WishListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(i)

    }

    private fun performPlaceOrderClick() {


        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.placeOrderRequest(mMap).enqueue(object : Callback<SubmitAnswerModel> {
            override fun onResponse(
                call: Call<SubmitAnswerModel>,
                response: Response<SubmitAnswerModel>
            ) {
                dismissProgressDialog()
                val responseModel = response?.body()
                Log.e(TAG, responseModel.toString())
                if (responseModel?.status == "1") {

                    showPlaceOrderAlertDialog(this@ShoppingBagActivity, responseModel.message)

                } else {
                    showAlertDialog(
                        this@ShoppingBagActivity,
                        "Insufficient chips balance, please add chips to continue."
                    )
                }

            }

            override fun onFailure(call: Call<SubmitAnswerModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }

    private fun initRecyclerView() {

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        shoppingBagRV.layoutManager = layoutManager
        shoppingBagAdapter = ShoppingBagAdapter(this, allProductListing)
        shoppingBagRV.adapter = shoppingBagAdapter
    }


    fun getAllBagDetails() {
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.cartItemDetails(mMap).enqueue(object : Callback<CartDetailsModel> {
            override fun onResponse(
                call: Call<CartDetailsModel>,
                response: Response<CartDetailsModel>
            ) {

                dismissProgressDialog()
                if (response.body()?.status == "1") {
                    Log.e(TAG, response.body().toString())
                    response.body()?.data?.let { allProductListing.addAll(it) }
                    totalOfChipsTv.text = response?.body()?.totalprice
                    initRecyclerView()
                    updateWidgets()
                    Log.e(TAG, response.body().toString())
                    noItemsInCart.visibility = View.GONE
                    shoppingBagRV.visibility = View.VISIBLE
                } else {
                    updateWidgets()
                    noItemsInCart.visibility = View.VISIBLE
                    shoppingBagRV.visibility = View.GONE
                }


            }

            override fun onFailure(call: Call<CartDetailsModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }


    override fun onRemoveClick(position: Int, cartID: String, txtNumberTV: TextView) {
        quantityTV = txtNumberTV
        removeItemFromCart(position, cartID)
    }

    override fun moveToWishListItem(
        cartID: String,
        productID: String,
        position: Int,
        txtNumberTV: TextView
    ) {
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["cartID"] = cartID
        mMap["productId"] = productID

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.addToWishList(mMap).enqueue(object : Callback<AddToWishListModel> {
            override fun onResponse(
                call: Call<AddToWishListModel>,
                response: Response<AddToWishListModel>
            ) {
                dismissProgressDialog()
                if (response.body()?.status == "1") {

                    val decreasePrice = allProductListing[position].chip_price.toInt()

                    allProductListing.removeAt(position)
                    removeItemFromCart(position, cartID)
                    shoppingBagRV.adapter?.notifyDataSetChanged()
                    if (allProductListing.size == 0) {
                        shoppingBagRV.visibility = View.GONE
                        noItemsInCart.visibility = View.VISIBLE
                        placeOrderParentLL.visibility = View.GONE

                    } else {
                        var newQuantity =
                            (shoppingBagRV.layoutManager?.findViewByPosition(position))?.findViewById<TextView>(
                                R.id.txtNumberTV
                            )?.text.toString()
                        totalOfChipsTv.text =
                            (((totalOfChipsTv.text.toString()).toInt()) - decreasePrice * (newQuantity.toInt())).toString()
                    }

                } else if (response.body()?.status == "0") {
                    showAlertDialog(mActivity, "Item is already in wishlist.")
                }
            }

            override fun onFailure(call: Call<AddToWishListModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })
    }


    override fun increasePrice(position: String?, type: String) {
        if (position != null) {

            if (type == "1") {
                totalOfChipsTv.text =
                    (((totalOfChipsTv.text.toString()).toInt()) + position.toInt()).toString()
            } else {
                totalOfChipsTv.text =
                    (((totalOfChipsTv.text.toString()).toInt()) - position.toInt()).toString()
            }

        }
    }

    override fun getLatestQuantity(position: Int, quantity: String) {

    }


    fun removeItemFromCart(position: Int, cartID: String?) {


        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["cartID"] = cartID

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.removeItemFromCart(mMap).enqueue(object : Callback<RemoveItemFromCartModel> {
            override fun onResponse(
                call: Call<RemoveItemFromCartModel>,
                response: Response<RemoveItemFromCartModel>
            ) {

                dismissProgressDialog()
                if (response.body()?.status == 1) {
                    Log.e(TAG, response.body().toString())
                    val decreasePrice = allProductListing[position].chip_price.toInt()
                    allProductListing.removeAt(position)
                    shoppingBagRV.adapter?.notifyDataSetChanged()
                    if (allProductListing.size == 0) {
                        shoppingBagRV.visibility = View.GONE
                        noItemsInCart.visibility = View.VISIBLE
                        placeOrderParentLL.visibility = View.GONE
                    } else {

                        var newQuantity = (shoppingBagRV.layoutManager?.findViewByPosition(position))?.findViewById<TextView>(
                                R.id.txtNumberTV
                            )?.text.toString()

                        totalOfChipsTv.text = (((totalOfChipsTv.text.toString()).toInt()) - decreasePrice * (newQuantity.toInt())).toString()
                    }


                }
            }

            override fun onFailure(call: Call<RemoveItemFromCartModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }

    fun updateWidgets() {
        if (allProductListing.isEmpty()) {
            placeOrderParentLL.visibility = View.GONE
        } else {
            placeOrderParentLL.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        performBackClick()
    }
    /*
     *
     * Error Alert Dialog
     * */
    fun showPlaceOrderAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            performBackClick()
        }
        alertDialog.show()
    }
}