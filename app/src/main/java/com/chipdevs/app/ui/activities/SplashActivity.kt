package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import com.chipdevs.app.R
import com.chipdevs.app.models.CustomNotificationModel
import io.branch.referral.Branch

class SplashActivity : BaseActivity() {

    var notificationListing: CustomNotificationModel? = null
    val SPLASH_TIME_OUT = 1500L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    private fun setUpBrachIOs() {
        val branch = Branch.getInstance()
        branch.initSession({ branchUniversalObject, linkProperties, error ->
            if (error == null) {
                if (linkProperties != null) {
                    if (isLogin()) {
                        getNotificationData()
                    } else {
                        val mIntent = Intent(mActivity, SignUpActivity::class.java)
                        mActivity.startActivity(mIntent)
                        finish()
                    }
                } else {
                    getNotificationData()
                }
            } else {
                getNotificationData()
            }
        }, this.intent.data, this)

    }


    private fun getNotificationData() {

        if (intent.hasExtra("Data")) {

            notificationListing = intent.getSerializableExtra("Data") as CustomNotificationModel

            if (isLogin()) {
                val fromNotification = "true"

                if (notificationListing?.notification_type == "4") {
                    val i = Intent(mActivity, ChatActivity::class.java)
                    i.putExtra("fromNotification", fromNotification).toString()
                    i.putExtra("mRoomId", notificationListing?.room_id)
                    i.putExtra("mUsername", notificationListing?.username)
                    i.putExtra("otherUserID", notificationListing?.otherID)
                    startActivity(i)
                    finish()

                } else
                    if (notificationListing?.notification_type == "1" || notificationListing?.notification_type == "0" ||
                        notificationListing?.notification_type == "2" || notificationListing?.notification_type == "3"
                    ) {
                        val notifIntent = Intent(mActivity, NotificationActivity::class.java)
                        notifIntent.putExtra("fromNotification", fromNotification)
                        startActivity(notifIntent)
                        finish()

                    }

            } else {
                val i = Intent(mActivity, SignInActivity::class.java)
                startActivity(i)
                finish()
            }

        } else {
            callToSplash()
        }


    }

    private fun callToSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT)
                if (isLogin()) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()

                } else {
                    val i = Intent(mActivity, SignInActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        this.intent = intent
    }

    override fun onStart() {
        super.onStart()
        setUpBrachIOs()
    }
}