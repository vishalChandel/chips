package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.models.ChangePwdModel
import com.chipdevs.app.viewModels.ChangePwdViewModel
import java.util.*

class ChangePwdActivity : BaseActivity() {

    //  Widgets
    @BindView(R.id.editOldPwdET)
    lateinit var editOldPwdET: EditText

    @BindView(R.id.editNewPwdET)
    lateinit var editNewPwdET: EditText

    @BindView(R.id.editConfirmNewPwdET)
    lateinit var editConfirmNewPwdET: EditText

    @BindView(R.id.btnSubmitTV)
    lateinit var btnSubmitTV: TextView

    private var mChangePwdViewModel: ChangePwdViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pwd)
        ButterKnife.bind(this)
        setEditTextFocused(mActivity, editConfirmNewPwdET)
        mChangePwdViewModel = ViewModelProviders.of(this).get(ChangePwdViewModel::class.java)
    }

    @OnClick(
        R.id.imgBackRL,
        R.id.btnSubmitTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> onBackPressed()
            R.id.btnSubmitTV -> performSubmitClick()
        }
    }



    fun performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeChangePwdRequest()
            else
                showToast(mActivity, getString(R.string.internal_server_error))
        }
    }
    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["old_password"] = editOldPwdET.text.toString()
        mMap["new_password"] = editNewPwdET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeChangePwdRequest() {
        showProgressDialog(mActivity)
        mChangePwdViewModel?.sendChangePwdData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<ChangePwdModel?> { mChangePwdModel ->
                if (mChangePwdModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mChangePwdModel.message)
                    val i = Intent(mActivity, SettingsActivity::class.java)
                    startActivity(i)
                    finish()
                    editOldPwdET.setText("")
                    editNewPwdET.setText("")
                    editConfirmNewPwdET.setText("")
                } else {
                    showAlertDialog(mActivity, mChangePwdModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    /*
  * Set up validations for Change Password fields
  * */
    fun isValidate(): Boolean {
        var flag = true
        if (editOldPwdET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_old_pwd))
            flag = false
        } else if (editOldPwdET.text.toString().length < 4) {
            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
            flag = false
        } else if (editNewPwdET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_new_pwd))
            flag = false
        } else if (editNewPwdET.text.toString().length < 4) {
            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
            flag = false
        } else if (editConfirmNewPwdET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_re_enter_password))
            flag = false
        } else if (!editNewPwdET!!.text.toString().trim()
                .equals(editConfirmNewPwdET!!.text.toString().trim())
        ) {
            showAlertDialog(mActivity, getString(R.string.the_new_re_typed_pwd))
            flag = false
        }
        return flag
    }
}