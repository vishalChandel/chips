package com.chipdevs.app.ui.activities

import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.utils.*
import com.chipdevs.app.viewModels.AccountStatusViewModel
import com.chipdevs.app.viewModels.GetProfileDetailsViewModel
import com.chipdevs.app.viewModels.InviteAndEarnViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import io.branch.referral.util.ContentMetadata
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.util.LinkProperties


class SettingsActivity : BaseActivity() {
    @BindView(R.id.txtProfileNameTV)
    lateinit var txtProfileNameTV: TextView

    @BindView(R.id.txtProfileEmailTV)
    lateinit var txtProfileEmailTV: TextView

    @BindView(R.id.profileIV)
    lateinit var profileIV: ImageView

    @BindView(R.id.switchSC)
    lateinit var switchSC: SwitchCompat

    private var mGetProfileDetailsViewModel: GetProfileDetailsViewModel? = null
    private var mInviteAndEarnViewModel: InviteAndEarnViewModel? = null

    var accountStatus: String? = null
    private var mAccountStatusViewModel: AccountStatusViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        ButterKnife.bind(this)
        mGetProfileDetailsViewModel =
            ViewModelProviders.of(this).get(GetProfileDetailsViewModel::class.java)
        mAccountStatusViewModel =
            ViewModelProviders.of(this).get(AccountStatusViewModel::class.java)
        mInviteAndEarnViewModel =
            ViewModelProviders.of(this).get(InviteAndEarnViewModel::class.java)
        executeGetProfileDetailsRequest()
        accountStatus = getStatus()
        switchSC.isClickable = false
        setStatusOnSwitchButton()
    }

    private fun setStatusOnSwitchButton() {
        if (accountStatus == "0") {
            switchSC.isChecked = false
        } else if (accountStatus == "1") {
            switchSC.isChecked = true
        }
    }

    @OnClick(
        R.id.imgEditProfileIV,
        R.id.imgBackIV,
        R.id.changePwdRL,
        R.id.aboutRL,
        R.id.privacyPolicyRL,
        R.id.contactUsRL,
        R.id.searchFriendRL,
        R.id.logoutRL,
        R.id.privateAccountRL,
        R.id.inviteEarnRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgEditProfileIV -> performEditProfileClick()
            R.id.imgBackIV -> onBackPressed()
            R.id.changePwdRL -> performChangePwdClick()
            R.id.aboutRL -> performAboutClick()
            R.id.privacyPolicyRL -> performPrivacyPolicyClick()
            R.id.contactUsRL -> performContactUsClick()
            R.id.searchFriendRL -> performSearchClick()
            R.id.logoutRL -> performLogoutClick()
            R.id.privateAccountRL -> performPrivateAccountClick()
            R.id.inviteEarnRL -> executeInviteAndEarnApi()
        }
    }

    private fun mGetProfileParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userid"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailsRequest() {
        showProgressDialog(mActivity)
        mGetProfileDetailsViewModel?.getProfileData(mActivity, mGetProfileParams())?.observe(this,
            androidx.lifecycle.Observer<GetProfileDetailsModel?> { mGetProfileData ->
                if (mGetProfileData.status == "1") {
                    dismissProgressDialog()
                    setDataOnWidgets(mGetProfileData)
                    AppPrefrences().writeString(
                        mActivity,
                        USERNAME,
                        mGetProfileData.userDetails?.userName
                    )
                } else {
                    showAlertDialog(mActivity, mGetProfileData.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    fun setDataOnWidgets(mGetProfileModel: GetProfileDetailsModel) {
//      Set Data on Widgets
        txtProfileNameTV.setText(mGetProfileModel?.userDetails?.firstName + " " + mGetProfileModel?.userDetails?.lastName)
        txtProfileEmailTV.text = getEmail()
        Glide
            .with(this)
            .load(mGetProfileModel.userDetails?.image)
            .centerCrop()
            .placeholder(R.drawable.ic_profile)
            .into(profileIV)
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["status"] = accountStatus.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAccountStatus(accountStatus: String) {
        showProgressDialog(mActivity)
        mAccountStatusViewModel?.sendAccountStatusData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<AccountStatusModel?> { mAccountStatusModel ->
                if (mAccountStatusModel.status == 1) {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mAccountStatusModel.message)
                    AppPrefrences().writeString(mActivity, STATUS, accountStatus)
                    setStatusOnSwitchButton()

                } else {
                    showAlertDialog(mActivity, mAccountStatusModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    private fun performPrivateAccountClick() {
        if (accountStatus == "1") {
            accountStatus = "0"
            executeAccountStatus(accountStatus!!)
            switchSC.isChecked = false

        } else {
            accountStatus = "1"
            executeAccountStatus(accountStatus!!)
            switchSC.isChecked = true

        }

    }

    private fun performPrivacyPolicyClick() {
        val i = Intent(mActivity, WebviewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performAboutClick() {
        val i = Intent(mActivity, WebviewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_ABOUT)
        startActivity(i)
    }

    private fun performEditProfileClick() {
        val i = Intent(mActivity, EditProfileActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun performChangePwdClick() {
        val i = Intent(mActivity, ChangePwdActivity::class.java)
        startActivity(i)
    }

    private fun performContactUsClick() {
        val i = Intent(mActivity, ContactUsActivity::class.java)
        startActivity(i)
    }

    private fun performSearchClick() {
        val i = Intent(mActivity, SearchActivity::class.java)
        startActivity(i)
    }

    private fun performLogoutClick() {
        showLogoutConfirmAlertDialog()
    }

    /*
    *
    * Logout Alert Dialog
    * */
    fun showLogoutConfirmAlertDialog() {
        val alertDialog = mActivity.let { Dialog(it) }
        alertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()

            logoutRequest()

        }
        alertDialog.show()
    }

    private fun clearSharedPrefenceData() {
        val preferences: SharedPreferences = AppPrefrences().getPreferences(applicationContext)
        val editor = preferences.edit()
        editor.remove(IS_LOGIN)
        editor.remove(USERID)
        editor.remove(FIRSTNAME)
        editor.remove(EMAIL)
        editor.remove(PROFILEPIC)
        editor.remove(LASTNAME)
        editor.remove(USERNAME)
        editor.remove(DEVICE_TOKEN)
       // editor.clear()
        editor.apply()
        editor.commit()

        val mIntent = Intent(mActivity, SignInActivity::class.java)
        TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        mActivity?.startActivity(mIntent)
        mActivity?.finish()
        mActivity?.finishAffinity()
    }

    fun logoutRequest()
    {

        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()

        val logoutCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        logoutCall.logoutRequest(mMap).enqueue(object :Callback<UpdateMessageSeenModel>
        {
            override fun onResponse(
                call: Call<UpdateMessageSeenModel>,
                response: Response<UpdateMessageSeenModel>
            ) {
                if(response?.body()?.status==1)
                {
                    Log.e(TAG,response?.body()?.message.toString())
                    clearSharedPrefenceData()
                    BackStackManager.fragmentPosition = ""
                }
                else
                {
                    showToast(this@SettingsActivity,response?.body()?.message)
                }

            }

            override fun onFailure(call: Call<UpdateMessageSeenModel>, t: Throwable) {

            }

        })

    }

    private fun mInviteAndEarnParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        Log.e("InviteParams", "**PARAM**$mMap")
        return mMap
    }

    private fun executeInviteAndEarnApi() {
        showProgressDialog(mActivity)
        mInviteAndEarnViewModel?.sendInviteAndEarnData(mActivity, mInviteAndEarnParams())?.observe(this,
            androidx.lifecycle.Observer<InviteAndEarnModel?> { mInviteAndEarnModel ->
                if (mInviteAndEarnModel.status == "1") {
                    dismissProgressDialog()
                    shareAppUrl(mInviteAndEarnModel?.url,mInviteAndEarnModel?.shareMessage.toString(),mInviteAndEarnModel?.referCode.toString())
                } else {
                    showAlertDialog(mActivity, mInviteAndEarnModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

    fun shareAppUrl(mUrl: String?, mMsg: String,mReferCode:String){
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        var shareMessage = mMsg + "\nReferral Code: $mReferCode" + "\n$mUrl"
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
        startActivity(Intent.createChooser(shareIntent, "Chips"))
    }
}

