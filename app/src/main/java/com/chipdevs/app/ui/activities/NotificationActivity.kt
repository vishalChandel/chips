package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.NotificationAdapter
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.utils.BackStackManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : BaseActivity(), NotificationAdapter.acceptRejectAction {
    lateinit var notificationAdapter: NotificationAdapter
    var notificationRV: RecyclerView? = null
    var noNotificationTv: TextView? = null
    var notificationListing = mutableListOf<NotificationData>()
    var notificationArrayListing = arrayListOf<MutableList<NotificationData>>()
    var followAcceptDetails: NotificationDetails? = null
    var fromNotification = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        ButterKnife.bind(this)
        initComponents()
        getAllNotificationData()
       // setDataOnWidgets()

    }

    @OnClick(
        R.id.imgBackRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> updateTheNotificationStatus()
        }
    }

    private fun updateTheNotificationStatus() {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.updateReadNotificationRequest(mMap).enqueue(object : Callback<UpdateMessageSeenModel> {
            override fun onResponse(
                call: Call<UpdateMessageSeenModel>,
                response: Response<UpdateMessageSeenModel>
            ) {
                if (response.body()?.status.toString() == "1") {
                    response.body()?.message?.let { Log.e(TAG, "NOTIFUPDATE" + it) }
                }

                getBackToHome()

            }

            override fun onFailure(call: Call<UpdateMessageSeenModel>, t: Throwable) {
            }

        })


    }

    private fun getBackToHome() {
        if (fromNotification == "true") {
            val i = Intent(mActivity, HomeActivity::class.java)
            BackStackManager.fragmentPosition = "home_tag"
            startActivity(i)
            finish()
        } else {
            finish()
        }
    }

    private fun initComponents() {


        notificationRV = findViewById(R.id.notificationRV)
        noNotificationTv = findViewById(R.id.noNotificationTv)

        if (intent != null) {
            fromNotification = intent.extras?.getString("fromNotification").toString()
            Log.e("FromNotification", fromNotification)
        }

    }

    private fun initRecyclerView() {

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        notificationRV?.layoutManager = layoutManager
        notificationAdapter = NotificationAdapter(this,notificationListing)
        notificationRV?.adapter = notificationAdapter
    }


    fun getAllNotificationData() {
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()

        val notificationCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        notificationCall.getNotificationsListRequest(mMap)
            .enqueue(object : Callback<NotificationModel> {
                override fun onResponse(
                    call: Call<NotificationModel>,
                    response: Response<NotificationModel>
                ) {

                    val responseModel = response.body()
                    if (responseModel?.status == 1) {
                        dismissProgressDialog()
                        Log.e(TAG, responseModel.data.toString())
                        notificationListing.addAll(responseModel.data)

                        if (notificationListing.isEmpty()) {
                            noNotificationTv?.visibility = View.VISIBLE
                            notificationRV?.visibility = View.GONE
                        } else {
                            initRecyclerView()
                            noNotificationTv?.visibility = View.GONE
                            notificationRV?.visibility = View.VISIBLE

                        }

                    } else {
                        dismissProgressDialog()
                        noNotificationTv?.visibility = View.VISIBLE
                        notificationRV?.visibility = View.GONE
                        Log.e(TAG, responseModel?.message.toString())
                    }
                }

                override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                    dismissProgressDialog()
                }

            })


    }

    fun setDataOnWidgets() {


        if (notificationArrayListing[0].isEmpty()) {
            noNotificationTv?.visibility = View.VISIBLE
            notificationRV?.visibility = View.GONE
        } else {
            initRecyclerView()
            noNotificationTv?.visibility = View.GONE
            notificationRV?.visibility = View.VISIBLE

        }


    }

    override fun onBtnClick(
        position: Int,
        followId: String,
        userId: String,
        followRequestStatus: String
    ) {
        acceptRejectFollowerRequest(followId, userId, followRequestStatus, position)
    }


    fun acceptRejectFollowerRequest(
        followId: String,
        otherUserId: String,
        followRequestStatus: String,
        position: Int
    ) {
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followUserID"] = otherUserId
        mMap["followID"] = followId
        mMap["appoveRejectFollow"] = followRequestStatus
        Log.e(TAG, mMap.toString())

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.acceptRejectFollowRequest(mMap).enqueue(
            object : Callback<AddFollowUnfollowModel> {
                override fun onResponse(
                    call: Call<AddFollowUnfollowModel>,
                    response: Response<AddFollowUnfollowModel>
                ) {
                    dismissProgressDialog()
                    var responseModel = response.body()
                    if (responseModel?.status == "1") {
                        showAlertDialog(this@NotificationActivity, responseModel.message)
                        followAcceptDetails = responseModel.notificationDetails
                        deleteNotification(notificationListing[position].notification_id)
                        notificationListing.removeAt(position)
                        notificationRV?.adapter?.notifyDataSetChanged()
                    }

                }

                override fun onFailure(call: Call<AddFollowUnfollowModel>, t: Throwable) {
                    dismissProgressDialog()
                }

            }
        )

    }


    fun deleteNotification(notificationId: String) {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["notificationID"] = notificationId

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.deleteFollowNotification(mMap).enqueue(object : Callback<DeleteNotifModel> {
            override fun onResponse(
                call: Call<DeleteNotifModel>,
                response: Response<DeleteNotifModel>
            ) {
                if (response.body()?.status == "1") {
                    Log.e(TAG, response.body()?.message.toString())
                }
            }

            override fun onFailure(call: Call<DeleteNotifModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

        })

    }

    override fun onBackPressed() {
        updateTheNotificationStatus()
    }
}