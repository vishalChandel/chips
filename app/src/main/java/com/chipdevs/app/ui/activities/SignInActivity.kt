package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.models.SignInModel
import com.chipdevs.app.utils.*
import com.chipdevs.app.viewModels.SignInViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging


class SignInActivity : BaseActivity() {
    /*
   * Widgets...
   * */
    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.txtForgotPwdTV)
    lateinit var txtForgotPwdTV: TextView

    @BindView(R.id.btnLoginTV)
    lateinit var btnLoginTV: TextView

    @BindView(R.id.txtSignUpTV)
    lateinit var txtSignUpTV: TextView

    /*
     * Initialize Objects...
     */
    var mDeviceToken: String = ""

    private var mSignInViewModel: SignInViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        ButterKnife.bind(this)
       getDeviceToken()
        mSignInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        setEditTextFocused(mActivity, editPasswordET)
    }

    @OnClick(
        R.id.txtForgotPwdTV,
        R.id.btnLoginTV,
        R.id.txtSignUpTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtForgotPwdTV -> performForgotPwdClick()
            R.id.btnLoginTV -> performLoginClick()
            R.id.txtSignUpTV -> performSignUpClick()
        }
    }


    private fun getDeviceToken() {

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                mDeviceToken = task.result!!
                Log.e(TAG, "**Push TokenGEN**$mDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $mDeviceToken")

                AppPrefrences().writeString(this, DEVICE_TOKEN,mDeviceToken)

            })

    }



    private fun performLoginClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeSignInApi()
            }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        mMap["pass"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = "2"
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }


    private fun executeSignInApi() {
        showProgressDialog(mActivity)
        mSignInViewModel?.getSignInData(mActivity, mParams())?.observe(this,
            { mSignInModel ->
                if (mSignInModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mSignInModel.message)
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mSignInModel.userDetails?.userID)
                    AppPrefrences().writeString(mActivity, EMAIL, mSignInModel.userDetails?.email)
                    AppPrefrences().writeString(mActivity, STATUS, mSignInModel.userDetails?.status)
                    val intent = Intent(mActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                    Log.e(TAG, mSignInModel.userDetails?.userID.toString())
                } else {
                    showAlertDialog(mActivity, mSignInModel.message)
                    dismissProgressDialog()
                }
            })
    }

    private fun performForgotPwdClick() {
        val i = Intent(mActivity, ForgotPwdActivity::class.java)
        startActivity(i)
    }

    private fun performSignUpClick() {
        val i = Intent(mActivity, SignUpActivity::class.java)
        startActivity(i)
        finish()
    }


    /*
  * Set up validations for Log In fields
  * */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        }
        return flag
    }
}