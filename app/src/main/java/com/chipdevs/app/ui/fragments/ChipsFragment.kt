package com.chipdevs.app.ui.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.ChatUserActivity
import com.chipdevs.app.ui.activities.ShoppingBagActivity
import com.jackandphantom.circularprogressbar.CircleProgressbar
import kotlinx.android.synthetic.main.fragment_chips.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit


class ChipsFragment : BaseFragment() {
    /*
    * Initialize...
    * */
    lateinit var mUnbinder: Unbinder
    var counter: CountDownTimer? = null
    var chipsValueTv: TextView? = null
    var shareIv: ImageView? = null
    var adsIV: ImageView? = null

    var totalChipsValueTv: TextView? = null
    var txtQuestionTV: TextView? = null
    var txtTimerTV: TextView? = null
    var txtOptionTV1: TextView? = null
    var txtOptionTV2: TextView? = null
    var txtOptionTV3: TextView? = null
    var txtOptionTV4: TextView? = null
    var btnShareTV: TextView? = null
    var timerPB: CircleProgressbar? = null

    var optionLL1: LinearLayout? = null
    var optionLL2: LinearLayout? = null
    var optionLL3: LinearLayout? = null
    var optionLL4: LinearLayout? = null

    var questionIVRL: RelativeLayout? = null
    var chipsParentLL: LinearLayout? = null
    var noQuestionDataFoundTv: TextView? = null

    var option1: CheckBox? = null
    var option2: CheckBox? = null
    var option3: CheckBox? = null
    var option4: CheckBox? = null


    var questionsListing = mutableListOf<QuestionData>()
    var answerListing = mutableListOf<AnswerOption1>()
    var answerListing2 = mutableListOf<AnswerOption2>()
    var position = 0
    var correctAnswer = ""
    var answerByUser = ""
    var timeUpStatus = ""

    var firstTime: Boolean = false
    var count = 0
    var questionTime = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_chips, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        initComponents(view)

        /**
         * set listener on checkboxes
         */

        option1?.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                option2?.isChecked = false
                option3?.isChecked = false
                option4?.isChecked = false
                btnShareTV?.text = getString(R.string.submit)
                shareIv?.visibility = View.GONE
                answerByUser = txtOptionTV1?.text.toString().trim()
            } else {
                btnShareTV?.text = getString(R.string.share)
                shareIv?.visibility = View.VISIBLE
            }
        }
        option2?.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                option1?.isChecked = false
                option3?.isChecked = false
                option4?.isChecked = false
                btnShareTV?.text = getString(R.string.submit)
                shareIv?.visibility = View.GONE
                answerByUser = txtOptionTV2?.text.toString().trim()
            } else {
                btnShareTV?.text = getString(R.string.share)
                shareIv?.visibility = View.VISIBLE
            }
        }
        option3?.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                option2?.isChecked = false
                option1?.isChecked = false
                option4?.isChecked = false
                btnShareTV?.text = getString(R.string.submit)
                shareIv?.visibility = View.GONE
                answerByUser = txtOptionTV3?.text.toString().trim()
            } else {
                btnShareTV?.text = getString(R.string.share)
                shareIv?.visibility = View.VISIBLE
            }
        }
        option4?.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                option2?.isChecked = false
                option3?.isChecked = false
                option1?.isChecked = false
                btnShareTV?.text = getString(R.string.submit)
                shareIv?.visibility = View.GONE
                answerByUser = txtOptionTV4?.text.toString().trim()
            } else {
                btnShareTV?.text = getString(R.string.share)
                shareIv?.visibility = View.VISIBLE
            }
        }
        return view
    }

    @OnClick(
        R.id.imgChatIV,
        R.id.imgBagIV,
        R.id.btnShareTV,
        R.id.option1,
        R.id.option2,
        R.id.option3,
        R.id.option4
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgChatIV -> performChatClick()
            R.id.imgBagIV -> performBagClick()
            R.id.btnShareTV -> performShareSubmitClick()
        }
    }


    private fun mSubmitAnswerRequest() {

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["quesID"] = questionsListing[0].quesID
        mMap["userID"] = getLoggedInUserID()
        mMap["answer"] = answerByUser
        mMap["timeUp"] = timeUpStatus
        Log.e(TAG, "**PARAMHome**$mMap")

        showProgressDialog(activity)
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.submitAnswerRequest(mMap).enqueue(object : Callback<SubmitAnswerModel> {
            override fun onResponse(
                call: Call<SubmitAnswerModel>,
                response: Response<SubmitAnswerModel>
            ) {
                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1") {

                    Log.e(TAG, responseModel.toString())
                    if (responseModel.message.contains("Question time up.")) {
                        executeQuestionsRequest()
                    } else {
                        showSubmitDialog(activity, response.body()?.message)
                    }

                    counter?.cancel()


                } else {
                    Log.e(TAG, responseModel.toString())
                    //  executeQuestionsRequest()
                    showToast(activity, response.body()?.message)
                }

            }

            override fun onFailure(call: Call<SubmitAnswerModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }

    private fun mShareQuestionParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["quesID"] = questionsListing[0].quesID
        mMap["userID"] = getLoggedInUserID()
        // mMap["answer"] = questionsListing[0].correctAnswer
        Log.e(TAG, "**PARAMHome**$mMap")
        return mMap
    }

    private fun performShareSubmitClick() {
        if (btnShareTV?.text == getString(R.string.share)) {
            shareToFriends(0)
        } else if (btnShareTV?.text == getString(R.string.submit)) {
            timeUpStatus = "0"
            mSubmitAnswerRequest()
        }
    }

    fun shareToFriends(position: Int) {

        showProgressDialog(activity)
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.shareQuestionsRequest(mShareQuestionParams())
            .enqueue(object : Callback<ShareQuestionModel> {
                override fun onResponse(
                    call: Call<ShareQuestionModel>,
                    response: Response<ShareQuestionModel>
                ) {
                    dismissProgressDialog()
                    if (response?.body()?.status == "1") {
                        showSubmitDialog(activity, "Share to your followers timeline.")

                    }

                }

                override fun onFailure(call: Call<ShareQuestionModel>, t: Throwable) {
                    dismissProgressDialog()
                }

            })


    }

    fun initComponents(view: View) {
        chipsValueTv = view.findViewById(R.id.chipsValueTv)
        totalChipsValueTv = view.findViewById(R.id.totalChipsValueTv)
        txtTimerTV = view.findViewById(R.id.txtTimerTV)
        txtOptionTV1 = view.findViewById(R.id.txtOptionTV1)
        txtOptionTV2 = view.findViewById(R.id.txtOptionTV2)
        txtOptionTV3 = view.findViewById(R.id.txtOptionTV3)
        txtOptionTV4 = view.findViewById(R.id.txtOptionTV4)
        btnShareTV = view.findViewById(R.id.btnShareTV)
        timerPB = view.findViewById(R.id.timerPB)
        txtQuestionTV = view.findViewById(R.id.txtQuestionTV)
        option4 = view.findViewById(R.id.option4)
        option3 = view.findViewById(R.id.option3)
        option2 = view.findViewById(R.id.option2)
        option1 = view.findViewById(R.id.option1)
        optionLL1 = view.findViewById(R.id.optionLL1)
        optionLL2 = view.findViewById(R.id.optionLL2)
        optionLL3 = view.findViewById(R.id.optionLL3)
        optionLL4 = view.findViewById(R.id.optionLL4)
        shareIv = view.findViewById(R.id.shareIV)
        questionIVRL = view.findViewById(R.id.questionIVRL)
        adsIV = view.findViewById(R.id.adsIV)
        chipsParentLL = view.findViewById(R.id.chipsParentLL)
        noQuestionDataFoundTv = view.findViewById(R.id.noQuestionDataFoundTv)


    }

    /**
     * GetAllQuestions
     */

    private fun mQuestionParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perpage"] = "10"
        Log.e(TAG, "**PARAMHome**$mMap")
        return mMap
    }

    private fun executeQuestionsRequest() {
        if (firstTime == false) {
            firstTime = true

        }
        showProgressDialog(activity)
        questionsListing.clear()


        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.getAllQuestionsRequest(mQuestionParams())
            .enqueue(object : Callback<AllQuestionsModel> {
                override fun onResponse(
                    call: Call<AllQuestionsModel>,
                    response: Response<AllQuestionsModel>
                ) {
                    clearChecks()
                    dismissProgressDialog()
                    if (response.body()?.status == "1") {
                        chipsParentLL?.visibility = View.VISIBLE
                        Log.e(TAG, response.body().toString())
                        questionsListing.addAll(response.body()!!.data)
                        chipsParentLL?.visibility = View.VISIBLE
                        noQuestionDataFoundTv?.visibility = View.GONE
                        setDataOnWidgets(response.body()!!)
                        setTimer()
                    } else {
                        chipsParentLL?.visibility = View.GONE
                        noQuestionDataFoundTv?.visibility = View.VISIBLE
                    }

                }

                override fun onFailure(call: Call<AllQuestionsModel>, t: Throwable) {
                    Log.e(TAG, t.toString())
                    dismissProgressDialog()
                }

            })

    }

    fun setDataOnWidgets(body: AllQuestionsModel) {


        totalChipsValueTv?.text = body.TotalChipsEarned
        correctAnswer = questionsListing[position].correctAnswer
        chipsValueTv?.text = questionsListing[position].chipsValue
        txtQuestionTV?.text = questionsListing[position].question
        txtTimerTV?.text = questionsListing[position].questionTime
        answerListing = questionsListing[position].answerOption1
        answerListing2 = questionsListing[position].answerOption2
        /**
         * Set options according to data from api
         */
        for (i in 0 until answerListing.size) {
            if (i == 0) {
                optionLL1?.visibility = View.VISIBLE
                txtOptionTV1?.text = answerListing[i].option
            } else {
                optionLL2?.visibility = View.VISIBLE
                txtOptionTV2?.text = answerListing[i].option
            }
        }

        for (i in 0 until answerListing2.size) {
            if (i == 0) {
                optionLL3?.visibility = View.VISIBLE
                txtOptionTV3?.text = answerListing2[i].option
            } else {
                optionLL4?.visibility = View.VISIBLE
                txtOptionTV4?.text = answerListing2[i].option
            }
        }

        if (questionsListing[position].questionImage.isNotEmpty()) {
            questionIVRL?.visibility = View.VISIBLE
            Glide.with(requireContext()).load(Uri.parse(questionsListing[position].questionImage))
                .placeholder(R.drawable.img_placeholder)
                .override(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                .error(R.drawable.img_placeholder)
                .into(questionIV)
        } else {
            questionIVRL?.visibility = View.GONE
        }


        Glide.with(requireContext()).load(Uri.parse(body.ads))
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .into(adsIV!!)


    }

    fun clearChecks() {
        option1?.isChecked = false
        option2?.isChecked = false
        option3?.isChecked = false
        option4?.isChecked = false
        optionLL1?.visibility = View.GONE
        optionLL2?.visibility = View.GONE
        optionLL3?.visibility = View.GONE
        optionLL4?.visibility = View.GONE
    }

    fun setTimer() {

        if (counter != null) {
            counter?.cancel()
            timerPB?.progress = 100F
        }

        if (questionsListing[position].questionTime.isEmpty()) {
            count = 30
            questionTime = 30
        } else {
            count = questionsListing[position].questionTime.toInt()
            questionTime = questionsListing[position].questionTime.toInt()
        }
        counter = object : CountDownTimer((questionTime * 1000).toLong(), 1000) {

            override fun onTick(millisUntilFinished: Long) {
                count -= 1
                timerPB?.maxProgress = questionTime.toFloat()
                timerPB?.progress = count.toFloat()
//show time like 00:00 format
                val text = String.format(
                    "%2d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(
                                    millisUntilFinished
                                )
                            )
                )
                txtTimerTV?.text = text
            }

            override fun onFinish() {
                // api call
                timeUpStatus = "1"
                mSubmitAnswerRequest()


            }
        }.start()
    }


    fun showSubmitDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            //Call Api Method
            executeQuestionsRequest()
        }
        alertDialog.show()
    }

    private fun performBagClick() {
        val i = Intent(activity, ShoppingBagActivity::class.java)
        startActivity(i)
    }

    private fun performChatClick() {
        val intent = Intent(activity, ChatUserActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        executeQuestionsRequest()
    }

    override fun onPause() {
        super.onPause()
        counter?.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        counter?.cancel()
        mUnbinder.unbind()
    }


}