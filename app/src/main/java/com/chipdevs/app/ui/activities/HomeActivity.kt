package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.GetProfileDetailsModel
import com.chipdevs.app.ui.fragments.ChipsFragment
import com.chipdevs.app.ui.fragments.HomeFragment
import com.chipdevs.app.ui.fragments.ProfileFragment
import com.chipdevs.app.ui.fragments.ShareFragment
import com.chipdevs.app.utils.*
import com.chipdevs.app.viewModels.GetProfileDetailsViewModel
import kotlinx.android.synthetic.main.activity_home.*
import java.util.HashMap

class HomeActivity : BaseActivity() {

    @BindView(R.id.imgProfileIV)
    lateinit var imgProfileIV: ImageView

    private var mGetProfileDetailsViewModel: GetProfileDetailsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
//        BackStackManager.fragmentPosition=""
        mGetProfileDetailsViewModel = ViewModelProviders.of(this).get(GetProfileDetailsViewModel::class.java)
        setUpFirstFragment()
    }

    private fun setUpFirstFragment() {
//        //performHomeClick()
        if(BackStackManager.fragmentPosition=="home_tag" || BackStackManager.fragmentPosition=="")
        {
            performHomeClick()
        }
        else if(BackStackManager.fragmentPosition=="chips_tag")
        {
            performChipsClick()
        }
        else if(BackStackManager.fragmentPosition=="share_tag")
        {
            performShareClick()
        }
        else if(BackStackManager.fragmentPosition=="profile_tag")
        {
            performProfileClick()
        }
    }

    @OnClick(
        R.id.actionHome,
        R.id.actionChips,
        R.id.actionShare,
        R.id.actionProfile

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.actionHome -> performHomeClick()
            R.id.actionChips -> performChipsClick()
            R.id.actionShare -> performShareClick()
            R.id.actionProfile -> performProfileClick()
        }
    }

    private fun performHomeClick() {
        BackStackManager.fragmentPosition="home_tag"
        switchFragment(HomeFragment(), HOME_TAG, false, null)
        imgHomeIV.setImageResource(R.drawable.ic_home_selected)
        imgChipsIV.setImageResource(R.drawable.ic_chips)
        imgShareIV.setImageResource(R.drawable.ic_share)

        //executeGetProfileDetailsRequest()
    }

    private fun performChipsClick() {
        BackStackManager.fragmentPosition="chips_tag"
        switchFragment(ChipsFragment(), CHIPS_TAG, false, null)
        imgHomeIV.setImageResource(R.drawable.ic_home)
        imgChipsIV.setImageResource(R.drawable.ic_chips_selected)
        imgShareIV.setImageResource(R.drawable.ic_share)

       // executeGetProfileDetailsRequest()
    }

    private fun performShareClick() {
        BackStackManager.fragmentPosition="share_tag"
        switchFragment(ShareFragment(), SHARE_TAG, false, null)
        imgHomeIV.setImageResource(R.drawable.ic_home)
        imgChipsIV.setImageResource(R.drawable.ic_chips)
        imgShareIV.setImageResource(R.drawable.ic_share_selected)

       // executeGetProfileDetailsRequest()
    }

    private fun performProfileClick() {
        BackStackManager.fragmentPosition="profile_tag"
        switchFragment(ProfileFragment(), PROFILE_TAG, false, null)
        imgHomeIV.setImageResource(R.drawable.ic_home)
        imgChipsIV.setImageResource(R.drawable.ic_chips)
        imgShareIV.setImageResource(R.drawable.ic_share)
       // executeGetProfileDetailsRequest()
    }


    // In your activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun mGetProfileParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userid"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeGetProfileDetailsRequest() {
        showProgressDialog(mActivity)
        mGetProfileDetailsViewModel?.getProfileData(mActivity, mGetProfileParams())?.observe(this,
            androidx.lifecycle.Observer<GetProfileDetailsModel?> { mGetProfileData ->
                if (mGetProfileData.status == "1") {
                    dismissProgressDialog()
                    setDataOnWidgets(mGetProfileData)

                } else {
                    showAlertDialog(mActivity, mGetProfileData.message)
                    dismissProgressDialog()
                }
            }
        )
    }
    fun setDataOnWidgets(mGetProfileModel: GetProfileDetailsModel) {
//      Set Data on Widgets
        Glide
            .with(this)
            .load(mGetProfileModel.userDetails?.image)
            .centerCrop()
            .placeholder(R.drawable.ic_profile)
            .into(imgProfileIV)
    }
    override fun onBackPressed() {
        super.onBackPressed()
        BackStackManager.fragmentPosition=""
        finishAffinity()
    }
    override fun onResume() {
        super.onResume()
        executeGetProfileDetailsRequest()
    }
}