package com.chipdevs.app.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.chipdevs.app.R
import com.chipdevs.app.adapters.HomeAdapter
import com.chipdevs.app.adapters.HomeChildAdapter
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.ChatUserActivity
import com.chipdevs.app.ui.activities.ShoppingBagActivity
import com.chipdevs.app.viewModels.GetAllProductsViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class HomeFragment : BaseFragment(), HomeChildAdapter.addToCart {
    @BindView(R.id.txtNoDataFoundTV)
    lateinit var txtNoDataFoundTV: TextView

    @BindView(R.id.txtHomeTV)
    lateinit var txtHomeTV: TextView

    @BindView(R.id.homeRV)
    lateinit var homeRV: RecyclerView

    @BindView(R.id.editSearchET)
    lateinit var editSearchET: EditText

    @BindView(R.id.searchRL)
    lateinit var searchRL: RelativeLayout

    @BindView(R.id.icSearchIV)
    lateinit var icSearchIV: ImageView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    var cartId: Int? = null

    /*
       * Initialize...
       * */
    lateinit var mUnbinder: Unbinder
    var homeAdapter: HomeAdapter? = null
    private var mGetAllProductsViewModel: GetAllProductsViewModel? = null
    lateinit var productItem: ProductItems
    var allProductsListing = mutableListOf<DataItem>()
    var mProductListing = mutableListOf<ProductItems>()
    var dataList = mutableListOf<DataItem>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        view.searchRL.visibility = View.GONE
        mUnbinder = ButterKnife.bind(this, view)
        mGetAllProductsViewModel =
            ViewModelProviders.of(this).get(GetAllProductsViewModel::class.java)
        getHomeListingData()

        editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event == null || event.action !== KeyEvent.ACTION_DOWN) {
                //do something
                getHomeListingData()
            }
            false
        })
        // getSearchData()
        return view
    }

    @OnClick(
        R.id.imgChatIV,
        R.id.imgBagIV,
        R.id.icSearchIV,
        R.id.imgCrossIV,
        R.id.icSearchIV2
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgChatIV -> performChatClick()
            R.id.imgBagIV -> performBagClick()
            R.id.icSearchIV -> performSearchClick()
            R.id.icSearchIV2 -> performSearchResultClick()
            R.id.imgCrossIV -> performCrossClick()
        }
    }

    private fun performSearchResultClick() {
        getHomeListingData()
    }

    private fun getHomeListingData() {
        if (activity?.let { isNetworkAvailable(it) }!!) {
            allProductsListing.clear()
            mProductListing.clear()
            executeHomeRequest()
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["search"] = editSearchET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeHomeRequest() {
        showProgressDialog(activity)

        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getAllProductsRequest(mParams())
            .enqueue(object : Callback<GetAllProductsModel> {
                override fun onResponse(
                    call: Call<GetAllProductsModel>,
                    response: Response<GetAllProductsModel>
                ) {
                    dismissProgressDialog()
                    val responseModel = response.body()
                    if (responseModel?.status == 1) {
                        allProductsListing.addAll(responseModel.data)
                        Log.e(TAG, ">>>ALLL LIst" + allProductsListing)
                        dataList = responseModel.data
                        for (i in 0 until dataList.size) {
                            val list = responseModel.data[i].products
                            for (j in 0 until list.size) {
                                productItem = list[j]
                                mProductListing.add(productItem)
                            }
                        }
                        setAdapter()
                        homeRV.visibility = View.VISIBLE
                        txtNoDataFoundTV.visibility = View.GONE
                    } else {
                        dismissProgressDialog()
                        homeRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        txtNoDataFoundTV.text = getString(R.string.no_data_found)
                    }

                }

                override fun onFailure(call: Call<GetAllProductsModel>, t: Throwable) {

                    dismissProgressDialog()
                }

            })


    }

    private fun setAdapter() {
        homeRV.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        homeAdapter = HomeAdapter(activity, mProductListing, this, allProductsListing)
        homeRV.adapter = homeAdapter
    }

    private fun performBagClick() {
        val i = Intent(activity, ShoppingBagActivity::class.java)
        startActivity(i)
    }

    private fun performChatClick() {
        val intent = Intent(activity, ChatUserActivity::class.java)
        startActivity(intent)
    }

    private fun performSearchClick() {
        searchRL.visibility = View.VISIBLE
        txtHomeTV.visibility = View.GONE
        icSearchIV.visibility = View.GONE
        icSearchIV2.visibility = View.VISIBLE
    }

    private fun performCrossClick() {
        if(editSearchET.text.isNullOrEmpty()){
            txtHomeTV.visibility = View.VISIBLE
            icSearchIV.visibility = View.VISIBLE
            icSearchIV2.visibility = View.GONE
            editSearchET.text.clear()
            searchRL.visibility = View.GONE
            getHomeListingData()
        }
        else if(editSearchET.text.isNotEmpty())
            editSearchET.text.clear()
    }


    override fun onBagClick(
        position: Int,
        chipPrice: String?,
        productID: String?,
        imgShoppingBagIV: ImageView,
        checkValue: Int?,
        cartID: String?
    ) {
        if (checkValue == 0)//not added YetInCart
        {

            addItemToCart(position, chipPrice, productID, imgShoppingBagIV, checkValue)

        } else {
            removeItemFromCart(checkValue, position, cartID, imgShoppingBagIV)
        }


    }

    private fun removeItemFromCart(
        checkValue: Int?,
        position: Int,
        cartID: String?,
        imgShoppingBagIV: ImageView
    ) {
        showProgressDialog(activity)
        val mMap: MutableMap<String?, String?> = HashMap()
        cartId = cartID?.toInt()
        mMap["cartID"] = cartId.toString()

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.removeItemFromCart(mMap).enqueue(object : Callback<RemoveItemFromCartModel> {
            override fun onResponse(
                call: Call<RemoveItemFromCartModel>,
                response: Response<RemoveItemFromCartModel>
            ) {

                dismissProgressDialog()
                if (response.body()?.status == 1) {
                    Log.e(TAG, response.body().toString())
                    imgShoppingBagIV.setImageResource(R.drawable.ic_bag_blue)
                    mProductListing[position].check_value = 0


                } else {
                    Log.e(TAG, response.body().toString())
                }


            }

            override fun onFailure(call: Call<RemoveItemFromCartModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }

    fun addItemToCart(
        position: Int,
        chipPrice: String?,
        productID: String?,
        imgShoppingBagIV: ImageView,
        checkValue: Int
    ) {

        showProgressDialog(activity)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["wishlistid"] = "0"
        mMap["productId"] = productID
        mMap["quantity"] = "1"
        mMap["price"] = chipPrice
        Log.e("ADDTOCARTPARAM", "**PARAMHome**$mMap")


        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.addToCartItem(mMap).enqueue(object : Callback<AddToCartModel> {
            override fun onResponse(
                call: Call<AddToCartModel>,
                response: Response<AddToCartModel>
            ) {
                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1") {

                    Log.e(TAG, responseModel.toString())
                    showAlertDialog(activity,"Item is added in the bag.")
                    imgShoppingBagIV.setImageResource(R.drawable.ic_bag)
                    cartId = responseModel.cartID

                    mProductListing[position].check_value = 1

//                    mModel?.products?.get(position)?.checkValue = 1
                    mProductListing[position].cartID = cartId.toString()


                } else {
                    Log.e(TAG, responseModel.toString())
                }


            }

            override fun onFailure(call: Call<AddToCartModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }
}



