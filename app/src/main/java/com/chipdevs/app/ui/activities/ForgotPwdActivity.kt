package com.chipdevs.app.ui.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.models.ForgotPwdModel
import com.chipdevs.app.viewModels.ForgotPwdViewModel
import java.util.*

class ForgotPwdActivity : BaseActivity() {
    /*
    * Widgets...
    * */
    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.btnSubmitTV)
    lateinit var btnSubmitTV: TextView

    @BindView(R.id.imgBackRL)
    lateinit var imgBackRL: RelativeLayout

    private var mForgotPwdViewModel: ForgotPwdViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pwd)
        ButterKnife.bind(this)
        mForgotPwdViewModel = ViewModelProviders.of(this).get(ForgotPwdViewModel::class.java)
        setEditTextFocused(mActivity, editEmailET)
    }

    @OnClick(
        R.id.btnSubmitTV,
        R.id.imgBackRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.btnSubmitTV -> performSubmitClick()
            R.id.imgBackRL -> onBackPressed()

        }
    }

    /*
   * Set up validations for Log In fields
   * */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        }
        return flag
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = editEmailET.text.toString().trim({ it <= ' ' })
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    fun performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeForgotRequest()
            else
                showToast(mActivity, getString(R.string.internal_server_error))
        }
    }

    private fun executeForgotRequest() {
        showProgressDialog(mActivity)
        mForgotPwdViewModel?.sendForgotPwdData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<ForgotPwdModel?> { mForgotPwdModel ->
                if (mForgotPwdModel.status == 1) {
                    dismissProgressDialog()
                    showFinishOpenPreviousActivityAlertDialog(mActivity, getString(R.string.your_new_pwd))
                    editEmailET.setText("")
                } else {
                    showAlertDialog(mActivity, mForgotPwdModel.message)
                    dismissProgressDialog()
                }
            }
        )
    }

}