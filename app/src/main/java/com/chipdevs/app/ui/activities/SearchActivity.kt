package com.chipdevs.app.ui.activities

import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.SearchAdapter
import com.chipdevs.app.models.SearchFiendData
import com.chipdevs.app.models.SearchFriendModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SearchActivity : BaseActivity() {
    lateinit var searchAdapter: SearchAdapter
    var counter: CountDownTimer? = null
    var searchedFriend = ""
    var isActivityPaused: Boolean? = false

    @BindView(R.id.searchFriendEt)
    lateinit var searchFriendEt: EditText

    @BindView(R.id.noUserFoundTv)
    lateinit var noUserFoundTv: TextView

    @BindView(R.id.searchRV)
    lateinit var searchRV: RecyclerView
    var searchedFriendList = mutableListOf<SearchFiendData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        ButterKnife.bind(this)


        searchFriendEt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Log.e("search text before", s.toString())


            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }

            @RequiresApi(Build.VERSION_CODES.P)
            override fun afterTextChanged(s: Editable?) {

                if (counter != null) {
                    counter?.cancel()
                }

                counter = object : CountDownTimer(1000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                    }

                    override fun onFinish() {
                        // api call
                        searchedFriendList.clear()
                        searchedFriend = s.toString()
                        searchFriendRequest()

                    }
                }.start()

            }

        })

    }

    @OnClick(
        R.id.imgBackRL,
        R.id.txtCancelTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> onBackPressed()
            R.id.txtCancelTV -> clearSearchField()
        }
    }

    fun clearSearchField()
    {
        if(searchFriendEt.text.isNullOrEmpty())
        {
          // onBackPressed()
        }
        else if(searchFriendEt.text.isNotEmpty())
        {
            searchFriendEt.text.clear()
        }


    }


    private fun initRecyclerView() {

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        searchRV.layoutManager = layoutManager
        searchAdapter = SearchAdapter(this, searchedFriendList)
        searchRV.adapter = searchAdapter
    }


    fun searchFriendRequest() {

        searchedFriendList.clear()
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["search"] = searchedFriend
        mMap["userID"] = getLoggedInUserID()

        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.getSearchFriendRequest(mMap).enqueue(object : Callback<SearchFriendModel> {
            override fun onResponse(
                call: Call<SearchFriendModel>,
                response: Response<SearchFriendModel>
            ) {
                var responseModel = response.body()
                if (responseModel?.status == "1") {
                    Log.e(TAG, responseModel.data.toString())
                    searchedFriendList.addAll(responseModel.data)
                    noUserFoundTv.visibility = View.GONE
                    searchRV.visibility = View.VISIBLE
                    initRecyclerView()
                } else {

                    noUserFoundTv.visibility = View.VISIBLE
                    searchRV.visibility = View.GONE

                }


            }

            override fun onFailure(call: Call<SearchFriendModel>, t: Throwable) {
                noUserFoundTv.visibility = View.VISIBLE
                searchRV.visibility = View.GONE

            }

        })

    }

    override fun onPause() {
        super.onPause()
        isActivityPaused=true
    }

    override fun onResume() {
        super.onResume()
        if(isActivityPaused==true)
        {
            searchFriendRequest()
        }

    }

}