package com.chipdevs.app.ui.activities

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.utils.*
import com.chipdevs.app.R.string.*

class WebviewActivity : BaseActivity() {
    @BindView(R.id.txtHeadingTV)
    lateinit var txtHeadingTV: TextView
    @BindView(R.id.mWebViewWV)
    lateinit var mWebViewWV: WebView
    var linkType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        ButterKnife.bind(this)
        getIntentData()
    }


    @OnClick(R.id.backRL)
    fun onClick(view: View) {
        when (view.id) {
            R.id.backRL -> performCrossClick()
        }
    }


    private fun getIntentData() {
        if (intent != null) {
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            if (linkType.equals(LINK_ABOUT)) {
                txtHeadingTV.text = getString(about_us)
                setUpWebView(ABOUT_WEB_LINK)
            } else if (linkType.equals(LINK_TERMS)) {
                txtHeadingTV.setText(getString(terms))
                setUpWebView(TERMS_WEB_LINK)
            } else if (linkType.equals(LINK_PP)) {
                txtHeadingTV.text = getString(privacy_policy)
                setUpWebView(PP_WEB_LINK)
            }
        }
    }

    private fun setUpWebView(mUrl: String) {
        mWebViewWV.webViewClient = WebViewClient()
        // this will load the url of the website
        mWebViewWV.loadUrl(mUrl)
        // this will enable the javascript settings
        mWebViewWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        mWebViewWV.settings.setSupportZoom(true)
    }

    private fun performCrossClick() {
        onBackPressed()
    }

}