package com.chipdevs.app.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.chipdevs.app.R
import com.chipdevs.app.adapters.ShareAdapter
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.ChatUserActivity
import com.chipdevs.app.ui.activities.ShoppingBagActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShareFragment : BaseFragment(), ShareAdapter.onSubmitQuestion {
    /*
       * Initialize...
       * */
    lateinit var mUnbinder: Unbinder
    lateinit var shareAdapter: ShareAdapter
    lateinit var shareRV: RecyclerView
    lateinit var noShaeDataFoundTv: TextView
    var adsImageString=""
    var sharedQuestionsListing = mutableListOf<QuestionData>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_share, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        shareRV = view.findViewById<RecyclerView>(R.id.shareRV)
        noShaeDataFoundTv = view.findViewById<TextView>(R.id.noShaeDataFoundTv)


        return view
    }

    @OnClick(
        R.id.imgChatIV,
        R.id.imgBagIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgChatIV -> performChatClick()
            R.id.imgBagIV -> performBagClick()
        }
    }

    private fun performBagClick() {
        val i = Intent(activity, ShoppingBagActivity::class.java)
        startActivity(i)
    }

    private fun performChatClick() {
        val intent = Intent(activity, ChatUserActivity::class.java)
        startActivity(intent)
    }

    /**
     * GetAllSharedQuestions
     */

    private fun mQuestionParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perpage"] = "1000"
        Log.e(TAG, "**PARAMHome**$mMap")
        return mMap
    }

    private fun executeSharedQuestionsRequest() {
        sharedQuestionsListing.clear()

        showProgressDialog(activity)
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.getAllSharedQuestionsRequest(mQuestionParams())
            .enqueue(object : Callback<AllSharedQuestions> {
                override fun onResponse(
                    call: Call<AllSharedQuestions>,
                    response: Response<AllSharedQuestions>
                ) {
                    dismissProgressDialog()
                    if (response.body()?.status == "1") {
                        Log.e(TAG, response.body().toString())

                        sharedQuestionsListing.addAll(response.body()!!.sharequestions)
                        adsImageString= response.body()!!.ads
                        shareRV.visibility = View.VISIBLE
                        noShaeDataFoundTv.visibility = View.GONE
                        initRecyclerView()
                    } else {
                        shareRV.visibility = View.GONE
                        noShaeDataFoundTv.visibility = View.VISIBLE
                    }

                }

                override fun onFailure(call: Call<AllSharedQuestions>, t: Throwable) {
                    Log.e(TAG, t.toString())
                    dismissProgressDialog()
                }

            })

    }

    private fun initRecyclerView() {

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        shareRV.setLayoutManager(layoutManager)
        sharedQuestionsListing.reverse()
        shareAdapter = ShareAdapter(this, sharedQuestionsListing,adsImageString)
        shareRV.setAdapter(shareAdapter)
    }

    private fun mSubmitAnswerRequest(
        position: Int,
        quesID: String,
        answerByUser: String,
        timeUpStatus: String
    ) {

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["quesID"] = quesID
        mMap["userID"] = getLoggedInUserID()
        mMap["answer"] = answerByUser
        mMap["timeUp"] = timeUpStatus
        Log.e(TAG, "**PARAMHome**$mMap")

        showProgressDialog(activity)
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.submitAnswerRequest(mMap).enqueue(object : Callback<SubmitAnswerModel> {
            override fun onResponse(
                call: Call<SubmitAnswerModel>,
                response: Response<SubmitAnswerModel>
            ) {
                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1") {
                    Log.e(TAG, responseModel.toString())
                    showAlertDialog(activity, response.body()?.message)
                    val previousPosition = position
                    sharedQuestionsListing.removeAt(position)
                    shareAdapter.notifyItemRemoved(position)
                    shareAdapter.notifyDataSetChanged()
                    shareRV.getLayoutManager()?.scrollToPosition(previousPosition)
                  //  shareRV.adapter?.notifyItemRangeChanged(position,sharedQuestionsListing.size-1)

                    if(sharedQuestionsListing.size==0)
                    {
                        shareRV.visibility = View.GONE
                        noShaeDataFoundTv.visibility = View.VISIBLE
                    }

                }

                else {
                    Log.e(TAG, responseModel.toString())
                    val previousPosition = position
                    sharedQuestionsListing.removeAt(position)
                    shareAdapter.notifyItemRemoved(position)

                    shareRV.getLayoutManager()?.scrollToPosition(previousPosition)

                    showToast(activity, response.body()?.message)
                }

            }

            override fun onFailure(call: Call<SubmitAnswerModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })

    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }

    override fun onAnswerClick(
        position: Int,
        quesID: String,
        answerByUser: String,
        timeUpStatus: String,
        correctAnswer: String,
        holder: ShareAdapter.ViewHolder
    ) {
        if (timeUpStatus == "0") {
            mSubmitAnswerRequest(position, quesID, answerByUser, timeUpStatus)
        } else {

            shareToFriends(position, quesID, correctAnswer)
        }

    }


    fun shareToFriends(position: Int, quesID: String, answerByUser: String) {

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["quesID"] = quesID
        mMap["userID"] = getLoggedInUserID()
      //  mMap["answer"] = answerByUser

        showProgressDialog(activity)
        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.shareQuestionsRequest(mMap).enqueue(object : Callback<ShareQuestionModel> {
            override fun onResponse(
                call: Call<ShareQuestionModel>,
                response: Response<ShareQuestionModel>
            ) {
                dismissProgressDialog()
                if (response?.body()?.status == "1") {
                    showAlertDialog(activity, "Share to your followers timeline.")
                    val previousPosition = position
                    sharedQuestionsListing.removeAt(position)
                    shareAdapter.notifyItemRemoved(position)
                    shareAdapter.notifyDataSetChanged()
                    shareRV.getLayoutManager()?.scrollToPosition(previousPosition)
                  //  shareRV.adapter?.notifyItemRangeChanged(position,sharedQuestionsListing.size-1)

                    if(sharedQuestionsListing.size==0)
                    {
                        shareRV.visibility = View.GONE
                        noShaeDataFoundTv.visibility = View.VISIBLE
                    }
                }
                else
                {
                    Log.e(TAG,"not working"+(response.body().toString()))
                }

            }

            override fun onFailure(call: Call<ShareQuestionModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }


    override fun onResume() {
        super.onResume()
        executeSharedQuestionsRequest()
    }
}