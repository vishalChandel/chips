package com.chipdevs.app.ui.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.chipdevs.app.R
import com.chipdevs.app.utils.*
import java.util.*
import java.util.regex.Pattern

open class BaseFragment : Fragment() {
    /*
       * Get Class Name
       * */
    var TAG = this@BaseFragment.javaClass.simpleName


    /*
    * Initialize Other Classes Objects...
    * */
    var progressDialog: Dialog? = null

    /*
    * Show Progress Dialog
    * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    open fun hideKeyboard(activity: Activity) {
        val view =
            activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


    /*
     * Validate Email Address
     * */
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }





    /*
       * Getting User ID
       * */
    fun getLoggedInUserID(): String {
        return activity?.let { AppPrefrences().readString(it, USERID, "") }!!
    }

      /*
       *
       * Get User Name
       * */
    fun getUserName(): String {
          return activity?.let { AppPrefrences().readString(it, USERNAME, "") }!!
    }

    /*
       * Getting User ID
       * */
    fun getFirstName(): String {
        return activity?.let { AppPrefrences().readString(it, FIRSTNAME, "") }!!
    }

    /*
     *
     * Get User Name
     * */
    fun getLastName(): String {
        return activity?.let { AppPrefrences().readString(it, LASTNAME, "") }!!
    }

    /*
       * Getting User ID
       * */
    fun getEmail(): String {
        return activity?.let { AppPrefrences().readString(it, EMAIL, "") }!!
    }

    /*
     *
     * Get User Name
     * */
    fun getGender(): String {
        return activity?.let { AppPrefrences().readString(it, GENDER, "") }!!
    }

    /*
       * Getting User ID
       * */
    fun getDOB(): String {
        return activity?.let { AppPrefrences().readString(it, DOB, "") }!!
    }

    /*
     *
     * Get User Name
     * */
    fun getGrade(): String {
        return activity?.let { AppPrefrences().readString(it, GRADE, "") }!!
    }

}
