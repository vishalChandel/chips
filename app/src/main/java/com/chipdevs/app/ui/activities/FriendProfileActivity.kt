package com.chipdevs.app.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide

import com.chipdevs.app.R
import com.chipdevs.app.models.*
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface

import com.chipdevs.app.ui.fragments.FollowersFragment
import com.chipdevs.app.ui.fragments.FollowingFragment
import com.chipdevs.app.viewModels.CreateRoomViewModel
import com.chips.app.models.CreateRoomModel

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class FriendProfileActivity : BaseActivity() {

    @BindView(R.id.friendsFollowerIV)
    lateinit var friendsFollowerIV: ImageView

    @BindView(R.id.friendsFollowerNameTv)
    lateinit var friendsFollowerNameTv: TextView

    @BindView(R.id.messageFollowerTv)
    lateinit var messageFollowerTv: TextView

    @BindView(R.id.followFollowerTv)
    lateinit var followFollowerTv: TextView

    @BindView(R.id.followerFollowingParentLL)
    lateinit var followerFollowingParentLL: FrameLayout

    @BindView(R.id.followingsTv)
    lateinit var followingsTv: TextView


    @BindView(R.id.followersTv)
    lateinit var followersTv: TextView

    @BindView(R.id.viewFollowing)
    lateinit var viewFollowing: View

    @BindView(R.id.viewFollower)
    lateinit var viewFollower: View

     @BindView(R.id.followerFollowingTabs)
    lateinit var followerFollowingTabs: LinearLayout

      @BindView(R.id.followerFollowingView)
    lateinit var followerFollowingView: LinearLayout

      @BindView(R.id.showPrivateAccountTV)
    lateinit var showPrivateAccountTV: TextView

    @BindView(R.id.friendProfileParentLL)
    lateinit var friendProfileParentLL: LinearLayout



    var recipentToke = ""
    var mOtherUserId = ""
    var isFromFollowRequest: Boolean? = true
    var firstTimeSeeProfile: Boolean? = false
    var fragmentStatus = "0"

    var mAccountPrivacy=""
    var fromFollowButton:Boolean?=false
    var followingListing = mutableListOf<Userfollowlist>()
    var followersListing = mutableListOf<Userfollowlist>()
    private var mCreateRoomViewModel: CreateRoomViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend_profile)
        mCreateRoomViewModel = ViewModelProviders.of(this).get(CreateRoomViewModel::class.java)
        getIntentDat()
        ButterKnife.bind(this)


    }

    @OnClick(
        R.id.imgBackRL,
        R.id.followFollowerTv,
        R.id.messageFollowerTv,
        R.id.followersTv,
        R.id.followingsTv
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> onBackPressed()
            R.id.followFollowerTv -> followUnfollowRequest()
            R.id.messageFollowerTv -> sendMessage()
            R.id.followingsTv -> getFollowingList()
            R.id.followersTv -> getFollowerList()
        }
    }

    fun getIntentDat()
    {
        if(intent!=null)
        {
            mOtherUserId = intent.getStringExtra("mOtherUserId").toString()
            mAccountPrivacy = intent.getStringExtra("mAccountPrivacy").toString()
        }

    }

    fun updateFollowersView() {

        switchFragment(FollowersFragment(mOtherUserId, followersListing))
        followerUIUpdates()
    }

    fun updateFollowingView() {


        switchFragment(FollowingFragment(mOtherUserId, followingListing))
        followingUIUpdates()

    }

    fun followingUIUpdates() {
        fragmentStatus = "1"
        followingsTv.setTextColor(ContextCompat.getColor(this, R.color.colorTextBlack))
        followersTv.setTextColor(ContextCompat.getColor(this, R.color.colorTextBlack))
        followingsTv.setTextColor(ContextCompat.getColor(this, R.color.colorBlue))
        viewFollower.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGrey))
        viewFollowing.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGrey))
        viewFollowing.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlue))
    }

    fun followerUIUpdates() {
        fragmentStatus = "2"
        followingsTv.setTextColor(ContextCompat.getColor(this, R.color.colorTextBlack))
        followersTv.setTextColor(ContextCompat.getColor(this, R.color.colorTextBlack))
        followersTv.setTextColor(ContextCompat.getColor(this, R.color.colorBlue))
        viewFollower.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGrey))
        viewFollowing.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGrey))
        viewFollower.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlue))
    }



    private fun setDataOnWidgets(userDetails: UserData) {

        if(userDetails.isfollow=="0" && userDetails.status=="0")
        {

            followersTv.text = userDetails.totalFollowers + " Followers"
            followingsTv.text = userDetails.totalFollowing + " Following"

            friendsFollowerNameTv.text = userDetails?.name
            Glide.with(this).load(userDetails?.image)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .into(friendsFollowerIV)

            if (userDetails?.changeStatus == "2") {
                followFollowerTv.text = getString(R.string.following)
            } else if (userDetails?.changeStatus == "0" || userDetails?.changeStatus == "1" || userDetails?.changeStatus == "3") {
                followFollowerTv.text = getString(R.string.follow)

            }

        }
        else if(userDetails.isfollow=="0" && userDetails.status=="1")
        {

            friendsFollowerNameTv.text = userDetails?.name
            Glide.with(this).load(userDetails?.image)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .into(friendsFollowerIV)

            showOthersProfile()
            updateToDefaultWidgets()

        }
        else if(userDetails.isfollow=="1" && userDetails.status=="0")
        {

            followersTv.text = userDetails.totalFollowers + " Followers"
            followingsTv.text = userDetails.totalFollowing + " Following"

            friendsFollowerNameTv.text = userDetails?.name
            Glide.with(this).load(userDetails?.image)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .into(friendsFollowerIV)

            if (userDetails?.changeStatus == "2") {

                friendsFollowerNameTv.text = userDetails?.name
                Glide.with(this).load(userDetails?.image)
                    .error(R.drawable.ic_profile)
                    .placeholder(R.drawable.ic_profile)
                    .into(friendsFollowerIV)

                followFollowerTv.text = getString(R.string.following)
            } else if (userDetails?.changeStatus == "0" || userDetails?.changeStatus == "1" || userDetails?.changeStatus == "3") {

                friendsFollowerNameTv.text = userDetails?.name
                Glide.with(this).load(userDetails?.image)
                    .error(R.drawable.ic_profile)
                    .placeholder(R.drawable.ic_profile)
                    .into(friendsFollowerIV)

                followFollowerTv.text = getString(R.string.follow)

            }
        }
        else if(userDetails.isfollow=="1"  && userDetails.status=="1")//user following the searched user but account is private
        {

            friendsFollowerNameTv.text = userDetails?.name
            Glide.with(this).load(userDetails?.image)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .into(friendsFollowerIV)

            if(userDetails.changeStatus=="0" || userDetails.changeStatus=="3" )//rejected and not followed user
            {
                showOthersProfile()
                updateToDefaultWidgets()
            }
            else if(userDetails.changeStatus=="2")
            {


                followersTv.text = userDetails.totalFollowers + " Followers"
                followingsTv.text = userDetails.totalFollowing + " Following"

                friendsFollowerNameTv.text = userDetails?.name
                Glide.with(this).load(userDetails?.image)
                    .error(R.drawable.ic_profile)
                    .placeholder(R.drawable.ic_profile)
                    .into(friendsFollowerIV)

                if (userDetails?.changeStatus == "2") {

                    friendsFollowerNameTv.text = userDetails?.name
                    Glide.with(this).load(userDetails?.image)
                        .error(R.drawable.ic_profile)
                        .placeholder(R.drawable.ic_profile)
                        .into(friendsFollowerIV)

                    followFollowerTv.text = getString(R.string.following)
                } else if (userDetails?.changeStatus == "0" || userDetails?.changeStatus == "1" || userDetails?.changeStatus == "3") {

                    friendsFollowerNameTv.text = userDetails?.name
                    Glide.with(this).load(userDetails?.image)
                        .error(R.drawable.ic_profile)
                        .placeholder(R.drawable.ic_profile)
                        .into(friendsFollowerIV)

                    followFollowerTv.text = getString(R.string.follow)

                }
            }
            else if(userDetails.changeStatus=="1")//request sent
            {

                friendsFollowerNameTv.text = userDetails?.name
                Glide.with(this).load(userDetails?.image)
                    .error(R.drawable.ic_profile)
                    .placeholder(R.drawable.ic_profile)
                    .into(friendsFollowerIV)

                updateToRequestSentTV()
                showOthersProfile()

            }
        }


    }


    fun getFollowingList() {
        followingListing.clear()
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followType"] = "2"//following
        mMap["profileUserId"] = mOtherUserId
        Log.e(">>>FollowParam", mMap.toString())
        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.getFollowersFollowingRequests(mMap)
            .enqueue(object : Callback<AllPeoplesModel> {
                override fun onResponse(
                    call: Call<AllPeoplesModel>,
                    response: Response<AllPeoplesModel>
                ) {
                    friendProfileParentLL.visibility=View.VISIBLE
                    dismissProgressDialog()
                    val responseModel = response.body()
                    Log.e(TAG, responseModel!!.toString())

                    if (responseModel.status == 1) {
                        followingListing.addAll(responseModel.userfollowlist)
                        setDataOnWidgets(responseModel.user_data)
                        updateFollowingView()
                    }
                }

                override fun onFailure(call: Call<AllPeoplesModel>, t: Throwable) {
                    dismissProgressDialog()

                }

            })


    }

    fun followUnfollowRequest() {
        if (followFollowerTv.text == getString(R.string.follow)) {
            fromFollowButton=true
            followUserRequest()
        } else if (followFollowerTv.text == getString(R.string.following)) {
            fromFollowButton=true
            followUserRequest()
        }
        else if (followFollowerTv.text == getString(R.string.follow_request_sent)) {
            fromFollowButton=true
            followUserRequest()
        }
    }


    fun getFollowerList() {
        followersListing.clear()
        showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followType"] = "1"//followers lists
        mMap["profileUserId"] = mOtherUserId
        Log.e(">>>FollowParam", mMap.toString())
        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.getFollowersFollowingRequests(mMap)
            .enqueue(object : Callback<AllPeoplesModel> {
                override fun onResponse(
                    call: Call<AllPeoplesModel>,
                    response: Response<AllPeoplesModel>
                ) {
                    friendProfileParentLL.visibility=View.VISIBLE
                    dismissProgressDialog()
                    val responseModel = response.body()
                    if (responseModel?.status == 1) {
                        followersListing.addAll(responseModel!!.userfollowlist)
                        setDataOnWidgets(responseModel?.user_data)
                        updateFollowersView()
                        // getFollowingList()

                    } else {
                        showToast(this@FriendProfileActivity, responseModel?.message)
                    }

                }

                override fun onFailure(call: Call<AllPeoplesModel>, t: Throwable) {
                    dismissProgressDialog()

                }

            })


    }

    fun followUserRequest() {
        isFromFollowRequest = false
        fromFollowButton=true
        showProgressDialog(this)

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["followUserID"] = mOtherUserId
        val searchFriendCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        searchFriendCall.followRequest(mMap).enqueue(object : Callback<AddFollowUnfollowModel> {
            override fun onResponse(
                call: Call<AddFollowUnfollowModel>,
                response: Response<AddFollowUnfollowModel>
            ) {

                dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == "1" && responseModel?.notificationDetails != null) {
                    Log.e(TAG, responseModel.notificationDetails.toString())

                    if(mAccountPrivacy=="1")
                    {
                        followFollowerTv.text = getString(R.string.follow_request_sent)
                        followFollowerTv.setBackground(ContextCompat.getDrawable(applicationContext, R.drawable.bg_description))
                        followFollowerTv.setTextColor(resources.getColor(R.color.colorBlack))
                    }
                    else
                    {
                        updateToFollowingTV()
                        if (fragmentStatus == "1") {
                            getFollowingList()
                            firstTimeSeeProfile = false
                        } else {
                            getFollowerList()
                            firstTimeSeeProfile = false
                        }
                    }



                } else if (responseModel?.status == "1" && responseModel?.notificationDetails == null) {

                    if(mAccountPrivacy=="1")
                    {
                        showOthersProfile()
                        updateToDefaultWidgets()

                    }
                    else
                    {
                        updateToDefaultWidgets()
                        if (fragmentStatus == "1") {
                            getFollowingList()
                            firstTimeSeeProfile = false
                        } else {

                            getFollowerList()
                            firstTimeSeeProfile = false
                        }

                        Log.e(TAG, responseModel.toString())
                    }

                }
                else if(responseModel?.status=="0")
                {
                    updateToDefaultWidgets()
                }
            }

            override fun onFailure(call: Call<AddFollowUnfollowModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }


    private fun updateToDefaultWidgets() {
        followFollowerTv.text = getString(R.string.follow)
        followFollowerTv.setTextColor(resources.getColor(R.color.colorWhite))
        followFollowerTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_follow))

    }

    private fun updateToFollowingTV() {
        followFollowerTv.text = getString(R.string.following)
        followFollowerTv.setTextColor(resources.getColor(R.color.colorWhite))
        followFollowerTv.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_follow))
    }
    private fun updateToRequestSentTV() {
        followFollowerTv.text = getString(R.string.follow_request_sent)
        followFollowerTv.setBackground(ContextCompat.getDrawable(applicationContext, R.drawable.bg_description))
        followFollowerTv.setTextColor(resources.getColor(R.color.colorBlack))
    }

    private fun sendMessage() {
        getCreateRoomData()
    }


    fun switchFragment(
        fragment: Fragment?
    ) {
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.followerFollowingParentLL, fragment).commit()

        }
    }

    private fun getCreateRoomData() {
        if (mActivity?.let { isNetworkAvailable(it) }!!) {
            executeCreateRoomRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun mCreateRoomParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["owner_id"] = mOtherUserId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCreateRoomRequest() {
        showProgressDialog(mActivity)
        mCreateRoomViewModel?.sendCreateRoomData(mActivity, mCreateRoomParams())?.observe(
            this,
            androidx.lifecycle.Observer<CreateRoomModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    val intent = Intent(mActivity, ChatActivity::class.java)
                    intent.putExtra("mUsername", mModel.userDetail?.firstName + " " + mModel.userDetail?.lastName)
                    intent.putExtra("mRoomId", mModel.roomId)
                    intent.putExtra("otherUserID", mModel.ownerId)
                    startActivity(intent)
                } else {
                    dismissProgressDialog()
                }
            })

    }

fun showOthersProfile()
{
    followerFollowingView.visibility=View.GONE
    messageFollowerTv.visibility=View.GONE
    followerFollowingTabs.visibility=View.GONE
    followerFollowingParentLL.visibility=View.GONE
    showPrivateAccountTV.visibility=View.VISIBLE
   // updateToDefaultWidgets()
}
    override fun onResume() {
        super.onResume()
      //  getFollowingList()

        getFollowerList()

    }


    override fun onPause() {
        super.onPause()

       // followerUIUpdates()
    }
}