package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.WishListAdapter
import com.chipdevs.app.interfaces.MoveToBag
import com.chipdevs.app.interfaces.RemoveFromWishList
import com.chipdevs.app.models.DataItem1
import com.chipdevs.app.models.MoveToBagModel
import com.chipdevs.app.models.RemoveFromWishListModel
import com.chipdevs.app.models.WishListModel
import com.chipdevs.app.viewModels.GetWishListViewModel
import com.chipdevs.app.viewModels.MoveToBagViewModel
import com.chipdevs.app.viewModels.RemoveFromWishListViewModel
import java.util.HashMap

class WishListActivity : BaseActivity() {
    /*
      * Initialize...
      * */
    @BindView(R.id.wishListRV)
    lateinit var wishListRV: RecyclerView

    @BindView(R.id.txtNoDataFoundTV)
    lateinit var txtNoDataFoundTV: TextView

    private var mGetWishListViewModel: GetWishListViewModel? = null
    private var mRemoveFromWishListViewModel: RemoveFromWishListViewModel? = null
    private var mMoveToBagViewModel: MoveToBagViewModel? = null

    var mPosition:Int=0
    var mProductID:String? = null
    var mID:String? = null
    var mPrice:String? = null
    var mQuantity:String? = "1"

    var mArrayList: ArrayList<DataItem1?>? = ArrayList<DataItem1?>()
    var wishListAdapter: WishListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_list)
        ButterKnife.bind(this)
        mGetWishListViewModel = ViewModelProviders.of(this).get(GetWishListViewModel::class.java)
        mRemoveFromWishListViewModel = ViewModelProviders.of(this).get(RemoveFromWishListViewModel::class.java)
        mMoveToBagViewModel = ViewModelProviders.of(this).get(MoveToBagViewModel::class.java)
        getWishListData()
    }

    @OnClick(
        R.id.imgBackRL,
        R.id.imgBagIV,
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> performBackClick()
            R.id.imgBagIV -> performBagClick()
        }
    }

    private fun performBackClick() {
        val i = Intent(mActivity, ShoppingBagActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(i)
        finish()

    }

    fun performCrossClick() {
        if (isNetworkAvailable(mActivity))
            executeRemoveFromWishListRequest()
        else
            showToast(mActivity, getString(R.string.internal_server_error))
    }

    private fun mParams2(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["productID"] = mProductID
        mMap["id"] = mID
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeRemoveFromWishListRequest() {
        showProgressDialog(mActivity)
        mRemoveFromWishListViewModel?.removeFromWishListData(mActivity, mParams2())?.observe(this,
            androidx.lifecycle.Observer<RemoveFromWishListModel?> { mRemoveFromWishListModel ->
                if (mRemoveFromWishListModel.status == "1") {
                    dismissProgressDialog()
                    mArrayList?.removeAt(mPosition)
                    wishListAdapter?.notifyDataSetChanged()

                    if(mArrayList!!.isEmpty()){
                        wishListRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }else{
                        wishListRV.visibility = View.VISIBLE
                        txtNoDataFoundTV.visibility=View.GONE
                    }
                } else {
                    dismissProgressDialog()
                }
            }
        )
    }


    fun performMoveToBagClick() {
        if (isNetworkAvailable(mActivity))
            executeMoveToBagRequest()
        else
            showToast(mActivity, getString(R.string.internal_server_error))
    }

    private fun mParams3(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        mMap["productId"] = mProductID
        mMap["wishlistid"] = mID
        mMap["quantity"] = mQuantity
        mMap["price"] = mPrice
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoveToBagRequest() {
        showProgressDialog(mActivity)
        mMoveToBagViewModel?.moveToBagData(mActivity, mParams3())?.observe(this,
            androidx.lifecycle.Observer<MoveToBagModel?> { mMoveToBagModel ->
                if (mMoveToBagModel.status == "1") {
                    dismissProgressDialog()
                    mArrayList?.removeAt(mPosition)
                    setAdapter()

                    if(mArrayList!!.isEmpty()){
                        wishListRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }else{
                        wishListRV.visibility = View.VISIBLE
                        txtNoDataFoundTV.visibility=View.GONE
                    }
                } else {

                    dismissProgressDialog()
                    mArrayList?.removeAt(mPosition)
                    setAdapter()
                    if(mArrayList!!.isEmpty()){
                        wishListRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                }
            }
        )
    }


    private fun getWishListData() {
        if (mActivity?.let { isNetworkAvailable(it) }!!) {
            if (mArrayList != null) {
                mArrayList!!.clear()
            }
            executeWishListRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeWishListRequest() {
        showProgressDialog(mActivity)
        mGetWishListViewModel?.getWishListData(mActivity, mParams())?.observe(
            this,
            androidx.lifecycle.Observer<WishListModel?> { mWishListModel ->
                if (mWishListModel.status == "1") {
                    dismissProgressDialog()
                    mArrayList = mWishListModel.data as ArrayList<DataItem1?>?
                    if(mArrayList==null){
                        wishListRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                    else
                        setAdapter()
                } else {
                    dismissProgressDialog()
                    wishListRV.visibility = View.GONE
                    txtNoDataFoundTV.visibility=View.VISIBLE

                }
            })

    }

    private fun setAdapter() {
        var layoutManager = wishListRV.setLayoutManager(GridLayoutManager(mActivity, 2))
        wishListAdapter = WishListAdapter(mActivity, mArrayList,mRemoveFromWishList,mMoveToBag)
        wishListRV.setAdapter(wishListAdapter)
    }

    private fun performBagClick() {

        val intent = Intent(mActivity, ShoppingBagActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(intent)

    }

    var mRemoveFromWishList: RemoveFromWishList = object : RemoveFromWishList {
        override fun viewClick(position:Int,productId: String, id: String) {
            mPosition=position
            mProductID=productId
            mID=id
            performCrossClick()
        }


    }
    var mMoveToBag: MoveToBag = object : MoveToBag {
        override fun viewClick(position:Int,productId: String, wishlistid: String,price:String) {
            mPosition=position
            mProductID=productId
            mID=wishlistid
            mPrice=price
            performMoveToBagClick()
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        performBackClick()
    }
}