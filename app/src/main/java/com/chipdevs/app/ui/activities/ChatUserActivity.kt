package com.chipdevs.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chipdevs.app.R
import com.chipdevs.app.adapters.ChatUserAdapter
import com.chipdevs.app.models.AllUsersItem
import com.chipdevs.app.models.GetChatUsersModel
import com.chipdevs.app.models.NotificationData
import com.chipdevs.app.models.NotificationModel
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.viewModels.GetChatUsersViewModel
import com.chipdevs.app.viewModels.GetWishListViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable
import java.util.HashMap

class ChatUserActivity : BaseActivity() {
    /*
         * Initialize...
         * */
    @BindView(R.id.chatUserRV)
    lateinit var chatUserRV: RecyclerView

    @BindView(R.id.txtNoDataFoundTV)
    lateinit var txtNoDataFoundTV: TextView

    @BindView(R.id.imgNotificationIV)
    lateinit var imgNotificationIV: ImageView

    private var mGetChatUsersViewModel: GetChatUsersViewModel? = null
    lateinit var chatUserAdapter: ChatUserAdapter
    var mArrayList: ArrayList<AllUsersItem?>? = ArrayList<AllUsersItem?>()
    var notificationListing= mutableListOf<NotificationData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_user)
        ButterKnife.bind(this)
        mGetChatUsersViewModel = ViewModelProviders.of(this).get(GetChatUsersViewModel::class.java)

    }

    @OnClick(
        R.id.imgBackRL,
        R.id.imgNotificationIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> performBackClick()
            R.id.imgNotificationIV -> performNotificationClick()
        }
    }

    private fun performBackClick() {
        val i = Intent(mActivity, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)

    }

    private fun getChatUsersData() {
        if (mActivity?.let { isNetworkAvailable(it) }!!) {
            if (mArrayList != null) {
                mArrayList!!.clear()
            }
            executeChatUsersRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeChatUsersRequest() {
        showProgressDialog(mActivity)
        mGetChatUsersViewModel?.getGetChatUsersData(mActivity, mParams())?.observe(
            this,
            androidx.lifecycle.Observer<GetChatUsersModel?> { mGetChatUsersModel ->
                if (mGetChatUsersModel.status == 1) {
                    dismissProgressDialog()

                    mArrayList = mGetChatUsersModel.allUsers as ArrayList<AllUsersItem?>?

                    if(mArrayList==null){
                        chatUserRV.visibility = View.GONE
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                    else
                        setAdapter()

                } else {
                    dismissProgressDialog()
                    chatUserRV.visibility = View.GONE
                    txtNoDataFoundTV.visibility=View.VISIBLE
                }
            })

    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        chatUserRV.layoutManager = layoutManager
        chatUserAdapter = ChatUserAdapter(mActivity, mArrayList)
        chatUserRV.setAdapter(chatUserAdapter)
    }

    private fun performNotificationClick() {
        val i = Intent(mActivity, NotificationActivity::class.java)
        val notificationArrayList = arrayListOf<MutableList<NotificationData>>()
        notificationArrayList.add(notificationListing)
        i.putExtra("notificationListing",notificationArrayList)
        startActivity(i)
    }




    fun getAllNotificationData() {
       // showProgressDialog(this)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()

        val notificationCall: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        notificationCall.getNotificationsListRequest(mMap).enqueue(object :
            Callback<NotificationModel>
        {
            override fun onResponse(
                call: Call<NotificationModel>,
                response: Response<NotificationModel>
            ) {
               // dismissProgressDialog()
                val responseModel = response.body()
                if (responseModel?.status == 1) {

                    Log.e(TAG, responseModel.data.toString())
                    if(responseModel?.count>0)
                    {
                        imgNotificationIV.setImageResource(R.drawable.ic_new_notification)
                    }
                    else
                    {
                        imgNotificationIV.setImageResource(R.drawable.ic_notification)
                    }

                }
                else
                {
                    Log.e(TAG, responseModel?.message.toString())
                }
            }

            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })


    }



    override fun onResume() {
        super.onResume()
        getAllNotificationData()
        getChatUsersData()

    }
}