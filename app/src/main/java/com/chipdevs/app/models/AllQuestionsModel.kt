package com.chipdevs.app.models

data class AllQuestionsModel(
    val ChipsValue: String,
    val TotalChipsEarned: String,
    val ads: String,
    val `data`: MutableList<QuestionData>,
    val message: String,
    val questionImage: String,
    val status: String
)

data class QuestionData(
    val algo_category_id: String,
    val algo_sub_category_id: String,
    val answerOption1: MutableList<AnswerOption1>,
    val answerOption2: MutableList<AnswerOption2>,
    val catID: String,
    val chipsValue: String,
    val correctAnswer: String,
    val createTime: String,
    val disable: String,
    val quesID: String,
    val question: String,
    val questionImage: String,
    val questionTime: String,
    val updateTime: String,
    val shared_name: String,
    val shared_image: String,

)

data class AnswerOption1(
    val id: String,
    val option: String,
    val quesID: String,
    val type: String
)

data class AnswerOption2(
    val id: String,
    val option: String,
    val quesID: String,
    val type: String
)