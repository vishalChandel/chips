package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetChatUsersModel(

	@field:SerializedName("last_page")
	val lastPage: String? = null,

	@field:SerializedName("all_users")
	val allUsers: List<AllUsersItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("unread_message_count")
	val unreadMessageCount: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AllUsersItem(

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("unread_count")
	val unreadCount: String? = null,

	@field:SerializedName("message_time")
	val messageTime: String? = null,

	@field:SerializedName("receiver_id")
	val receiverId: String? = null,

	@field:SerializedName("profile_pic")
	val profilePic: String? = null,

	@field:SerializedName("room_no")
	val roomNo: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("sender_id")
	val senderId: String? = null,

	@field:SerializedName("username")
	val username: String? = null
) : Parcelable
