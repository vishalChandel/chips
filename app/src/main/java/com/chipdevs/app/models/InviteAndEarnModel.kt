package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class InviteAndEarnModel(

	@field:SerializedName("shareMessage")
	val shareMessage: String? = null,

	@field:SerializedName("referCode")
 val referCode: @RawValue Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
