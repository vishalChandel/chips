package com.chipdevs.app.models

data class RemoveItemFromCartModel(
    val message: String,
    val status: Int
)