package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetChatListingModel(

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("last_page")
	val lastPage: String? = null,

	@field:SerializedName("all_messages")
	val allMessages: List<AllMessagesItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("receiver_detail")
	val receiverDetail: ReceiverDetail? = null
) : Parcelable

@Parcelize
data class AllMessagesItem(

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("receiver_id")
	val receiverId: String? = null,

	@field:SerializedName("profile_pic")
	var profilePic: String? = null,

	@field:SerializedName("read_status")
	val readStatus: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("sender_id")
	val senderId: String? = null,

	@field:SerializedName("username")
    var username: String? = null,
	var viewType : Int = -1
) : Parcelable {
	override fun toString(): String {
		return "ChatMessageData(readStatus=$readStatus, creationDate=$creationDate, username=$username, id=$id, profilePic=$profilePic, message=$message, receiverId=$receiverId, roomId=$roomId, viewType=$viewType)"
	}
}

@Parcelize
data class ReceiverDetail(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("shoeSize")
	val shoeSize: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("admin")
	val admin: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("deviceType")
	val deviceType: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("pass")
	val pass: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("verified")
	val verified: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("deviceToken")
	val deviceToken: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("grade")
	val grade: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("changeStatus")
	val changeStatus: String? = null,

	@field:SerializedName("age")
	val age: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
