package com.chipdevs.app.models

data class SubmitAnswerModel(
    val message: String,
    val status: String
)