package com.chipdevs.app.models

data class DeleteNotifModel(
    val message: String,
    val status: String
)