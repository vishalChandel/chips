package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddMessageModel(

	@field:SerializedName("data")
	val data: AllMessagesItem? = null,

	@field:SerializedName("notificationDetails")
	val notificationDetails: NotificationDetails1? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class NotificationDetails1(

	@field:SerializedName("notification_type")
	val notificationType: String? = null,

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("followID")
	val followID: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("notification_read_status")
	val notificationReadStatus: String? = null,

	@field:SerializedName("otherID")
	val otherID: String? = null,

	@field:SerializedName("notification_id")
	val notificationId: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("viewed_status")
	val viewedStatus: String? = null,

	@field:SerializedName("username")
	val username: String? = null
) : Parcelable
