package com.chipdevs.app.models

data class IncrementDecrementModel(
    val cart_details: ItemIncrementData,
    val message: String,
    val status: String
)

data class ItemIncrementData(
    val cartID: String,
    val cartTime: String,
    val price: String,
    val productId: String,
    val quantity: String,
    val userId: String
)