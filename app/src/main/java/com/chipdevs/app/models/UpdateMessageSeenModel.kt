package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UpdateMessageSeenModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	var status: Int? = null
) : Parcelable
