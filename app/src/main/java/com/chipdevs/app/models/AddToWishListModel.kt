package com.chipdevs.app.models

data class AddToWishListModel(
    val message: String,
    val status: String,
    val wishlistid: Int
)