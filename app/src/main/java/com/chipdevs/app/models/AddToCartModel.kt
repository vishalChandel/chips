package com.chipdevs.app.models

data class AddToCartModel(
    val cartID: Int,
    val check_value: Int,
    val message: String,
    val status: String
)