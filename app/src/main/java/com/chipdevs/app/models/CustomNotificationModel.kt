package com.chipdevs.app.models

import java.io.Serializable

class CustomNotificationModel(
    val creation_date: String,
    val followID: String,
    val image: String,
    val message: String,
    val username: String,
    val notification_id: String,
    val notification_read_status: String,
    val notification_type: String,
    val otherID: String,
    val room_id: String,
    val userID: String,
    val viewed_status: String
):Serializable

