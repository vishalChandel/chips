package com.chipdevs.app.models

data class AllSharedQuestions(
    val ads: String,
    val message: String,
    val sharequestions: MutableList<QuestionData>,
    val status: String,
    val userID: String
)
