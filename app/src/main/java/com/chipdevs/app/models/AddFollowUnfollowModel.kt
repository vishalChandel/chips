package com.chipdevs.app.models

data class AddFollowUnfollowModel(
    val message: String,
    val notificationDetails: NotificationDetails,
    val status: String
)

data class NotificationDetails(
    val creation_date: String,
    val followID: String,
    val image: String,
    val message: String,
    val notification_id: String,
    val notification_read_status: String,
    val notification_type: String,
    val otherID: String,
    val room_id: String,
    val userID: String,
    val username: String,
    val viewed_status: String
)