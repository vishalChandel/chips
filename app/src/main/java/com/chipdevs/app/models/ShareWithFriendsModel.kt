package com.chipdevs.app.models

data class ShareWithFriendsModel(
    val message: String,
    val status: String
)