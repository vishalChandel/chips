package com.chipdevs.app.models

data class CartDetailsModel(
    val `data`: MutableList<CartDetails>,
    val message: String,
    val status: String,
    val totalprice: String
)

data class CartDetails(
    val cartID: String,
    val chip_price: String,
    val created_at: String,
    val description: String,
    val genreID: String,
    val image: String,
    val maxQuantity: String,
    val productID: String,
    var quantity: String,
    val return_days: String,
    val status: String,
    val title: String,
    val updated_at: String
)