package com.chipdevs.app.models

import java.io.Serializable

data class NotificationModel(
    val count: Int,
    val `data`: MutableList<NotificationData>,
    val message: String,
    val status: Int
):Serializable

data class NotificationData(
    val creation_date: String,
    val creation_date_timestamp: String,
    val followID: String,
    val image: String,
    val message: String,
    val name: String,
    val notification_id: String,
    val notification_read_status: String,
    val notification_type: String,
    val otherID: String,
    val room_id: String,
    val userID: String,
    val viewed_status: String ,
    val description: String
):Serializable