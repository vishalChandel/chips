package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetProfileDetailsModel(

	@field:SerializedName("PlayerRanking")
	val playerRanking: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("userDetails")
	val userDetails: UserDetails? = null,

	@field:SerializedName("chipsAvailable")
	val chipsAvailable: String? = null,

	@field:SerializedName("friends")
	val friends: List<FriendsItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class FriendsItem(

	@field:SerializedName("deviceType")
	val deviceType: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("shoeSize")
	val shoeSize: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("pass")
	val pass: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("verified")
	val verified: String? = null,

	@field:SerializedName("admin")
	val admin: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("deviceToken")
	val deviceToken: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("grade")
	val grade: String? = null,

	@field:SerializedName("changeStatus")
	val changeStatus: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("age")
	val age: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class UserDetails(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("shoeSize")
	val shoeSize: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("admin")
	val admin: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("deviceType")
	val deviceType: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("pass")
	val pass: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("verified")
	val verified: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,
@field:SerializedName("cashTag")
	val cashTag: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("deviceToken")
	val deviceToken: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("grade")
	val grade: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("changeStatus")
	val changeStatus: String? = null,

	@field:SerializedName("age")
	val age: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
