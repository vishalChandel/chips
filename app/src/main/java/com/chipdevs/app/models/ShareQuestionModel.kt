package com.chipdevs.app.models

data class ShareQuestionModel(
    val message: String,
    val status: String
)