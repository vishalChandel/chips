package com.chipdevs.app.models

data class SearchFriendModel(
    val `data`: MutableList<SearchFiendData>,
    val message: String,
    val status: String
)

data class SearchFiendData(
    val address: String,
    val admin: String,
    val age: String,
    var changeStatus: String,
    val city: String,
    val created: String,
    val deviceToken: String,
    val deviceType: String,
    val dob: String,
    val email: String,
    val firstName: String,
    val follow: String,
    val gender: String,
    val grade: String,
    val image: String,
    val lastName: String,
    val name: String,
    val pass: String,
    val shoeSize: String,
    val state: String,
    val status: String,
    val userID: String,
    val userName: String,
    val verificationCode: String,
    val verified: String,
    val zipcode: String
)