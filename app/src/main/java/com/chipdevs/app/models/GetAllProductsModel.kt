package com.chipdevs.app.models


data class GetAllProductsModel(
    val `data`: MutableList<DataItem>,
    val message: String,
    val status: Int
)

data class DataItem(
    val created: String,
    val description: String,
    val genreID: String,
    val products: MutableList<ProductItems>,
    val title: String
)

data class ProductItems(
    var cartID: String,
    var check_value: Int,
    val chip_price: String,
    val created_at: String,
    val description: String,
    val genreID: String,
    val image: String,
    val maxQuantity: String,
    val productID: String,
    val return_days: String,
    val status: String,
    val title: String,
    val updated_at: String
)