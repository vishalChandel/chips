package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SignInModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("userDetails")
	val userDetails: UserDetails? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

