package com.chipdevs.app.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WishListModel(

	@field:SerializedName("data")
	val data: List<DataItem1?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class DataItem1(

	@field:SerializedName("genreID")
	val genreID: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("chip_price")
	val chipPrice: String? = null,

	@field:SerializedName("productID")
	val productID: String? = null,

	@field:SerializedName("return_days")
	val returnDays: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("maxQuantity")
	val maxQuantity: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
