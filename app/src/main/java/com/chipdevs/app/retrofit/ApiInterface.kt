package com.chipdevs.app.retrofit

import com.chipdevs.app.models.*
import com.chips.app.models.CreateRoomModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {

    @POST("webServices/login.php")
    fun signInRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SignInModel>

    @POST("webServices/signupv2.php")
    fun signUpRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SignUpModel>

    @POST("webServices/forgetPassword.php")
    fun forgotPwdRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ForgotPwdModel>

    @POST("webServices/GetAllProducts.php")
    fun getAllProductsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllProductsModel>


    @POST("webServices/GetAllQuestion.php")
    fun getAllQuestionsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AllQuestionsModel>

    @POST("webServices/GetAllSharedQuestions.php")
    fun getAllSharedQuestionsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AllSharedQuestions>

    @POST("webServices/shareQuestions.php")
    fun shareQuestionsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ShareQuestionModel>

    @POST("webServices/ChangePassword.php")
    fun changePwdRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ChangePwdModel>

    @POST("webServices/getProfileDetails.php")
    fun getProfileDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetProfileDetailsModel>

    @POST("webServices/ContactUs.php")
    fun sendContactUsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ContactUsModel>

    @POST("webServices/UpadtePublicAndPrivate.php")
    fun accountStatusRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AccountStatusModel>


    @POST("webServices/NotificationActivity.php")
    fun getNotificationsListRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<NotificationModel>


    @POST("webServices/appoveRejectFollowRequest.php")
    fun acceptRejectFollowRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AddFollowUnfollowModel>


    @POST("webServices/deleteNotification.php")
    fun deleteFollowNotification(
        @Body mParams: Map<String?, String?>?
    ): Call<DeleteNotifModel>


 @POST("webServices/GetSearchFriends.php")
    fun getSearchFriendRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SearchFriendModel>

 @POST("webServices/addFollowAndUnfollow.php")
    fun followRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AddFollowUnfollowModel>


 @POST("webServices/GetFollowUsers.php")
    fun getFollowersFollowingRequests(
        @Body mParams: Map<String?, String?>?
    ): Call<AllPeoplesModel>

    @POST("webServices/editProfilev2.php")
    fun editProfileRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<EditProfileModel>

   @POST("webServices/addCart.php")
    fun addToCartItem(
        @Body mParams: Map<String?, String?>?
    ): Call<AddToCartModel>



   @POST("webServices/removeCart.php")
    fun removeItemFromCart(
        @Body mParams: Map<String?, String?>?
    ): Call<RemoveItemFromCartModel>



   @POST("webServices/cartDetails.php")
    fun cartItemDetails(
        @Body mParams: Map<String?, String?>?
    ): Call<CartDetailsModel>

@POST("webServices/IncrementAndDecrement.php")
    fun incrementDecrementItem(
        @Body mParams: Map<String?, String?>?
    ): Call<IncrementDecrementModel>



@POST("webServices/submitAnswer.php")
    fun submitAnswerRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SubmitAnswerModel>


@POST("webServices/addToWishlist.php")
    fun addToWishList(
        @Body mParams: Map<String?, String?>?
    ): Call<AddToWishListModel>

    @POST("webServices/getWistlist.php")
    fun getWishListRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<WishListModel>


   @POST("webServices/placeOrder.php")
    fun placeOrderRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SubmitAnswerModel>
    @POST("webServices/addCart.php")
    fun moveToBagRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<MoveToBagModel>

    @POST("webServices/removeforWishlist.php")
    fun removeFromWishListRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<RemoveFromWishListModel>

    @POST("webServices/GetChatUsers.php")
    fun getChatUsersRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetChatUsersModel>


    @POST("webServices/submitShareAnswer.php")
    fun shareWithFriends(
        @Body mParams: Map<String?, String?>?
    ): Call<ShareWithFriendsModel>

    @POST("webServices/GetChatListing.php")
    fun getChatListingRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetChatListingModel>

    @POST("webServices/AddMessage.php")
    fun addMsgRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AddMessageModel>

    @POST("webServices/UpdateMessageSeen.php")
    fun updateMessageSeenRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<UpdateMessageSeenModel>


    @POST("webServices/RequestChat.php")
    fun createRoomRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<CreateRoomModel>

    @POST("webServices/updateNotificationReadStatus.php")
    fun updateReadNotificationRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<UpdateMessageSeenModel>


    @POST("webServices/logOut.php")
    fun logoutRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<UpdateMessageSeenModel>

    @POST("webServices/inviteAndEarnUrl.php")
    fun inviteAndEarnRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<InviteAndEarnModel>
}


