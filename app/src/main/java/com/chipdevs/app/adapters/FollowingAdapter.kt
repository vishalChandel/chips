package com.chipdevs.app.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.Userfollowlist
import com.chipdevs.app.ui.activities.FriendProfileActivity
import com.chipdevs.app.ui.activities.OthersProfileActivity
import com.chipdevs.app.ui.fragments.FollowingFragment

class FollowingAdapter(

    var context: FollowingFragment,
    var followersListing: MutableList<Userfollowlist>,
    var activity: FragmentActivity?,
   var loggedInUserID: String
) : RecyclerView.Adapter<FollowingAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_following, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        setDataOnWidgets(holder,position)

        holder.btnFollowTV.setOnClickListener {


            context.onFollowClick(
                position, followersListing[position].userID, followersListing[position].isfollow,
                holder.btnFollowTV,
                followersListing[position].status, followersListing[position].changeStatus
            )
        }

        holder.profileRL.setOnClickListener {

            if(followersListing[position].userID==loggedInUserID)
            {
                //trying to open own account
            }
            else
            {

                val intent = Intent(activity, FriendProfileActivity::class.java)
                intent.putExtra("mUserName",followersListing[position].name)
                intent.putExtra("mOtherUserId",followersListing[position].userID)
                intent.putExtra("mImageUrl",followersListing[position].image)
                intent.putExtra("mFollowStatus",followersListing[position].changeStatus)
                intent.putExtra("mAccountPrivacy",followersListing[position].status)
                context.startActivity(intent)


//                if(followersListing[position].status=="1")//account private
//                {
//                    val intent = Intent(activity, OthersProfileActivity::class.java)
//                    intent.putExtra("mUserName",followersListing[position].name)
//                    intent.putExtra("mOtherUserId",followersListing[position].userID)
//                    intent.putExtra("mImageUrl",followersListing[position].image)
//
//                    intent.putExtra("mFollowStatus",followersListing[position].changeStatus)
//
//                    context.startActivity(intent)
//                }
//                else//account public
//                {
//                    val intent = Intent(activity, FriendProfileActivity::class.java)
//                    intent.putExtra("mUserName",followersListing[position].name)
//                    intent.putExtra("mOtherUserId",followersListing[position].userID)
//                    intent.putExtra("mImageUrl",followersListing[position].image)
//                    intent.putExtra("mFollowStatus",followersListing[position].changeStatus)
//                    context.startActivity(intent)
//                }
            }

        }
    }

    override fun getItemCount(): Int {
        return followersListing.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgFriendProfileIV: ImageView
        var txtFriendNameTV: TextView
        var btnFollowTV: TextView
        var profileRL:RelativeLayout

        init {
            imgFriendProfileIV = itemView.findViewById(R.id.imgFriendProfileIV)
            txtFriendNameTV = itemView.findViewById(R.id.txtFriendNameTV)
            btnFollowTV = itemView.findViewById(R.id.btnFollowTV)
            profileRL= itemView.findViewById(R.id.profileRL)
        }
    }

    fun setDataOnWidgets(holder: ViewHolder, position: Int)
    {
        holder.txtFriendNameTV.text = followersListing[position].name


        if (followersListing[position].userID == loggedInUserID) {
            holder.btnFollowTV.visibility = View.GONE
        } else {
            if (followersListing[position].isfollow == "0") {
                if (followersListing[position].changeStatus == "0") {
                    holder.btnFollowTV.text = context?.getString(R.string.follow)
                }


            } else if (followersListing[position].isfollow == "1") {
                if (followersListing[position].changeStatus == "1") {
                    holder.btnFollowTV.text = context?.getString(R.string.requested)
                    holder.btnFollowTV.setBackground(
                        ContextCompat.getDrawable(
                            activity!!,
                            R.drawable.bg_description
                        )
                    )
                    holder.btnFollowTV.setTextColor(context.resources.getColor(R.color.colorBlack))
                } else if (followersListing[position].changeStatus == "2") {
                    holder.btnFollowTV.text = context?.getString(R.string.following)
                } else {
                    holder.btnFollowTV.text = context?.getString(R.string.follow)
                }
            }
        }


        Glide.with(context!!).load(followersListing[position].image)
            .placeholder(R.drawable.ic_profile)
            .error(R.drawable.ic_profile)
            .into(holder.imgFriendProfileIV)

    }
    interface  followUnFollowUser
    {
        fun onFollowClick(
            position: Int,
            otherUserId: String,
            isfollow: String,
            btnFollowTV: TextView,
            status: String,
            changeStatus: String,
        )

    }

}