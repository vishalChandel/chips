package com.chipdevs.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.interfaces.MoveToBag
import com.chipdevs.app.interfaces.RemoveFromWishList
import com.chipdevs.app.models.DataItem1

class WishListAdapter(
    var mContext: Context, var mArrayList: ArrayList<DataItem1?>?,
    var mRemoveFromWishList: RemoveFromWishList,
    var mMoveToBag: MoveToBag) :
    RecyclerView.Adapter<WishListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_wishlist, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)
        mContext?.let {
            Glide.with(it).load(mModel?.image)
                .placeholder(R.drawable.img_imagview_ph)
                .error(R.drawable.img_imagview_ph)
                .into(holder.imgCardIV)
        }
        holder.txtCardNameTV.text = mModel!!.title
        holder.txtCardPriceTV.text = mModel!!.description
        holder.txtChipsPointsTV.text = mModel!!.chipPrice


        holder.imgCrossIV.setOnClickListener {
            mRemoveFromWishList.viewClick(position, mModel.productID!!, mModel.id!!)
        }

        holder.txtMoveToBagTV.setOnClickListener {
            mMoveToBag.viewClick(position, mModel.productID!!, mModel.id!!,mModel.chipPrice!!)

        }
    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCardIV: ImageView
        var imgChipsLogoIV: ImageView
        var imgCrossIV: ImageView
        var txtCardNameTV: TextView
        var txtCardPriceTV: TextView
        var txtChipsPointsTV: TextView
        var txtMoveToBagTV: TextView

        init {
            imgCardIV = itemView.findViewById(R.id.imgCardIV)
            imgChipsLogoIV = itemView.findViewById(R.id.imgChipsLogoIV)
            imgCrossIV = itemView.findViewById(R.id.imgCrossIV)
            txtCardNameTV = itemView.findViewById(R.id.txtCardNameTV)
            txtCardPriceTV = itemView.findViewById(R.id.txtCardPriceTV)
            txtChipsPointsTV = itemView.findViewById(R.id.txtChipsPointsTV)
            txtMoveToBagTV = itemView.findViewById(R.id.txtMoveToBagTV)
        }
    }
}