package com.chipdevs.app.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.chipdevs.app.Interfaces.FollowerCounterCallBack
import com.chipdevs.app.models.Userfollowlist
import com.chipdevs.app.ui.fragments.FollowersFragment
import com.chipdevs.app.ui.fragments.FollowingFragment


class FriendProfilePagerAdapter(
    val myContext: FollowerCounterCallBack,
    fm: FragmentManager,
    internal var totalTabs: Int,
    var mOtherUserId: String,
    var followingListing: MutableList<Userfollowlist>,
   var  followersListing: MutableList<Userfollowlist>
) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {

                return FollowingFragment(mOtherUserId,followingListing)
            }
            1 -> {
                return FollowersFragment(mOtherUserId,followersListing)
            }
            else -> return FollowingFragment(mOtherUserId,followingListing)
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}