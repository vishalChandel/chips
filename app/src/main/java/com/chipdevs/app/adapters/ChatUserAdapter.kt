package com.chipdevs.app.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.AllUsersItem


import com.chipdevs.app.ui.activities.ChatActivity


class ChatUserAdapter(var mContext: Context,var mArrayList: ArrayList<AllUsersItem?>?) : RecyclerView.Adapter<ChatUserAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_chat_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)
        if(mModel!!.unreadCount?.toInt()==0)
        {
            holder.unReadMessageCountTv.visibility=View.GONE
        }
        else
        {
            holder.unReadMessageCountTv.text= mModel.unreadCount
            holder.unReadMessageCountTv.visibility=View.VISIBLE
        }
        mContext?.let {
            Glide.with(it).load(mModel?.profilePic)
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.imgProfileIV)
        }
        holder.txtUserNameTV.text = mModel!!.username
        holder.txtUserMessageTV.text = mModel!!.message
        holder.chatUserRL.setOnClickListener {
            val intent = Intent(mContext, ChatActivity::class.java)
            intent.putExtra("mRoomId",mModel.roomNo)
            intent.putExtra("mUsername",mModel.username)
            intent.putExtra("otherUserID",mModel.receiverId)
            mContext?.startActivity(intent)
        }


    }
    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgProfileIV: ImageView
        var txtUserNameTV: TextView
        var txtUserMessageTV: TextView
        var unReadMessageCountTv: TextView
        var chatUserRL: RelativeLayout


        init {
            imgProfileIV = itemView.findViewById(R.id.imgProfileIV)
            txtUserNameTV = itemView.findViewById(R.id.txtUserNameTV)
            txtUserMessageTV = itemView.findViewById(R.id.txtUserMessageTV)
            chatUserRL = itemView.findViewById(R.id.chatUserRL)
            unReadMessageCountTv = itemView.findViewById(R.id.unReadMessageCountTv)
        }
    }


}