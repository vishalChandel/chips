package com.chipdevs.app.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.NotificationData
import com.chipdevs.app.ui.activities.ChatActivity
import com.chipdevs.app.ui.activities.FriendProfileActivity
import com.chipdevs.app.ui.activities.NotificationActivity
import com.jackandphantom.circularprogressbar.CircleProgressbar
import de.hdodenhof.circleimageview.CircleImageView

class NotificationAdapter(
    var context: NotificationActivity,
    var notificationListing: MutableList<NotificationData>
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_notification, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var followRequestStatus = ""
        Glide.with(context!!).load(notificationListing[position].image)
            .error(R.drawable.ic_profile)
            .placeholder(R.drawable.ic_profile)
            .into(holder.imgProfileIV2)

        if (notificationListing[position].notification_type == "1")
        {
            holder.messageNotifLL.visibility=View.GONE
            holder.otherNotifLL.visibility=View.VISIBLE

            holder.acceptRejectLL.visibility = View.VISIBLE
            holder.txtDateTimeTV.visibility = View.GONE
            holder.txtNotificationTV2.text = notificationListing[position].message
        } else if (notificationListing[position].notification_type == "3") {

          //  holder.itemView.visibility = View.GONE
        } else if(notificationListing[position].notification_type == "4")//messageNotification
        {
            holder.messageNotifLL.visibility=View.VISIBLE
            holder.otherNotifLL.visibility=View.GONE

            holder.txtNotificationTVMessage.text = notificationListing[position].message
            holder.messageReceivedTv.text = notificationListing[position].description
            holder.txtDateTimeTVMessage.text = notificationListing[position].creation_date

            Glide.with(context!!).load(notificationListing[position].image)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .into(holder.imgProfileIVMessage)
        }
        else
        {
            holder.messageNotifLL.visibility=View.GONE
            holder.otherNotifLL.visibility=View.VISIBLE
            holder.acceptRejectLL.visibility = View.GONE
            holder.txtDateTimeTV.visibility = View.VISIBLE
            holder.txtNotificationTV2.text = notificationListing[position].message
            holder.txtDateTimeTV.text = notificationListing[position].creation_date
        }

        holder.btnAcceptTV.setOnClickListener {
            followRequestStatus = "1"
            context.onBtnClick(
                position, notificationListing[position].followID,
                notificationListing[position].otherID, followRequestStatus
            )

        }
        holder.btnRejectTV.setOnClickListener {
            followRequestStatus = "2"
            context.onBtnClick(
                position, notificationListing[position].followID,
                notificationListing[position].otherID, followRequestStatus
            )
        }


        holder.itemView.setOnClickListener {

            if (notificationListing[position].notification_type == "4")//navigate to chat activity
            {
                val i = Intent(context, ChatActivity::class.java)
                i.putExtra("mRoomId", notificationListing[position].room_id)
                i.putExtra("mUsername", notificationListing[position].name)
                i.putExtra("otherUserID", notificationListing[position].otherID)
                context.startActivity(i)
            } else if (notificationListing[position].notification_type == "0") {


                val intent = Intent(context, FriendProfileActivity::class.java)
                intent.putExtra("mUserName", notificationListing[position].name)
                intent.putExtra("mOtherUserId", notificationListing[position].otherID)
                intent.putExtra("mImageUrl", notificationListing[position].image)
                context?.startActivity(intent)
            }

        }
    }

    override fun getItemCount(): Int {
        return notificationListing.size
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //var imgProfileIV1: ImageView
        var imgProfileIV2: ImageView = itemView.findViewById(R.id.imgProfileIV2)

        // var txtNotificationTV1: TextView
        var txtNotificationTV2: TextView = itemView.findViewById(R.id.txtNotificationTV2)
        var btnAcceptTV: TextView = itemView.findViewById(R.id.btnAcceptTV)
        var btnRejectTV: TextView = itemView.findViewById(R.id.btnRejectTV)
        var txtDateTimeTV: TextView = itemView.findViewById(R.id.txtDateTimeTV)
        var acceptRejectLL: LinearLayout = itemView.findViewById(R.id.acceptRejectLL)
        var messageNotifLL: LinearLayout = itemView.findViewById(R.id.messageNotifLL)
        var otherNotifLL: LinearLayout = itemView.findViewById(R.id.otherNotifLL)
        var txtNotificationTVMessage: TextView = itemView.findViewById(R.id.txtNotificationTVMessage)
        var txtDateTimeTVMessage: TextView = itemView.findViewById(R.id.txtDateTimeTVMessage)
        var messageReceivedTv: TextView = itemView.findViewById(R.id.messageReceivedTv)
        var imgProfileIVMessage: CircleImageView = itemView.findViewById(R.id.imgProfileIVMessage)

    }


    interface acceptRejectAction {
        fun onBtnClick(
            position: Int,
            followId: String,
            userId: String,
            followRequestStatus: String
        )
    }



}