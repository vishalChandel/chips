package com.chipdevs.app.adapters.chat

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chipdevs.app.R
import com.chipdevs.app.models.AllMessagesItem
import com.chipdevs.app.utils.LEFT_VIEW_HOLDER
import com.chipdevs.app.utils.RIGHT_VIEW_HOLDER
import java.util.*

class ChatMessagesAdapter(val mActivity: Activity?, var mArrayList: ArrayList<AllMessagesItem?>?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right_row, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left_row, parent, false)
            return ItemLeftViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
            (holder as ItemRightViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessagesItem?
            )
        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
            (holder as ItemLeftViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessagesItem?
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        return -1
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }
}
