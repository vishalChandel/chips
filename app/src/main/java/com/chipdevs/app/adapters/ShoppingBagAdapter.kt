package com.chipdevs.app.adapters

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.CartDetails
import com.chipdevs.app.models.IncrementDecrementModel
import com.chipdevs.app.models.ItemIncrementData
import com.chipdevs.app.retrofit.ApiClient
import com.chipdevs.app.retrofit.ApiInterface
import com.chipdevs.app.ui.activities.ShoppingBagActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ShoppingBagAdapter(
    var context: ShoppingBagActivity,
    var allProductListing: MutableList<CartDetails>
) : RecyclerView.Adapter<ShoppingBagAdapter.ViewHolder>() {

    var type = ""
    var incrementedDetail: ItemIncrementData? = null
    var progressDialog: Dialog? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_shopping_bag, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(allProductListing[position].image)
            .placeholder(R.drawable.img_imagview_ph)
            .error(R.drawable.img_imagview_ph)
            .into(holder.imgCardIV)

        holder.txtCardNameTV.text = allProductListing[position].title
        holder.txtChipsPointsTV.text = allProductListing[position].chip_price
        holder.txtNumberTV.text = allProductListing[position].quantity
        holder.txtCardPriceTV.text = allProductListing[position].description

        holder.txtRemoveTV.setOnClickListener {
            context.onRemoveClick(position, allProductListing[position].cartID,holder.txtNumberTV)

        }


        holder.txtMoveToWishlistTV.setOnClickListener {
            context.moveToWishListItem(
                allProductListing[position].cartID,
                allProductListing[position].productID, position,holder.txtNumberTV
            )

        }

        holder.imgPlusIV.setOnClickListener {

            type = "1"
            incrementDecrementCartItem(allProductListing[position].cartID, holder, type, position)
        }
        holder.imgMinusIV.setOnClickListener {

            if (holder.txtNumberTV.text == "1") {
                context.onRemoveClick(
                    position,
                    allProductListing[position].cartID,
                    holder.txtNumberTV
                )
                // val toast=Toast.makeText(context,"No more decrement",Toast.LENGTH_SHORT).show()
            } else {
                type = "2"
                incrementDecrementCartItem(
                    allProductListing[position].cartID,
                    holder,
                    type,
                    position
                )
            }

        }
    }

    override fun getItemCount(): Int {
        return allProductListing.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCardIV: ImageView = itemView.findViewById(R.id.imgCardIV)
        var imgChipsLogoIV: ImageView = itemView.findViewById(R.id.imgChipsLogoIV)
        var imgPlusIV: ImageView = itemView.findViewById(R.id.imgPlusIV)
        var imgMinusIV: ImageView = itemView.findViewById(R.id.imgMinusIV)
        var txtCardNameTV: TextView = itemView.findViewById(R.id.txtCardNameTV)
        var txtCardPriceTV: TextView = itemView.findViewById(R.id.txtCardPriceTV)
        var txtChipsPointsTV: TextView = itemView.findViewById(R.id.txtChipsPointsTV)
        var txtRemoveTV: TextView = itemView.findViewById(R.id.txtRemoveTV)
        var txtMoveToWishlistTV: TextView = itemView.findViewById(R.id.txtMoveToWishlistTV)
        var txtNumberTV: TextView = itemView.findViewById(R.id.txtNumberTV)
        var txtDividerTV: TextView = itemView.findViewById(R.id.txtDividerTV)
    }

    interface onRemoveItem {
        fun onRemoveClick(
            position: Int,
            cartID: String,
            txtNumberTV: TextView,
        )

        fun moveToWishListItem(
            cartID: String,
            productID: String,
            position: Int,
            txtNumberTV: TextView
        )
        fun increasePrice(position: String?, type: String)
        fun getLatestQuantity(position: Int, quantity: String)


    }

    fun incrementDecrementCartItem(
        cartID: String,
        holder: ViewHolder,
        type: String,
        position: Int
    ) {
        showProgressDialog(context)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["cartID"] = cartID
        mMap["type"] = this.type

        val call: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        call.incrementDecrementItem(mMap).enqueue(object : Callback<IncrementDecrementModel> {
            override fun onResponse(
                call: Call<IncrementDecrementModel>,
                response: Response<IncrementDecrementModel>
            ) {
                dismissProgressDialog()
                if (response?.body()?.status == "1") {

                    incrementedDetail = response?.body()?.cart_details
                    allProductListing[position].quantity = incrementedDetail?.quantity.toString()
                    notifyDataSetChanged()
                    context.getLatestQuantity(position,incrementedDetail?.quantity!!)
                    updateNewData(holder, incrementedDetail, type)

                }
            }

            override fun onFailure(call: Call<IncrementDecrementModel>, t: Throwable) {
                dismissProgressDialog()
            }

        })
    }

    fun updateNewData(holder: ViewHolder, incrementedDetail: ItemIncrementData?, type: String) {

        holder.txtNumberTV.text = incrementedDetail?.quantity
        holder.txtChipsPointsTV.text = incrementedDetail?.price
        context.increasePrice(incrementedDetail?.price, type)
    }


    /*
     * Show Progress Dialog
     * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


}