package com.chipdevs.app.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chipdevs.app.R
import com.chipdevs.app.models.SearchFiendData
import com.chipdevs.app.ui.activities.FriendProfileActivity
import com.chipdevs.app.ui.activities.OthersProfileActivity
import com.chipdevs.app.ui.activities.SearchActivity

class SearchAdapter(var context: SearchActivity, var searchedFriendList: MutableList<SearchFiendData>) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_search, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtOthersNameTV.text = searchedFriendList[position].name
        holder.itemView.setOnClickListener {

            val intent = Intent(context, FriendProfileActivity::class.java)
            intent.putExtra("mOtherUserId",searchedFriendList[position].userID)
            intent.putExtra("mAccountPrivacy",searchedFriendList[position].status)
            context.startActivity(intent)

//            if(searchedFriendList[position].status=="1")//account private
//            {
//
//                val intent = Intent(context, FriendProfileActivity::class.java)
//                intent.putExtra("mUserName",searchedFriendList[position].name)
//                intent.putExtra("mOtherUserId",searchedFriendList[position].userID)
//                intent.putExtra("mImageUrl",searchedFriendList[position].image)
//                intent.putExtra("mFollowStatus",searchedFriendList[position].changeStatus)
//                intent.putExtra("mAccountPrivacy",searchedFriendList[position].status)
//                context.startActivity(intent)
//
////                if(searchedFriendList[position].changeStatus=="2")
////                {
////                    val intent = Intent(context, FriendProfileActivity::class.java)
////                    intent.putExtra("mUserName",searchedFriendList[position].name)
////                    intent.putExtra("mOtherUserId",searchedFriendList[position].userID)
////                    intent.putExtra("mImageUrl",searchedFriendList[position].image)
////                    intent.putExtra("mFollowStatus",searchedFriendList[position].changeStatus)
////                    intent.putExtra("mAccountPrivacy",searchedFriendList[position].status)
////                    context.startActivity(intent)
////                }
////                else{
////                    val intent = Intent(context, OthersProfileActivity::class.java)
////                    intent.putExtra("mUserName",searchedFriendList[position].name)
////                    intent.putExtra("mOtherUserId",searchedFriendList[position].userID)
////                    intent.putExtra("mImageUrl",searchedFriendList[position].image)
////                    intent.putExtra("mFollowStatus",searchedFriendList[position].changeStatus)
////                    context.startActivity(intent)
////                }
//
//
//
////                if(OthersProfileActivity.isRequestSent=="yes")
////                {
////                    intent.putExtra("mFollowStatus","1")
////                }
////                else if(OthersProfileActivity.isRequestSent=="no")
////                {
////                    intent.putExtra("mFollowStatus","0")
////                }
////                else
////                {
////                    intent.putExtra("mFollowStatus",searchedFriendList[position].changeStatus)
////                }
//
//
//
//
//            }
//            else//account public
//            {
//                val intent = Intent(context, FriendProfileActivity::class.java)
//                intent.putExtra("mUserName",searchedFriendList[position].name)
//                intent.putExtra("mOtherUserId",searchedFriendList[position].userID)
//                intent.putExtra("mImageUrl",searchedFriendList[position].image)
//                intent.putExtra("mFollowStatus",searchedFriendList[position].changeStatus)
//                intent.putExtra("mAccountPrivacy",searchedFriendList[position].status)
//                context.startActivity(intent)
//            }

        }
    }

    override fun getItemCount(): Int {
        return searchedFriendList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var icSearchIV: ImageView
        var txtOthersNameTV: TextView
        var mainRL: RelativeLayout


        init {
            icSearchIV = itemView.findViewById(R.id.icSearchIV)
            txtOthersNameTV = itemView.findViewById(R.id.txtOthersNameTV)
            mainRL = itemView.findViewById(R.id.mainRL)
        }
    }
}