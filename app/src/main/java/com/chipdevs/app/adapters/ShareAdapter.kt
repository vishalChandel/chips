package com.chipdevs.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.AnswerOption1
import com.chipdevs.app.models.AnswerOption2
import com.chipdevs.app.models.QuestionData
import com.chipdevs.app.ui.fragments.ShareFragment


class ShareAdapter(
    var context: ShareFragment,
    var sharedQuestionsListing: MutableList<QuestionData>,
    var adsImageString: String
) : RecyclerView.Adapter<ShareAdapter.ViewHolder>() {

    var correctAnswer = ""
    var answerByUser = ""
    var timeUpStatus = ""

    var answerListing = mutableListOf<AnswerOption1>()
    var answerListing2 = mutableListOf<AnswerOption2>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_share, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtQuestionTV.text = sharedQuestionsListing[position].question
        holder.txtValueTV.text = sharedQuestionsListing[position].chipsValue
        holder.txtNameTV.text = sharedQuestionsListing[position].shared_name

        Glide.with(context).load(sharedQuestionsListing[position].shared_image)
            .error(R.drawable.ic_profile)
            .placeholder(R.drawable.ic_profile)
            .into(holder.imgProfileCIV)

        Glide.with(context).load(adsImageString)
            .error(R.drawable.img_placeholder)
            .placeholder(R.drawable.img_placeholder)
            .override(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            .into(holder.imgChipsIV)

        answerListing = sharedQuestionsListing[position].answerOption1
        answerListing2 = sharedQuestionsListing[position].answerOption2

        /**
         * Set options according to data from api
         */
        for (i in 0 until answerListing.size) {
            if (i == 0) {
                holder.option1LL.visibility = View.VISIBLE
                holder.txtOptionTV1.text = answerListing[i].option
            } else {
                holder.option2LL.visibility = View.VISIBLE
                holder.txtOptionTV2?.text = answerListing[i].option
            }
        }

        for (i in 0 until answerListing2.size) {
            if (i == 0) {
                holder.option3LL.visibility = View.VISIBLE
                holder.txtOptionTV3.text = answerListing2[i].option
            } else {
                holder.option4LL.visibility = View.VISIBLE
                holder.txtOptionTV4.text = answerListing2[i].option
            }
        }


        /**
         * set listener on checkboxes
         */

        holder.imgCheckIV1.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                holder.imgCheckIV2.isChecked = false
                holder.imgCheckIV3.isChecked = false
                holder.imgCheckIV4.isChecked = false

                holder.btnShareTV.text = context.getString(R.string.submit)
                holder.icShareIV?.visibility = View.GONE
                answerByUser = holder.txtOptionTV1?.text.toString().trim()
            } else {
                holder.btnShareTV?.text = context.getString(R.string.share)
                holder.icShareIV?.visibility = View.VISIBLE
            }
        }
        holder.imgCheckIV2.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                holder.imgCheckIV1?.isChecked = false
                holder.imgCheckIV3.isChecked = false
                holder.imgCheckIV4.isChecked = false

                holder.btnShareTV.text = context.getString(R.string.submit)
                holder.icShareIV?.visibility = View.GONE
                answerByUser = holder.txtOptionTV2.text.toString().trim()
            } else {
                holder.btnShareTV?.text = context.getString(R.string.share)
                holder.icShareIV?.visibility = View.VISIBLE
            }
        }
        holder.imgCheckIV3.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                holder.imgCheckIV1.isChecked = false
                holder.imgCheckIV2.isChecked = false
                holder.imgCheckIV4.isChecked = false

                holder.btnShareTV?.text = context.getString(R.string.submit)
                holder.icShareIV?.visibility = View.GONE
                answerByUser = holder.txtOptionTV3?.text.toString().trim()
            } else {
                holder.btnShareTV?.text = context.getString(R.string.share)
                holder.icShareIV?.visibility = View.VISIBLE
            }
        }
        holder.imgCheckIV4.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                holder.imgCheckIV1.isChecked = false
                holder.imgCheckIV2.isChecked = false
                holder.imgCheckIV3.isChecked = false
                holder.btnShareTV?.text = context.getString(R.string.submit)
                holder.icShareIV?.visibility = View.GONE
                answerByUser = holder.txtOptionTV4?.text.toString().trim()
            } else {
                holder.btnShareTV?.text = context.getString(R.string.share)
                holder.icShareIV?.visibility = View.VISIBLE
            }
        }


        holder.btnShareTV.setOnClickListener {

            if (holder.btnShareTV.text == context.getString(R.string.share)) {
                timeUpStatus = "1"

                context.onAnswerClick(
                    holder.adapterPosition,
                    sharedQuestionsListing[position].quesID,
                    answerByUser,
                    timeUpStatus,
                    sharedQuestionsListing[position].correctAnswer,
                    holder
                )
                clearChecks(holder)
            } else if(holder.btnShareTV.text == context.getString(R.string.submit)) {
                timeUpStatus = "0"

                context.onAnswerClick(
                    holder.adapterPosition, sharedQuestionsListing[position].quesID,
                    answerByUser, timeUpStatus, sharedQuestionsListing[position].correctAnswer,holder
                )
                clearChecks(holder)
            }

        }

    }

    override fun getItemCount(): Int {
        return sharedQuestionsListing.size
    }

  public fun clearChecks(holder: ViewHolder) {
        holder.imgCheckIV1.isChecked = false
        holder.imgCheckIV2.isChecked = false
        holder.imgCheckIV3.isChecked = false
        holder.imgCheckIV4.isChecked = false
//        holder.option1LL?.visibility = View.GONE
//        holder.option2LL?.visibility = View.GONE
//        holder.option3LL?.visibility = View.GONE
//        holder.option4LL?.visibility = View.GONE
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCheckIV1: CheckBox
        var imgCheckIV2: CheckBox
        var imgCheckIV3: CheckBox
        var imgCheckIV4: CheckBox
        var icShareIV: ImageView
        var imgChipsIV: ImageView
        var imgProfileCIV: ImageView
        var txtQuestionTV: TextView
        var txtOptionTV1: TextView
        var txtOptionTV2: TextView
        var txtOptionTV3: TextView
        var txtOptionTV4: TextView
        var btnShareTV: TextView
        var txtNameTV: TextView
        var txtChipsValueTV: TextView
        var txtValueTV: TextView

        var option1LL: LinearLayout
        var option2LL: LinearLayout
        var option3LL: LinearLayout
        var option4LL: LinearLayout


        init {
            imgCheckIV1 = itemView.findViewById(R.id.option1)
            imgCheckIV2 = itemView.findViewById(R.id.option2)
            imgCheckIV3 = itemView.findViewById(R.id.option3)
            imgCheckIV4 = itemView.findViewById(R.id.option4)
            icShareIV = itemView.findViewById(R.id.icShareIV)
            imgChipsIV = itemView.findViewById(R.id.imgChipsIV)
            imgProfileCIV = itemView.findViewById(R.id.imgProfileCIV)
            txtQuestionTV = itemView.findViewById(R.id.txtQuestionTV)
            txtOptionTV1 = itemView.findViewById(R.id.txtOptionTV1)
            txtOptionTV2 = itemView.findViewById(R.id.txtOptionTV2)
            txtOptionTV3 = itemView.findViewById(R.id.txtOptionTV3)
            txtOptionTV4 = itemView.findViewById(R.id.txtOptionTV4)
            btnShareTV = itemView.findViewById(R.id.btnShareTV)
            txtNameTV = itemView.findViewById(R.id.txtNameTV)
            txtChipsValueTV = itemView.findViewById(R.id.txtChipsValueTV)
            txtValueTV = itemView.findViewById(R.id.txtValueTV)

            option1LL = itemView.findViewById(R.id.optionLL1)
            option2LL = itemView.findViewById(R.id.optionLL2)
            option3LL = itemView.findViewById(R.id.optionLL3)
            option4LL = itemView.findViewById(R.id.optionLL4)


        }
    }

    interface onSubmitQuestion {
        fun onAnswerClick(
            position: Int,
            quesID: String,
            answerByUser: String,
            timeUpStatus: String,
            correctAnswer: String,
            holder: ViewHolder)

    }


}