package com.chipdevs.app.adapters.chat

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chipdevs.app.R
import com.chipdevs.app.models.AllMessagesItem
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

class ItemRightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtDataTimeTV = itemView.findViewById<TextView>(R.id.txtDataTimeTV) as TextView
    var txtMessageTV = itemView.findViewById<TextView>(R.id.txtMessageTV) as TextView

    fun bindData(mActivity: Activity?, mModel: AllMessagesItem?) {
        txtDataTimeTV.setText(gettingLongToFormatedTime(mModel!!.creationDate!!.toLong()))
        txtMessageTV.setText(mModel!!.message)
    }


    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("hh:mm a")
        // CREATE DateFormatSymbols WITH ALL SYMBOLS FROM (DEFAULT) Locale
        val symbols = DateFormatSymbols(Locale.getDefault())
        // OVERRIDE SOME symbols WHILE RETAINING OTHERS
        symbols.setAmPmStrings(arrayOf("AM", "PM"))
        jdf.setDateFormatSymbols(symbols)
        return jdf.format(date)
    }

}