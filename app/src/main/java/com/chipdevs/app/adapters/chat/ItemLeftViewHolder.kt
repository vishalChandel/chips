package com.chipdevs.app.adapters.chat

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.AllMessagesItem
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

class ItemLeftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtDataTimeTV = itemView.findViewById<TextView>(R.id.txtDataTimeTV) as TextView
    var txtNameTV = itemView.findViewById<TextView>(R.id.txtNameTV) as TextView
    var txtMessageTV = itemView.findViewById<TextView>(R.id.txtMessageTV) as TextView
    var itemImageIV = itemView.findViewById<TextView>(R.id.itemImageIV) as de.hdodenhof.circleimageview.CircleImageView

    fun bindData(mActivity: Activity?, mModel: AllMessagesItem?) {
        txtDataTimeTV.setText(gettingLongToFormatedTime(mModel!!.creationDate!!.toLong()))
        txtNameTV.setText(mModel.username+",")
        txtMessageTV.setText(mModel!!.message)
        mActivity?.let {
            Glide.with(it)
                .load(mModel!!.profilePic)
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(itemImageIV)
        }
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("hh:mm a")
        // CREATE DateFormatSymbols WITH ALL SYMBOLS FROM (DEFAULT) Locale
        val symbols = DateFormatSymbols(Locale.getDefault())
        // OVERRIDE SOME symbols WHILE RETAINING OTHERS
        symbols.setAmPmStrings(arrayOf("AM", "PM"))
        jdf.setDateFormatSymbols(symbols)
        return jdf.format(date)
    }
}