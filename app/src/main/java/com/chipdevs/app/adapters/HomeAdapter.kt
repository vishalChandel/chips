package com.chipdevs.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chipdevs.app.R
import com.chipdevs.app.models.DataItem
import com.chipdevs.app.models.ProductItems
import com.chipdevs.app.ui.fragments.HomeFragment


class HomeAdapter(
    var activity: FragmentActivity?,
    var mProductListing: MutableList<ProductItems>,
    var homeFragment: HomeFragment,
    var allProductsListing: MutableList<DataItem>
) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    var homeAdapter: HomeChildAdapter? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.item_home, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtCardNameTV.text = allProductsListing[position].title

        setChildAdapter(holder,allProductsListing[position].products)

    }

    override fun getItemCount(): Int {
       return allProductsListing.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtCardNameTV: TextView
        var homeChildRV: RecyclerView
        init {

            txtCardNameTV = itemView.findViewById(R.id.txtCardNameTV)
            homeChildRV = itemView.findViewById(R.id.homeChildRV)

        }
    }

    fun setChildAdapter(holder: ViewHolder, products: MutableList<ProductItems>)
    {
        holder.homeChildRV.layoutManager = GridLayoutManager(activity, 2)
        homeAdapter = HomeChildAdapter(activity,products,homeFragment)
        holder.homeChildRV.adapter = homeAdapter
    }

}