package com.chipdevs.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.FriendsItem
import com.chipdevs.app.ui.activities.FriendProfileActivity

class FriendsListAdapter(var mActivity: Activity?, var mArrayList: ArrayList<FriendsItem?>?) :
    RecyclerView.Adapter<FriendsListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_friends_list, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)
        holder.txtFriendsNameTV.text = mModel!!.firstName+" "+mModel!!.lastName
        mActivity?.let {
            Glide.with(it).load(mModel?.image)
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.imgFriendsProfileIV)
        }
        holder.itemView.setOnClickListener {

            val intent = Intent(mActivity, FriendProfileActivity::class.java)
            intent.putExtra("mUserName",mModel.userName)
            intent.putExtra("mOtherUserId",mModel.userID)
            intent.putExtra("mImageUrl",mModel.image)
            intent.putExtra("mFollowStatus",mModel.changeStatus)
            intent.putExtra("mAccountPrivacy",mModel.status)
            mActivity?.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgFriendsProfileIV: ImageView
        var txtFriendsNameTV: TextView


        init {
            imgFriendsProfileIV = itemView.findViewById(R.id.imgFriendsProfileIV)
            txtFriendsNameTV = itemView.findViewById(R.id.txtFriendsNameTV)
        }
    }


}