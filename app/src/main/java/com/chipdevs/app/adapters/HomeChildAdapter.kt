package com.chipdevs.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chipdevs.app.R
import com.chipdevs.app.models.ProductItems
import com.chipdevs.app.ui.fragments.HomeFragment


class HomeChildAdapter(
    var activity: FragmentActivity?,
    var products: MutableList<ProductItems>,
   var context: HomeFragment
) :
    RecyclerView.Adapter<HomeChildAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.item_home_child, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.titleTVChild.text=products[position].title
        holder.descriptionTVChild.text =products[position].description
        holder.chipsValueTVChild.text= products[position].chip_price
        var image: String? = products[position].image

        Glide.with(activity!!).load(image)
                .placeholder(R.drawable.img_imagview_ph)
                .error(R.drawable.img_imagview_ph)
                .into(holder.imgCardIV)



        holder.imgShoppingBagIVChild.setOnClickListener {

            context.onBagClick(
                position,
                products[position].chip_price,
                products[position].productID,
                holder.imgShoppingBagIVChild,
                products[position].check_value,
                products[position].cartID

            )
        }

        if (products[position].check_value == 0)//item not added in cart
        {
            holder.imgShoppingBagIVChild.setImageResource(R.drawable.ic_bag_blue)
        } else {
            holder.imgShoppingBagIVChild.setImageResource(R.drawable.ic_bag)
        }

    }

    override fun getItemCount(): Int {
       return products.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCardIV: ImageView
        var imgChipsLogoIV: ImageView
        var imgShoppingBagIVChild: ImageView



        var chipsValueTVChild: TextView

        var descriptionTVChild: TextView

        var titleTVChild: TextView

        init {
            imgCardIV = itemView.findViewById(R.id.imgCardIV)
            imgChipsLogoIV = itemView.findViewById(R.id.imgChipsLogoIV)
            imgShoppingBagIVChild = itemView.findViewById(R.id.imgShoppingBagIVChild)
            chipsValueTVChild = itemView.findViewById(R.id.chipsValueTVChild)
            descriptionTVChild = itemView.findViewById(R.id.descriptionTVChild)
            titleTVChild = itemView.findViewById(R.id.titleTVChild)
        }
    }

    interface addToCart {
        fun onBagClick(
            position: Int,
            chipPrice: String?,
            productID: String?,
            imgShoppingBagIV: ImageView,
            checkValue: Int?,
            cartID: String?
        )
    }
}